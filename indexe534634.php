<?php

ignore_user_abort(true);
$ob = ob_start();
$times['start'] = microtime(true);

# Names of the files
$f = [
    'config'         => '/home/masterpoll-documents/bot-config.php', // PHP file
    'bot.dir'        => '/var/www/bot/', // Directory of the bot
    'jpgraph.dir'    => '/home/masterpoll-documents/lib/chart/', // Directory of the JPGraphLib
    'functions'      => 'functions.php', // PHP file
    'anti-flood'     => 'antiflood.php', //PHP file
    'database'       => 'database.php', // PHP file
    'plugin_manager' => 'plugin_manager.php', // PHP file
    'plugins.dir'    => 'plugins', // Directory of the plugins
    'plugins'        => 'plugins.json', // JSON file
    'thumb_logo'     => '/home/masterpoll-documents/thumb_logo.jpeg', // Thumb file
    'affiliate'      => '/home/masterpoll-documents/affiliates.json', // JSON file of affiliates
	'class'          => '/home/masterpoll-documents/bots/PollCreator-Class.php', // Classe dei sondaggi
    'languages'      => 'languages.json' // JSON Languages file
];

if (!isset($_GET['key'])) {
    if ($config['devmode']) {
        echo "<b>Bot Error:</b> the Telegram Bot API key on the parameter 'key' was not found";
    } else {
        http_response_code(404);
    }
    die;
}

# Informazioni del Bot
$api = urldecode($_GET['key']); // Access key to the Telegram Bot API
$botID = str_replace('bot', '', explode(":", $api)[0]);
require $f['config'];
$admins = $config['admins']; // Admins of the Bot
$password = urldecode($_GET['password']); // Password to identify requests
$time_run = date('d-m-y-h-i-s');
date_default_timezone_set("UTC");

# Autenticazione del Bot
if ($config['password']) {
    if ($password !== $config['password']) {
        if ($config['devmode']) {
            echo "<br><b>Bot Error:</b> Password delle request errata!<br>";
        } else {
            http_response_code(404);
        }
        die;
    }
}

# Telegram Json Update
$times['telegram_update'] = microtime(true);
$content = file_get_contents("php://input");
if (!$content) {
    $update = false;
    if ($config['devmode']) {
        //echo "<br><b>Bot Warning</b>: Telegram non ha inviato nessun contenuto.<br>";
    } else {
        die;
    }
} else {
    $update = json_decode($content, true);
    $rupdate = $update;
}

# Funzioni
$times['functions'] = microtime(true);
$ch_api = curl_init();
require $f['functions'];

# Gestione Permessi
if (in_array($userID, $admins)) {
    $isadmin = true;
    if (!$is_clone) require($f['plugin_manager']);
} else {
    $isadmin = false;
}

# Database (Opzionale)
if ($config['usa_il_db'] and $config['usa_redis']) {
    require $f['database'];
} else {
    $config['json_payload'] = true;
    $config['response'] = false;
    if ($cbdata) {
        cb_reply($cbid, "⚠️ Bot offline", false);
    } elseif ($typechat == "private") {
        sm($chatID, "⚠️ Bot offline");
    }
    die;
}

# Whitelist utenti (modalità manutenzione o beta testing)
if ($u) {
    if ($config['whitelist_users'] !== false and is_array($config['whitelist_users'])) {
        $p = db_query("SELECT * FROM polls WHERE user_id = ? and poll_id = ?", [244432022, 53], true);
        if (!$p['ok']) {
            $config['json_payload'] = true;
            $config['response'] = false;
            if ($cbdata) {
                cb_reply($cbid, "⚠️ Bot offline", false);
            } elseif ($typechat == "private") {
                sm($chatID, "⚠️ Bot offline");
            }
            die;
        }
        $p = $p['result'];
        $betatesters = json_decode($p['choices'], true);
        $config['whitelist_users'] = $betatesters['participants'];
        $config['whitelist_users'] = array_merge($config['whitelist_users'], $admins);
        if (!in_array($userID, $config['whitelist_users'])) {
            if ($config['devmode']) {
                $uc = "@MasterPollBeta";
            } else {
                $uc = "@MasterPoll";
            }
            if ($typechat == "private") {
                sm($chatID, "<b>⛔ Beta Testing</b> \nJoin the Beta Testing on @MasterPollBeta.");
            }
            die;
        }
    }
}

# Gestione Plugin
$times['plugins'] = microtime(true);
$plugins = json_decode(file_get_contents($f['plugins']), true);
foreach ($plugins as $pl => $on) {
    $pluginp = $pl;
    if ($on) {
        if (file_exists($f['plugins.dir'] . '/' . $pl)) {
            require($f['plugins.dir'] . '/' . $pl);
        } else {
            call_error("Il plugin '" . code($pl) . "' non è stato trovato in " . code($f['plugins.dir']));
        }
    }
}
