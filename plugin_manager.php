<?php
$pluginp = $f['plugin_manager'];
$config['types']['quiz'] = true;
/*
NeleBotFramework
    Copyright (C) 2018  PHP-Coders

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
//Attenzione: in questo file non è possibile fare query al database

$c = [
    "true" => "✅",
    "false" => "❌",
    "" => "❌"
];

if ($cmd == "dump") {
	$config['json_payload'] = false;
	$config['response'] = false;
	file_put_contents("update-" . $update['update_id'] . ".json", json_encode($rupdate, JSON_PRETTY_PRINT));
	sd($chatID, "update-" . $update['update_id'] . ".json", "Bot: @" . $config['username_bot'] . " [" . code($botID) . "]");
	sleep(1);
	if (file_exists("update-" . $update['update_id'] . ".json")) unlink("update-" . $update['update_id'] . ".json");
	die;
}

if (strpos($cmd, "decode ") === 0) {
	$text = explode(" ", $cmd, 2)[1];
	sm($chatID, "<b>Encoded string:</b> " . htmlspecialchars($text) . "\n<b>Decoded string</b>: " . htmlspecialchars(bot_decode($text)));
	die;
}

if (strpos($cmd, "encode ") === 0) {
	$text = explode(" ", $cmd, 2)[1];
	sm($chatID, "<b>Encoded string:</b> " . htmlspecialchars(bot_encode($text)) . "\n<b>Decoded string:</b> " . htmlspecialchars($text));
	die;
}

if ($cmd == "isadmin") {
	sm($chatID, "Sei un Admin del Bot");
	die;
}

if ($cmd == "bots" or strpos($cbdata, 'editBots_') === 0) {
	$file = "/home/masterpoll-documents/status-bots.json";
	if (file_exists($file)) {
		$json = json_decode(file_get_contents($file), true);
	} else {
		$exjson = $config['usernames'];
		foreach($exjson as $j => $us) {
			$json[$j] = true;
		}
		file_put_contents($file, json_encode($json, JSON_PRETTY_PRINT));
	}
	if (strpos($cbdata, 'editBots_') === 0) {
		$e = explode("-", str_replace("editBots_", '', $cbdata));
		$type = strtolower($e[0]);
		$set = round($e[1]);
		if ($type == "all") {
			foreach ($json as $m => $s) {
				$json[$m] = $set;
			}
			$json[$botID] = true;
		} else {
			$json[$type] = $set;
		}
		file_put_contents($file, json_encode($json, JSON_PRETTY_PRINT));
	}
	foreach ($json as $type => $active) {
		if ($active) {
			$e = false;
			$active = true;
		} else {
			$e = true;
			$active = false;
		}
		$emo = $c[json_encode($active)];
		$extext = $type;
		$type = $config['usernames'][$type];
		$menu[] = [
			[
				"text" => "$type $emo",
				"callback_data" => "editBots_$extext-$e"
			]
		];
	}
	$menu[] = [
		[
			"text" => "Off",
			"callback_data" => "editBots_all-0"
		],
		[
			"text" => "On",
			"callback_data" => "editBots_all-1"
		]
	];
	$menu[] = [
		[
			"text" => "Fatto",
			"callback_data" => "fatto"
		]
	];
	if ($cmd) {
		dm($chatID, $msgID);
		sm($chatID, "Attiva/Disattiva i Bot:", $menu);
	} else {
		editMenu($chatID, $cbmid, $menu);
		cb_reply($cbid, '', false);
	}
	die;
}

if ($cmd == "methods" or strpos($cbdata, 'editMethods_') === 0) {
	$file = "/home/masterpoll-documents/api-methods.json";
	if (file_exists($file)) {
		$json = json_decode(file_get_contents($file), true);
	} else {
		$json = [
			'status' => true,
			'createpoll' => true,
			'editpoll' => true,
			'getpoll' => true,
			'getactivepolls' => true,
			'getallpolls' => true,
			'openpoll' => true,
			'closepoll' => true,
			'graph' => true,
			'getme' => true,
			'getsubscribers' => true
		];
		file_put_contents($file, json_encode($json, JSON_PRETTY_PRINT));
	}
	if (strpos($cbdata, 'editMethods_') === 0) {
		$e = explode("-", str_replace("editMethods_", '', $cbdata));
		$type = strtolower($e[0]);
		$set = round($e[1]);
		if ($type == "all") {
			foreach ($json as $m => $s) {
				$json[$m] = $set;
			}
		} else {
			$json[$type] = $set;
		}
		file_put_contents($file, json_encode($json, JSON_PRETTY_PRINT));
	}
	foreach ($json as $type => $active) {
		if ($active) {
			$e = false;
			$active = true;
		} else {
			$e = true;
			$active = false;
		}
		$emo = $c[json_encode($active)];
		$menu[] = [
			[
				"text" => "$type $emo",
				"callback_data" => "editMethods_$type-$e"
			]
		];
	}
	$menu[] = [
		[
			"text" => "Off",
			"callback_data" => "editMethods_all-0"
		],
		[
			"text" => "On",
			"callback_data" => "editMethods_all-1"
		]
	];
	$menu[] = [
		[
			"text" => "Fatto",
			"callback_data" => "fatto"
		]
	];
	if ($cmd) {
		dm($chatID, $msgID);
		sm($chatID, "Attiva/Disattiva i metodi delle API:", $menu);
	} else {
		editMenu($chatID, $cbmid, $menu);
		cb_reply($cbid, '', false);
	}
	die;
}

if ($cmd == "types" or strpos($cbdata, 'editTypes') === 0) {
	$file = "/home/masterpoll-documents/types.json";
	if (file_exists($file)) {
		$json = json_decode(file_get_contents($file), true);
	} else {
		$exjson = $config['types'];
		foreach($exjson as $j) {
			$json[$j] = true;
		}
		file_put_contents($file, json_encode($json, JSON_PRETTY_PRINT));
	}
	if (strpos($cbdata, 'editTypes_') === 0) {
		$e = explode("-", str_replace("editTypes_", '', $cbdata));
		$type = strtolower($e[0]);
		$set = round($e[1]);
		if ($type == "all") {
			foreach ($json as $m => $s) {
				$json[$m] = $set;
			}
		} else {
			$json[$type] = $set;
		}
		file_put_contents($file, json_encode($json, JSON_PRETTY_PRINT));
	}
	foreach ($json as $type => $active) {
		if ($active) {
			$e = false;
			$active = true;
		} else {
			$e = true;
			$active = false;
		}
		$emo = $c[json_encode($active)];
		$extext = $type;
		$type[0] = strtoupper($type[0]);
		$menu[] = [
			[
				"text" => "$type $emo",
				"callback_data" => "editTypes_$extext-$e"
			]
		];
	}
	$menu[] = [
		[
			"text" => "Disattiva",
			"callback_data" => "editTypes_all-0"
		],
		[
			"text" => "Attiva",
			"callback_data" => "editTypes_all-1"
		]
	];
	$menu[] = [
		[
			"text" => "Fatto",
			"callback_data" => "fatto"
		]
	];
	if ($cmd) {
		dm($chatID, $msgID);
		sm($chatID, "Attiva/Disattiva l'utilizzo dei tipi di sondaggi:", $menu);
	} else {
		editMenu($chatID, $cbmid, $menu);
		cb_reply($cbid, '', false);
	}
	die;
}

if ($cmd == "shop" or strpos($cbdata, 'editShop') === 0) {
	dm($chatID, $msgID);
	$file = "/home/masterpoll-documents/shop.json";
	$numtobool = [
		1 => true,
		0 => false
	];
	if (file_exists($file)) {
		$json = json_decode(file_get_contents($file), true);
	} else {
		$json = [];
		file_put_contents($file, json_encode($json, JSON_PRETTY_PRINT));
	}
	if (strpos($cbdata, 'editShop_') === 0) {
		$e = explode("-", str_replace("editShop_", '', $cbdata));
		$type = $e[0];
		$set = round($e[1]);
		if ($type == "all") {
			foreach ($json as $m => $s) {
				$json[$m]['available'] = $numtobool[$set];
			}
		} else {
			$json[$type]['available'] = $numtobool[$set];
		}
		file_put_contents($file, json_encode($json, JSON_PRETTY_PRINT));
	}
	foreach ($json as $art => $infos) {
		$active = $infos['available'];
		if ($active) {
			$e = false;
			$active = true;
		} else {
			$e = true;
			$active = false;
		}
		$emo = $c[json_encode($active)];
		$menu[] = [
			[
				"text" => "$art $emo",
				"callback_data" => "editShop_$art-$e"
			]
		];
	}
	$menu[] = [
		[
			"text" => "Disattiva",
			"callback_data" => "editShop_all-0"
		],
		[
			"text" => "Attiva",
			"callback_data" => "editShop_all-1"
		]
	];
	$menu[] = [
		[
			"text" => "Fatto",
			"callback_data" => "fatto"
		]
	];
	if ($cmd) {
		sm($chatID, "Attiva/Disattiva i prodotti sul negozio:", $menu);
	} else {
		editMenu($chatID, $cbmid, $menu);
		cb_reply($cbid, '', false);
	}
	die;
}

if (strpos($cmd, "addpl ") === 0) {
    $pl = str_replace("addpl ", '', $cmd);
    if (file_exists($f['plugins'])) {
        $pls = json_decode(file_get_contents($f['plugins']), true);
    } else {
        $pls = [];
    }
    if (!is_array($pls)) {
        $tot = "Errore: " . $f['plugins'] . " non è un array. \nFile corrotto: aggiustalo o ricrealo.";
    } else {
        $tot = bold("Plugin aggiunto!");
        $pls[$pl] = true;
        file_put_contents($f['plugins'], json_encode($pls, JSON_PRETTY_PRINT));
    }
    sm($chatID, $tot);
    die;
}

if ($cmd == "plugins" or strpos($cbdata, 'selectPlugins') === 0 or strpos($cbdata, 'editPlugins') === 0 or strpos($cbdata, 'orderPlugins') === 0 or strpos($cbdata, 'removePlugins') === 0) {
	dm($chatID, $msgID);
	$file = $f['plugins'];
	$numtobool = [
		1 => true,
		0 => false
	];
	if (file_exists($file)) {
		$json = json_decode(file_get_contents($file), true);
	} else {
		$json = [];
		file_put_contents($file, json_encode($json, JSON_PRETTY_PRINT));
	}
	if (strpos($cbdata, 'editPlugins_') === 0) {
		cb_reply($cbid, '', false);
		$e = explode("-", str_replace("editPlugins_", '', $cbdata));
		$type = $e[0];
		$selected = $type;
		$set = round($e[1]);
		if (!$set) {
			$dis = "dis";
		} else {
			$dis = "";
		}
		if ($type == "all") {
			$sfile = "All files";
		} else {
			$sfile = "The file '$type'";
		}
		cb_reply($cbid, "$sfile has been $dis" . "actived from plugins list.", false);
		if ($type == "all") {
			foreach ($json as $m => $s) {
				$json[$m] = $numtobool[$set];
			}
		} else {
			$json[$type] = $numtobool[$set];
		}
		file_put_contents($file, json_encode($json, JSON_PRETTY_PRINT));
	} elseif (strpos($cbdata, 'orderPlugins_') === 0) {
		$e = explode("-", str_replace("orderPlugins_", '', $cbdata));
		$type = $e[0];
		$position = round($e[1]) - 1;
		cb_reply($cbid, "The file '$type' has been moved plugins list.", false);
		$plugins = array_keys($json);
		foreach ($plugins as $num => $tpl) {
			if ($tpl == $type) $exposition = $num;
		}
		$copy = $plugins[$position];
		$plugins[$position] = $type;
		$plugins[$exposition] = $copy;
		foreach ($plugins as $num => $tpl) {
			$newjson[$tpl] = $json[$tpl];
		}
		$json = $newjson;
		file_put_contents($file, json_encode($json, JSON_PRETTY_PRINT));
		$selected = $type;
	} elseif (strpos($cbdata, 'removePlugins_') === 0) {
		$type = str_replace("removePlugins_", '', $cbdata);
		cb_reply($cbid, "The file '$type' has been removed from plugins list.", false);
		unset($json[$type]);
		file_put_contents($file, json_encode($json, JSON_PRETTY_PRINT));
	} elseif (strpos($cbdata, 'selectPlugins_') === 0) {
		$selected = str_replace("selectPlugins_", '', $cbdata);
	}
	$rnum = 0;
	foreach ($json as $art => $active) {
		$rnum = $rnum + 1;
		if ($active) {
			$e = false;
			$active = true;
		} else {
			$e = true;
			$active = false;
		}
		$emo = $c[json_encode($active)];
		$b = $art;
		if ($selected !== $art) {
			$b .= $emo;
			$bcb = "selectPlugins_$art";
		} else {
			$bcb = "selectPlugins";
		}
		$menu[] = [
			[
				"text" => $b,
				"callback_data" => $bcb
			]
		];
		if ($selected == $art) {
			$sottomenu[] = [
				"text" => "$emo",
				"callback_data" => "editPlugins_$art-$e"
			];
			if (array_keys($json)[0] !== $art) {
				$sottomenu[] = [
					"text" => "⬆️",
					"callback_data" => "orderPlugins_$art-" . ($rnum - 1)
				];
			}
			if (array_keys($json)[count($json) - 1] !== $art){
				$sottomenu[] = [
					"text" => "⬇️",
					"callback_data" => "orderPlugins_$art-" . ($rnum + 1)
				];
			}
			$sottomenu[] = [
				"text" => "🗑",
				"callback_data" => "removePlugins_$art"
			];
			$menu[] = $sottomenu;
			unset($sottomenu);
		}
	}
	$menu[] = [
		[
			"text" => "Bot Off",
			"callback_data" => "editPlugins_all-0"
		],
		[
			"text" => "Bot On",
			"callback_data" => "editPlugins_all-1"
		]
	];
	$menu[] = [
		[
			"text" => "Fatto",
			"callback_data" => "fatto"
		]
	];
	if ($cmd) {
		sm($chatID, "Attiva/Disattiva plugins:", $menu);
	} else {
		editMenu($chatID, $cbmid, $menu);
		cb_reply($cbid, '', false);
	}
	die;
}

if ($cmd == "ping" and $isadmin) {
	$time_start = microtime(true);
	$dev[0] = sm($chatID, code("Pong 0"));
	$time_end = microtime(true);
	$execution_time = number_format($time_end - $time_start, 3);
	$time_start1 = microtime(true);
	$dev[1] = sm($chatID, code("Pong 1"));
	$time_end1 = microtime(true);
	$execution_time1 = number_format($time_end1 - $time_start1, 3);
	$total_time = number_format($time_end1 - $times['start'], 3);
	sm($chatID, bold("Performance del Bot🤖 \n") . "<code>sendMessage:  $execution_time secondi\nsendMessage1: $execution_time1 secondi\nTotale:       $total_time secondi </>\n\n" . code(json_encode($dev, JSON_PRETTY_PRINT)));
	die;
}

if ($cbdata == "fatto") {
    cb_reply($cbid, '👍', false);
	dm($chatID, $cbmid);
    die;
}

if ($cmd == "geterror") {
	$logs = file_get_contents("/var/log/masterpoll/bot$botID.log");
	$logs = explode("\n[", $logs);
	$lastlog = "[" . $logs[count($logs) - 1];
	if ($lastlog == "[") $lastlog = "Nessun log registrato...";
	sm($chatID, "Error logs: \n" . substr($lastlog, 0, 4010));
	die;
}

if ($cmd == "clear_logs") {
	file_put_contents("/var/log/masterpoll/bot$botID.log", '');
	sm($chatID, "🗑 Logs puliti!");
	die;
}

if ($cmd == "error") {
    call_error("Errore di prova");
    die;
}

if ($cmd == "dberror") {
    db_query("TEST ERRORI AL DB");
    die;
}