<?php

function noflood($word = false) {
	if ($word == false) return null;
	return "1";
}

function updatePoll ($poll_id, $creator, $tranne = []) {
	global $config;
	global $redis;
	global $botID;
	if (!isset($redis) or !$config['usa_redis']) {
		return ['ok' => false, 'error_code' => 502, 'description' => "Redis not work", 'updates' => [], 'method' => "updatePoll-1"];
	}
	$get = rget("update-$botID-$poll_id-$creator");
	if ($get['ok']) {
		$get = $get['result'];
	} else {
		if (isset($get['method'])) {
			$get['method']["updatePoll-2"] = $get['method'];
		} else {
			$get['method'] = "updatePoll-2";
		}
		return $get;
	}
	if ($get) {
		$set = rset("update-$botID-$poll_id-$creator", 100);
		if (!$set['ok']) {
			if (isset($set['method'])) {
				$set['method']["updatePoll-3"] = $set['method'];
			} else {
				$set['method'] = "updatePoll-3";
			}
			return $set;
		}
		$get = rget("update-$botID-$poll_id-$creator");
		if ($get['ok']) {
			$get = $get['result'];
		} else {
			if (isset($get['method'])) {
				$get['method']["updatePoll-4"] = $get['method'];
			} else {
				$get['method'] = "updatePoll-4";
			}
			return $get;
		}
		if ($get) {
			$del = rdel("update-$botID-$poll_id-$creator");
			if ($del['ok']) {
				$del = $del['result'];
			} else {
				if (isset($del['method'])) {
					$del['method']["updatePoll-5"] = $del['method'];
				} else {
					$del['method'] = "updatePoll-5";
				}
				return $del;
			}
			return ['ok' => false, 'error_code' => 429, 'description' => "Too Many Request: retry after 1"];
		}
	}
	if (isset($tranne)) {
		if (!is_array($tranne)) {
			$tranne = [$tranne];
		}
	} else {
		$tranne = [];
	}
	$p = sendPoll($poll_id, $creator);
	if ($p['ok']) {
		$p = $p['result'];
	} else {
		if (isset($p['method'])) {
			$p['method']["updatePoll-6"] = $p['method'];
		} else {
			$p['method'] = "updatePoll-6";
		}
		return $p;
	}
	if ($p['status'] == "deleted") {
		return ['ok' => false, 'error_code' => 401, 'description' => "Access denied: can't update a deleted poll", 'method' => "updatePoll-7"];
	} elseif (!$p['status']) {
		return ['ok' => false, 'error_code' => 404, 'description' => "Not Found: poll not found", 'method' => "updatePoll-8"];
	}
	if (!is_array($p['messages'])) {
		return ['ok' => false, 'error_code' => 400, 'description' => "Bad Request: messages must be an array", 'updates' => [], 'method' => "updatePoll-9"];
	}
	if (!isset($p['messages'][$botID])) {
		return ['ok' => true, 'updates' => [], 'message' => "No message saved in this bot", 'method' => "updatePoll-10"];
	}
	$p['messages'][$botID] = array_values($p['messages'][$botID]);
	if (!isset($p['messages'][$botID])) {
		return ['ok' => true, 'updates' => [], 'message' => "No message saved in this bot (after auto-fix)", 'method' => "updatePoll-11"];
	}
	$ex = rand(1, 99);
	$set = rset("update-$botID-$poll_id-$creator", $ex);
	if ($set['ok']) {
		$set = $set['result'];
	} else {
		if (isset($set['method'])) {
			$set['method']["updatePoll-12"] = $set['method'];
		} else {
			$set['method'] = "updatePoll-12";
		}
		return $set;
	}
	$clang = getLanguage($creator);
	foreach ($p['messages'][$botID] as $m) {
		$get = rget("update-$botID-$poll_id-$creator");
		if ($get['ok']) {
			$get = $get['result'];
		} else {
			if (isset($get['method'])) {
				$get['method']["updatePoll-13"] = $get['method'];
			} else {
				$get['method'] = "updatePoll-13";
			}
			return $get;
		}
		if ($get == $ex) {
			if (!$m['id']) {
			} elseif (!is_numeric($m['id']) and !$m['type']) {
				// Messaggio inline normale
				if (!in_array($m['id'], $tranne)) {
					if (!isset($nr) or !isset($nmenu)) {
						$npoll = pollToMessage($p, false);
						if ($npoll['ok']) {
							$nr = $npoll['text'];
							$ndwpp = $npoll['disable_web_preview'];
							$nmenu = $npoll['menu'];
						}
					}
					if (isset($nr) and isset($nmenu)) {
						$em = editMsg($m['chat'], $nr, $m['id'], $nmenu, 'def', $ndwpp);
						$upds[] = $m;
					}
				}
			} elseif (!is_numeric($m['id']) and !$m['chat'] and is_numeric($m['type'])) {
				// Messaggio inline buh
				if (!in_array($m['id'], $tranne)) {
					if (!isset($nr) or !isset($nmenu)) {
						$npoll = pollToMessage($p, false);
						if ($npoll['ok']) {
							$nr = $npoll['text'];
							$ndwpp = $npoll['disable_web_preview'];
							$nmenu = $npoll['menu'];
						}
					}
					if (isset($nr) and isset($nmenu)) {
						$em = editMsg($m['chat'], $nr, $m['id'], $nmenu, 'def', $ndwpp);
						$upds[] = $m;
					}
				}
			} elseif (!is_numeric($m['id']) and $m['chat'] and !$m['type']) {
				// Messaggio normale
				if (!in_array($m['id'], $tranne)) {
					if (!isset($nr) or !isset($nmenu)) {
						$npoll = pollToMessage($p, false);
						if ($npoll['ok']) {
							$nr = $npoll['text'];
							$ndwpp = $npoll['disable_web_preview'];
							$nmenu = $npoll['menu'];
						}
					}
					if (isset($nr) and isset($nmenu)) {
						$em = editMsg($m['chat'], $nr, $m['id'], $nmenu, 'def', $ndwpp);
						$upds[] = $m;
					}
				}
			} elseif ($m['id'] and $m['chat'] and $m['type'] === "moderate") {
				// Messaggio di moderate
				if (!in_array($m['id'], $tranne)) {
					if ($m['chat'] > 0) {
						$modlang = getLanguage($m['chat']);
					} else {
						$modlang = $clang;
					}
					if (!isset($mr[$modlang]) or !isset($mmenu[$modlang])) {
						$mpoll = pollToMessage($p, 'moderate', ['lang' => $modlang]);
						if ($mpoll['ok']) {
							$ndwpp = $mpoll['disable_web_preview'];
							$mr[$modlang] = $mpoll['text'];
							$mmenu[$modlang] = $mpoll['menu'];
						}
					}
					if (isset($mr[$modlang]) and isset($mmenu[$modlang])) {
						$em = editMsg($m['chat'], $mr[$modlang], $m['id'], $mmenu[$modlang], 'def', $ndwpp);
						$upds[] = $m;
					}
				}
			} elseif ($m['id'] and $m['type'] === "options") {
				// Messaggio in impostazioni (da non aggiornare)
			} elseif ($m['id'] and $m['type'] === "deleted") {
				// Messaggio eliminato (da non aggiornare)
			} elseif ($m['id'] and $m['type'] === "addadmin") {
				// Messaggio di invito agli amministratori
				if (!in_array($m['id'], $tranne)) {
					if (!isset($addadr) or !isset($menuaddadmins)) {
						$menuaddadmins[0] = [
							[
								"text" => getTranslate('administratorsAddMe', false, $clang),
								"callback_data" => "addMeAdmin_$poll_id-$creator"
							]
						];
						$addadr = getTranslate('administratorInvite', [$p['title']], $clang);
					}
					if (isset($addadr) and isset($menuaddadmins)) {
						$em = editMsg($m['chat'], $addadr, $m['id'], $menuaddadmins, 'def', true);
						$upds[] = $m;
					}
				}
			} elseif ($m['id'] and $m['type'] === "button") {
				// Messaggio con bottone "Vota"
				if (!in_array($m['id'], $tranne)) {
					if (!isset($nr) or !isset($nmenu)) {
						$npoll = pollToMessage($p, false);
						if ($npoll['ok']) {
							$nr = $npoll['text'];
							$ndwpp = $npoll['disable_web_preview'];
							$nmenu = $npoll['menu'];
						}
					}
					if (!isset($menub)) {
						if ($p['type'] == "board") {
							$urlor = "cburl-" . bot_encode("board_" . $p['poll_id'] . "-" . $p['creator']);
						} else {
							$urlor = "cburl-" . bot_encode("mypoll_" . $p['poll_id'] . "-" . $p['creator']);
						}
						$menub[] = [
							[
								"text" => getTranslate('buttonVote', false, $clang),
								"callback_data" => $urlor
							]
						];
						if ($p['settings']['sharable']) {
							$menub[] = [
								[
									"text" => getTranslate('share', false, $clang),
									"callback_data" => "cburl-" . bot_encode("share_" . $p['poll_id'] . "-" . $p['creator'])
								],
							];
						}
					}
					if (isset($nr) and isset($menub)) {
						$em = editMsg($m['chat'], $nr, $m['id'], $menub, 'def', $ndwpp);
						$upds[] = $m;
					}
				}
			} elseif (is_numeric($m['id']) and $m['chat'] and $m['type'] === true and $m['chat'] == $creator) {
				// Messaggio da Admin in chat privata
				if (!in_array($m['id'], $tranne)) {
					if ($m['chat'] > 0) {
						$rlang = getLanguage($m['chat']);
					} else {
						$rlang = $clang;
					}
					if (!isset($r[$rlang]) or !isset($menu[$rlang])) {
						$poll = pollToMessage($p, true, ['lang' => $rlang]);
						if ($poll['ok']) {
							$r[$rlang] = $poll['text'];
							$ndwpp = $poll['disable_web_preview'];
							$menu[$rlang] = $poll['menu'];
						}
					}
					if (isset($r[$rlang]) and isset($menu[$rlang])) {
						$em = editMsg($m['chat'], $r[$rlang], $m['id'], $menu[$rlang], 'def', $ndwpp);
						$upds[] = $m;
					}
				}
			} elseif (is_numeric($m['id']) and $m['type'] == $creator) {
				// Messaggio da Creatore in chat privata
				if (!in_array($m['id'], $tranne)) {
					$rlang = getLanguage($m['type']);
					if (!isset($r[$rlang]) or !isset($menu[$rlang])) {
						$poll = pollToMessage($p, true, ['lang' => $rlang]);
						if ($poll['ok']) {
							$r[$rlang] = $poll['text'];
							$ndwpp = $poll['disable_web_preview'];
							$menu[$rlang] = $poll['menu'];
						}
					}
					if (isset($r[$rlang]) and isset($menu[$rlang])) {
						$em = editMsg($m['chat'], $r[$rlang], $m['id'], $menu[$rlang], 'def', $ndwpp);
						$upds[] = $m;
					}
				}
			} elseif (is_numeric($m['id']) and !$m['type'] and is_numeric($m['chat'])) {
				// Messaggio normale in chat privata
				if (!in_array($m['id'], $tranne)) {
					if (!isset($nr) or !isset($nmenu)) {
						$npoll = pollToMessage($p, false);
						if ($npoll['ok']) {
							$nr = $npoll['text'];
							$ndwpp = $npoll['disable_web_preview'];
							$nmenu = $npoll['menu'];
						}
					}
					if (isset($nr) and isset($nmenu)) {
						$em = editMsg($m['chat'], $nr, $m['id'], $nmenu, 'def', $ndwpp);
						$upds[] = $m;
					}
				}
			} elseif (is_numeric($m['id']) and is_numeric($m['type']) and !isset($m['chat'])) {
				// Messaggio normale in chat privata
				if (!in_array($m['id'], $tranne)) {
					if (!isset($nr) or !isset($nmenu)) {
						$npoll = pollToMessage($p, false);
						if ($npoll['ok']) {
							$nr = $npoll['text'];
							$ndwpp = $npoll['disable_web_preview'];
							$nmenu = $npoll['menu'];
						}
					}
					if (isset($nr) and isset($nmenu)) {
						$em = editMsg($m['chat'], $nr, $m['id'], $nmenu, 'def', $ndwpp);
						$upds[] = $m;
					}
				}
			} else {
				// Messaggio da config
				call_error("Strange update of poll: " . json_encode($m));
			}
			if (isset($em['error_code'])) {
				if ($em['description'] == "Forbidden: MESSAGE_AUTHOR_REQUIRED") {
					insertMessage($p, $m['id'], "deleted", $m['chat']);
				} elseif ($em['description'] == "Bad Request: MESSAGE_ID_INVALID") {
					insertMessage($p, $m['id'], "deleted", $m['chat']);
				} elseif ($em['description'] == "Forbidden: user is deactivated") {
					insertMessage($p, $m['id'], "deleted", $m['chat']);
				} elseif ($em['description'] == "Bad Request: message identifier is not specified") {
					insertMessage($p, $m['id'], "deleted", $m['chat']);
				} elseif ($em['description'] == "Bad Request: message is not modified: specified new message content and reply markup are exactly the same as a current content and reply markup of the message") {
				} else {
				}
			}
		} else {
			$finito = true;
		}
	}
	if ($finito) {
		return ['ok' => true, 'updates' => $upds, 'message' => 'Update interrupted', 'method' => "updatePoll-14"];
	} else {
		sleep(1);
		$del = rdel("update-$botID-$poll_id-$creator");
		if ($del['ok']) {
			$del = $del['result'];
		} else {
			if (isset($del['method'])) {
				$del['method']["updatePoll-15"] = $del['method'];
			} else {
				$del['method'] = "updatePoll-15";
			}
			return $del;
		}
	}
	return ['ok' => true, 'updates' => $upds, 'message' => "Updated all messages of this bot", 'method' => "updatePoll-16"];
}

if (strpos($cbdata, "cburl-") === 0) {
	$ur = str_replace("cburl-", '', $cbdata);
	if (!$ur) {
		call_error("cburl vuoto: $cbdata\n<code>$userID</>");
		cb_reply($cbid, getTranslate('updatingPoll'), true);
		die;
	}
	$url = "https://t.me/" . $config['username_bot'] . "?start=$ur";
	$r = bot_decode($ur);
	$r2 = base64_decode($ur);
	if (isset($isjsonup)) unset($isjsonup);
	if ($r) {
		$json = json_decode($r, true);
		if (isset($json['poll_id'])) {
			$poll_id = $json['poll_id'];
			$creator = $json['creator'];
			$p = sendPoll($poll_id, $creator);
			if ($p['ok']) {
				$p = $p['result'];
			} else {
				cb_reply($cbid, "❌ " . getTranslate('generalError'), false);
				die;
			}
			if ($typechat !== "private") insertMessage($p, $cbmid, false, $chatID);
		} elseif (strpos($r, "list_") === 0) {
			$e = explode("-", str_replace("list_", "", $r));
			$poll_id = $e[0];
			$creator = $e[1];
			$p = sendPoll($poll_id, $creator);
			if ($p['ok']) {
				$p = $p['result'];
			} else {
				cb_reply($cbid, "❌ " . getTranslate('generalError'), false);
				die;
			}
			if (in_array($typechat, ['group', 'supergroup', 'channel'])) {
				$admin = false;
			} elseif (isAdmin($userID, $p)['result'] and is_numeric($cbmid)) {
				$admin = true;
			} else {
				$admin = false;
			}
			//insertMessage($p, $cbmid, false, $chatID);
		} elseif (strpos($r, "mypoll_") === 0) {
			$e = explode("-", str_replace("mypoll_", "", $r));
			$poll_id = $e[0];
			$creator = $e[1];
			$p = sendPoll($poll_id, $creator);
			if ($p['ok']) {
				$p = $p['result'];
			} else {
				cb_reply($cbid, "❌ " . getTranslate('generalError'), false);
				die;
			}
			if (in_array($typechat, ['group', 'supergroup', 'channel'])) {
				$admin = false;
			} elseif (isAdmin($userID, $p)['result'] and is_numeric($cbmid)) {
				$admin = true;
			} else {
				$admin = false;
			}
			if ($typechat !== "private") insertMessage($p, $cbmid, false, $chatID);
		} elseif (strpos($r, "share_") === 0) {
			$e = explode("-", str_replace("share_", "", $r));
			$poll_id = $e[0];
			$creator = $e[1];
			$p = sendPoll($poll_id, $creator);
			if ($p['ok']) {
				$p = $p['result'];
			} else {
				cb_reply($cbid, "❌ " . getTranslate('generalError'), false);
				die;
			}
			if (in_array($typechat, ['group', 'supergroup', 'channel'])) {
				$admin = false;
			} elseif (isAdmin($userID, $p)['result'] and is_numeric($cbmid)) {
				$admin = true;
			} else {
				$admin = false;
			}
			insertMessage($p, $cbmid, false, $chatID);
		} elseif (strpos($r, "board_") === 0) {
			$e = explode("-", str_replace("board_", "", $r));
			$poll_id = $e[0];
			$creator = $e[1];
			$p = sendPoll($poll_id, $creator);
			if ($p['ok']) {
				$p = $p['result'];
			} else {
				cb_reply($cbid, "❌ " . getTranslate('generalError'), false);
				die;
			}
			if (in_array($typechat, ['group', 'supergroup', 'channel'])) {
				$admin = false;
			} elseif (isAdmin($userID, $p)['result'] and is_numeric($cbmid)) {
				$admin = true;
			} else {
				$admin = false;
			}
			insertMessage($p, $cbmid, false, $chatID);
		} elseif (strpos($r, "append_") === 0) {
			$e = explode("-", str_replace("append_", "", $r));
			$poll_id = $e[0];
			$creator = $e[1];
			$p = sendPoll($poll_id, $creator);
			if ($p['ok']) {
				$p = $p['result'];
			} else {
				cb_reply($cbid, "❌ " . getTranslate('generalError'), false);
				die;
			}
			if (in_array($typechat, ['group', 'supergroup', 'channel'])) {
				$admin = false;
			} elseif (isAdmin($userID, $p)['result'] and is_numeric($cbmid)) {
				$admin = true;
			} else {
				$admin = false;
			}
			insertMessage($p, $cbmid, false, $chatID);
		} elseif ($r2) {
			cb_reply($cbid, 'Message too old...');
			die;
		}
		if (!isset($isjsonup)) {
			cb_url($cbid, $url);
		} else {
			cb_reply($cbid, '');
		}
		if ($poll_id and $creator) $update_poll = "poll_$poll_id-$creator";
	} else {
		cb_reply($cbid, '❌ Error auto-reported...', true);
		call_error("Errore in cburl: $cbdata\nURL: $url \nEncode: $ur \nDecode: " . bot_decode($ur));
		die;
	}
}

if (strpos($update_poll, 'poll_') === 0) {
	$e = explode("-", str_replace('poll_', '', $update_poll));
	updatePoll($e[0], $e[1]);
	die;
}

if (strpos($cmd, 'spoll_') === 0 and ($isadmin or $u['settings']['admin_perms']['spoll'])) {
	$config['json_payload'] = false;
	$e = explode("-", str_replace('spoll_', '', $cmd));
	$p = sendPoll($e[0], $e[1]);
	if (!$p['ok']) {
		sm($chatID, "Nope: " . code(json_encode($p)));
		die;
	} else {
		$p = $p['result'];
	}
	$m = pollToMessage($p, true, ['lang' => $lang]);
	if (!$m['ok']) {
		sm($chatID, "Nope: " . code(json_encode($m)));
		die;
	}
	$ok = sm($chatID, $m['text'], $m['menu']);
	if (!$ok['ok']) {
		sm($chatID, "OK: " . $ok['description']);
	}
	die;
}

if (strpos($cmd, 'infopoll_') === 0 and ($isadmin or $u['settings']['admin_perms']['infopoll'])) {
	$e = explode("-", str_replace('infopoll_', '', $cmd));
	$p = sendPoll($e[0], $e[1]);
	if (!$p['ok']) {
		sm($chatID, "Nope: " . code(json_encode($p)));
		die;
	} else {
		$p = $p['result'];
	}
	$text = "ℹ️ " . code("/spoll_" . $e[0] . "-" . $e[1]);
	$text .= "\n️👤 " . tag($e[1], "Owner", " [{$e[1]}]");
	if ($p['status'] == "deleted") {
		$text .= "\nSondaggio eliminato";
	} elseif (in_array($p['status'], ["open", "closed"])) {
		if ($p['status']) {
			$text .= "\nSondaggio aperto";
		} else {
			$text .= "\nSondaggio chiuso";
		}
		$text .= "\nTipo: " . $p['type'];
		if ($p['anonymous']) {
			$anemo = "✅";
		} else {
			$anemo = "☑️";
		}
		$text .= "\nVoto anonimo: $anemo";
		if ($p['title']) $text .= "\nTitolo: " . htmlspecialchars($p['title']);
		if ($p['description']) $text .= "\nDesc: " . htmlspecialchars($p['description']);
		if (isset($p['settings']['max_voters'])) {
			$limit = "/" . $p['settings']['max_voters'];
		}
		$text .= "\nIscritti al sondaggio: " . $p['votes'] . $limit;
	} else {
		$text .= "\nSondaggio rovinato";
	}
	$config['json_payload'] = false;
	$ok = sm($chatID, $text);
	if (!$ok['ok']) {
		sm($chatID, "OK: " . $ok['description']);
	}
	die;
}

if (strpos($cmd, 'update_') === 0 and $isadmin) {
	$e = explode("-", str_replace('update_', '', $cmd));
	$upds = updatePoll($e[0], $e[1]);
	sm($chatID, code(json_encode($upds)));
	sm($chatID, getTranslate('done'));
	die;
}

if ($cmd == "cancel") {
	if (isset($u['settings']['createpoll'])) {
		unset($u['settings']['createpoll']);
		$canceld = true;
	}
	$q1 = db_query("UPDATE utenti SET settings = ?, page = ? WHERE user_id = ?", [json_encode($u['settings']), "", $userID], "no");
	if (!$q1['ok']) {
		sm($chatID, "❌ " . getTranslate('generalError'));
		die;
	}
	ssponsor($userID, $lang, $u['settings']);
	if (isset($canceld)) {
		$t = getTranslate('hasCanceled');
	} elseif ($u['page'] !== '') {
		$t = getTranslate('canceledThat');
	} else {
		$t = getTranslate('nothingToCancel');
	}
	sm($chatID, $t, 'nascondi');
	die;
}

if ($cmd == "bug") {
	if ($reply) {
		$config['json_payload'] = false;
		$m = fw($config['console'], $chatID, $rmsgID);
		if ($m['ok'] !== true) {
			sm($chatID, getTranslate('bugReportFail'));
			die;
		}
		$menu[] = [
			[
				"text" => "Fixato ✅",
				"callback_data" => "fix_$chatID-$rmsgID"
			]
		];
		$menu[] = [
			[
				"text" => "Non valido 💤",
				"callback_data" => "unknown_$chatID-$rmsgID"
			]
		];
		$menu[] = [
			[
				"text" => "Delete 🗑",
				"callback_data" => "notfix_$chatID-$rmsgID"
			]
		];
		$config['logs_token'] = str_replace('bot', '', $api);
		sm($config['console'], "#BugReport #id$userID", $menu);
		sm($chatID, getTranslate('bugReportDone'));
	} else {
		$q1 = db_query("UPDATE utenti SET page = ? WHERE user_id = ?", ["bugReport", $userID], "no");
		if (!$q1['ok']) {
			sm($chatID, "❌ " . getTranslate('generalError'));
			die;
		}
		sm($chatID, getTranslate('bugReport'));
	}
	die;
}

if ($u['page'] == "bugReport" and !$cmd and !$cbid) {
	$q1 = db_query("UPDATE utenti SET page = ? WHERE user_id = ?", ["", $userID], "no");
	if (!$q1['ok']) {
		sm($chatID, "❌ " . getTranslate('generalError'));
		die;
	}
	$config['json_payload'] = false;
	$m = fw($config['console'], $chatID, $msgID);
	if ($m['ok'] !== true) {
		sm($chatID, getTranslate('bugReportFail'));
		die;
	}
	$menu[] = [
		[
			"text" => "Fixato ✅",
			"callback_data" => "fix_$chatID-$msgID"
		]
	];
	$menu[] = [
		[
			"text" => "Non valido 💤",
			"callback_data" => "unknown_$chatID-$msgID"
		]
	];
	$menu[] = [
		[
			"text" => "Delete 🗑",
			"callback_data" => "notfix_$chatID-$msgID"
		]
	];
	$config['logs_token'] = str_replace('bot', '', $api);
	sm($config['console'], "#BugReport #id$userID", $menu);
	sm($chatID, getTranslate('bugReportDone'));
	die;
}

if ($isadmin and strpos($cmd, "pollsetlang_") === 0) {
	$e = explode("-", str_replace('pollsetlang_', '', $cmd));
	$poll_id = $e[0];
	$creator = $e[1];
	$newlang = $e[2];
	$poll = sendPoll($poll_id, $creator);
	if ($poll['ok']) {
		$poll = $poll['result'];
	} else {
		sm($chatID, "❌ " . getTranslate('generalError'));
		die;
	}
	$poll['settings']['language'] = $newlang;
	$q = db_query("UPDATE polls SET settings = ? WHERE user_id = ? and poll_id = ?", [json_encode($poll['settings']), $creator, $poll_id], "no");
	sm($chatID, "Mandato: " . code(json_encode($q, JSON_PRETTY_PRINT)));
}

if (strpos($cbdata, "//anonymous ") === 0 and $isadmin) {
	$e = explode("-", str_replace("//anonymous ", '', $cbdata));
	$poll_id = $e[0];
	$creator = $e[1];
	$anonymous = $e[2];
	$r = db_query("UPDATE polls SET anonymous = ? WHERE poll_id = ? and user_id = ?", [$anonymous, $poll_id, $creator], "no");
	if (!$r['ok']) {
		cb_reply($cbid, "❌ " . getTranslate('generalError'), false);
		die;
	}
	$cbdata = "/option $poll_id-$creator";
}

if (strpos($cbdata, "cancelOption_") === 0) {
	$e = explode("-", str_replace("cancelOption_", '', $cbdata));
	$poll_id = $e[0];
	$creator = $e[1];
	$type = $e[2];
	if ($u['settings']['cache']) {
		if ($u['settings']['cache'] !== $cbmid) editMenu($chatID, $u['settings']['cache'], []);
		unset($u['settings']['cache']);
	}
	$u['page'] = "";
	$q1 = db_query("UPDATE utenti SET page = ?, settings = ? WHERE user_id = ?", ['', json_encode($u['settings']), $userID], "no");
	if (!$q1['ok']) {
		cb_reply($cbid, "❌ " . getTranslate('generalError'), false);
		die;
	}
	$cbdata = "coptions_$poll_id-$creator-$type";
}

if (strpos($cbdata, "vote:") === 0) {
	cb_reply($cbid, getTranslate('updatingPoll'), true);
	$up = str_replace("vote:", "", $cbdata);
	$e = explode("-", $up, 3);
	$poll_id = $e[0];
	$creator = $e[1];
	$cbdata = "update_$poll_id-$creator-true";
}

if (strpos($cbdata, "v:") === 0) {
	$config['json_payload'] = false;
	$up = str_replace("v:", "", $cbdata);
	$e = explode("-", $up, 3);
	$poll_id = $e[0];
	$creator = $e[1];
	$choice = $e[2];
	$p = sendPoll($poll_id, $creator);
	if ($p['ok']) {
		$p = $p['result'];
	} else {
		cb_reply($cbid, "❌ " . getTranslate('generalError'), true);
		if ($config['devmode']) {
			call_error("Error " . $p['error_code'] . ": " . htmlspecialchars($p['description']) . "\nMessage: " . $p['message'] . "\nMethod: " . $p['method']);
		}
		die;
	}
	$choice = array_keys($p['choice'])[round($choice)];
	if ($p['status'] == "open") {
		if (!isAdmin($userID, $p)['result']) {
			$cu = db_query("SELECT banlist,bottino FROM utenti WHERE user_id = ?", [$creator], true);
			if ($cu['ok']) {
				$cu = $cu['result'];
			} else {
				if ($config['devmode']) {
					call_error("Error " . $cu['error_code'] . ": " . htmlspecialchars($cu['description']) . "\nMessage: " . $cu['message'] . "\nMethod: " . $cu['method']);
				}
				cb_reply($cbid, "❌ " . getTranslate('generalError'), true);
				die;
			}
			$banlist = json_decode($cu['banlist'], true);
			if (isset($banlist)) {
				if (isset($banlist[$userID])) {
					if (is_numeric($banlist[$userID])) {
						if ($banlist[$userID] <= time()) {
							unset($banlist[$userID]);
							$q1 = db_query("UPDATE utenti SET banlist = ? WHERE user_id = ?", [json_encode($banlist), $creator]);
							if (!$q1['ok']) {
								if ($config['devmode']) {
									call_error("Error " . $q1['error_code'] . ": " . htmlspecialchars($q1['description']) . "\nMessage: " . $q1['message'] . "\nMethod: " . $q1['method']);
								}
								cb_reply($cbid, "❌ " . getTranslate('generalError'), true);
								die;
							}
						} else {
							cb_reply($cbid, '🚷 ' . getTranslate('voteNotAllowed'), false);
							die;
						}
					} else {
						cb_reply($cbid, '🚷 ' . getTranslate('voteNotAllowed'), false);
						die;
					}
				}
			}
			if ($cu['bottino']) {
				$ban = getBottinoBan($userID);
				if ($ban['blacklist']) {
					cb_reply($cbid, '🚷 ' . getTranslate('voteNotAllowed'), false);
					die;
				}
			}
		} else {
			$admin = true;
		}
		if ($p['settings']['max_voters']) {
			if ($p['votes'] == $p['settings']['max_voters']) {
				$haveChoices = haveChoices($p, $userID);
				if ($haveChoices['ok']) {
					$haveChoices = $haveChoices['result'];
				} else {
					if ($config['devmode']) {
						call_error("Error " . $haveChoices['error_code'] . ": " . htmlspecialchars($haveChoices['description']) . "\nMessage: " . $haveChoices['message'] . "\nMethod: " . $haveChoices['method']);
					}
					cb_reply($cbid, "❌ " . getTranslate('generalError'), true);
					die;
				}
				if ($haveChoices) {} else {
					cb_reply($cbid, getTranslate('maxVotesLimit'), true);
					die;
				}
			}
		}
		if ($p['type'] == "vote") {
			$r = haveChoices($p, $userID);
			if ($r['ok']) {
				$r = $r['result'];
			} else {
				if ($config['devmode']) {
					call_error("Error " . $r['error_code'] . ": " . htmlspecialchars($r['description']) . "\nMessage: " . $r['message'] . "\nMethod: " . $r['method']);
				}
				cb_reply($cbid, "❌ " . getTranslate('generalError'), true);
				die;
			}
			if (!in_array($choice, $r)) {
				$q1 = removeAllChoices($p, $userID);
				if (!$q1['ok']) {
					if ($config['devmode']) {
						call_error("Error " . $q1['error_code'] . ": " . htmlspecialchars($q1['description']) . "\nMessage: " . $q1['message'] . "\nMethod: " . $q1['method']);
					}
					cb_reply($cbid, "❌ " . getTranslate('generalError'), true);
					die;
				}
				$q1 = addChoice(["poll_id" => $p['poll_id'], "creator" => $p['creator']], $userID, $choice);
				if (!$q1['ok']) {
					if ($config['devmode']) {
						call_error("Error " . $q1['error_code'] . ": " . htmlspecialchars($q1['description']) . "\nMessage: " . $q1['message'] . "\nMethod: " . $q1['method']);
					}
					cb_reply($cbid, "❌ " . getTranslate('generalError'), true);
					die;
				}
				$chosenChoice = getTranslate('callbackVotedFor', [$choice]);
			} else {
				$chosenChoice = getTranslate('callbackTookBack', [$choice]);
				$q1 = removeAllChoices($p, $userID);
				if (!$q1['ok']) {
					if ($config['devmode']) {
						call_error("Error " . $q1['error_code'] . ": " . htmlspecialchars($q1['description']) . "\nMessage: " . $q1['message'] . "\nMethod: " . $q1['method']);
					}
					cb_reply($cbid, "❌ " . getTranslate('generalError'), true);
					die;
				}
			}
		} elseif ($p['type'] == "quiz") {
			$r = haveChoices($p, $userID);
			if ($r['ok']) {
				$r = $r['result'];
			} else {
				cb_reply($cbid, "❌ " . getTranslate('generalError'), true);
				die;
			}
			if (!in_array($choice, $r)) {
				if (count($p['settings']['right_answers']) == 1) {
					$q1 = removeAllChoices($p, $userID);
					if (!$q1['ok']) {
						cb_reply($cbid, "❌ " . getTranslate('generalError'), true);
						die;
					}
				}
				$q1 = addChoice(["poll_id" => $p['poll_id'], "creator" => $p['creator']], $userID, $choice);
				if (!$q1['ok']) {
					cb_reply($cbid, "❌ " . getTranslate('generalError'), true);
					die;
				}
				$chosenChoice = getTranslate('callbackVotedFor', [$choice]);
			} else {
				$chosenChoice = getTranslate('callbackTookBack', [$choice]);
				$q1 = removeAllChoices($p, $userID);
				if (!$q1['ok']) {
					cb_reply($cbid, "❌ " . getTranslate('generalError'), true);
					die;
				}
			}
		} elseif ($p['type'] == "rating") {
			$r = haveChoices($p, $userID);
			if ($r['ok']) {
				$r = $r['result'];
			} else {
				cb_reply($cbid, "❌ " . getTranslate('generalError'), true);
				die;
			}
			if (!in_array($choice, $r)) {
				$q1 = removeAllChoices($p, $userID);
				if (!$q1['ok']) {
					cb_reply($cbid, "❌ " . getTranslate('generalError'), true);
					die;
				}
				$q1 = addChoice(["poll_id" => $p['poll_id'], "creator" => $p['creator']], $userID, $choice);
				if (!$q1['ok']) {
					cb_reply($cbid, "❌ " . getTranslate('generalError'), true);
					die;
				}
				$chosenChoice = getTranslate('callbackVotedFor', [$choice]);
			} else {
				$chosenChoice = getTranslate('callbackTookBack', [$choice]);
				$q1 = removeAllChoices($p, $userID);
				if (!$q1['ok']) {
					cb_reply($cbid, "❌ " . getTranslate('generalError'), true);
					die;
				}
			}
		} elseif ($p['type'] == "participation") {
			$r = haveChoices($p, $userID);
			if ($r['ok']) {
				$r = $r['result'];
			} else {
				cb_reply($cbid, "❌ " . getTranslate('generalError'), true);
				die;
			}
			if (!in_array($choice, $r)) {
				$q1 = removeAllChoices($p, $userID);
				if (!$q1['ok']) {
					cb_reply($cbid, "❌ " . getTranslate('generalError'), true);
					die;
				}
				$q1 = addChoice(["poll_id" => $p['poll_id'], "creator" => $p['creator']], $userID, $choice);
				if (!$q1['ok']) {
					cb_reply($cbid, "❌ " . getTranslate('generalError'), true);
					die;
				}
				$chosenChoice = getTranslate('callbackParticipateIn', [$p['title']]);
			} else {
				$chosenChoice = getTranslate('callbackTookBackParticipation', [$p['title']]);
				$q1 = removeAllChoices($p, $userID);
				if (!$q1['ok']) {
					cb_reply($cbid, "❌ " . getTranslate('generalError'), true);
					die;
				}
			}
		} elseif ($p['type'] == "doodle") {
			$r = haveChoice($p, $userID, $choice);
			if ($r['ok']) {
				$r = $r['result'];
			} else {
				cb_reply($cbid, "❌ " . getTranslate('generalError'), true);
				die;
			}
			if ($r !== $choice) {
				$q1 = addChoice($p, $userID, $choice);
				if (!$q1['ok']) {
					cb_reply($cbid, "❌ " . getTranslate('generalError'), true);
					die;
				}
				$chosenChoice = getTranslate('callbackVotedFor', [$choice]);
			} else {
				$chosenChoice = getTranslate('callbackTookBack', [$choice]);
				$q1 = removeChoice($p, $userID, $choice);
				if (!$q1['ok']) {
					cb_reply($cbid, "❌ " . getTranslate('generalError'), true);
					die;
				}
			}
		} elseif ($p['type'] == "limited doodle") {
			$r = haveChoice($p, $userID, $choice);
			if ($r['ok']) {
				$r = $r['result'];
			} else {
				cb_reply($cbid, "❌ " . getTranslate('generalError'), true);
				die;
			}
			$max = $p['settings']['max_choices'];
			if ($r !== $choice) {
				$userTotalChoices = userTotalChoices($p, $userID);
				if ($userTotalChoices['ok']) {
					$userTotalChoices = $userTotalChoices['result'];
				} else {
					cb_reply($cbid, "❌ " . getTranslate('generalError'), true);
					die;
				}
				if ($userTotalChoices < $max) {
					$q1 = addChoice($p, $userID, $choice);
					if (!$q1['ok']) {
						cb_reply($cbid, "❌ " . getTranslate('generalError'), true);
						die;
					}
					$chosenChoice = getTranslate('callbackVotedFor', [$choice]);
				} else {
					$chosenChoice = getTranslate('callbackLimitedMaxReached', [$max]);
				}
			} else {
				$chosenChoice = getTranslate('callbackTookBack', [$choice]);
				$q1 = removeChoice($p, $userID, $choice);
				if (!$q1['ok']) {
					cb_reply($cbid, "❌ " . getTranslate('generalError'), true);
					die;
				}
			}
		}
	} elseif ($p['status'] == "closed") {
		cb_reply($cbid, getTranslate('tryVotePollClosed'), true);
		die;
	} else {
		cb_reply($cbid, getTranslate('tryVotePollClosed'), true);
		die;
	}
	if ($u['status'][$botID] !== "avviato") {
		$base = bot_encode("va_$poll_id-$creator");
		$url = "https://t.me/" . $config['username_bot'] . "?start=$base";
		cb_reply($cbid, $chosenChoice, false);//cb_url($cbid, $url); unset($cbid);
		$config['console'] = false;
		ssponsor($chatID, $lang, $u['settings'], $creator);
		die;
	} else {
		if (!isset($u['settings']['1hour'])) {
			$fai = true;
		} elseif ($u['settings']['1hour'] <= time()) {
			$fai = true;
		} else {
			$fai = false;
		}
		if ($fai) {
			$s = ssponsor($chatID, $lang, $u['settings'], $creator);
			$base = bot_encode("va_$poll_id-$creator");
			$url = "https://t.me/" . $config['username_bot'] . "?start=$base";
			if ($s['ok']) {
				cb_reply($cbid, $chosenChoice, false);
			} else {
				cb_url($cbid, $url);
			}
			$config['console'] = false;
			$u['settings']['1hour'] = time() + (60 * 60);
			db_query("UPDATE utenti SET settings = ? WHERE user_id = ?", [json_encode($u['settings']), $userID], "no");
			die;
		}
	}
	if (isset($cbid)) cb_reply($cbid, $chosenChoice, false);
	unset($cbid);
	$cbdata = "update_$poll_id-$creator-true";
}

if (strpos($cbdata, "graph_") === 0) {
	$e = explode("-", str_replace("graph_", '', $cbdata));
	$poll_id = $e[0];
	$creator = $e[1];
	$type = $e[2];
	$url = "https://api.masterpoll.xyz/graph?" . http_build_query(['id' => $poll_id, 'owner_id' => $creator, 'type' => $type, 'theme' => $u['theme']]);
	$time = time();
	$rget = rget("graph-$poll_id-$creator");
	if ($rget['ok']) {
		$rget = $rget['result'];
	} else {
		cb_reply($cbid, "❌ " . getTranslate('generalError'), false);
		die;
	}
	if ($rget) {
		$rdel = rdel("graph-$poll_id-$creator");
		if ($rdel['ok']) {
			$rdel = $rdel['result'];
		} else {
			cb_reply($cbid, "❌ " . getTranslate('generalError'), false);
			die;
		}
		cb_reply($cbid, '⏱', false);
		die;
	} else {
		$rset = rset("graph-$poll_id-$creator", 1);
		if ($rset['ok']) {
			$rset = $rset['result'];
		} else {
			cb_reply($cbid, "❌ " . getTranslate('generalError'), false);
			die;
		}
	}
	$p = sendPoll($poll_id, $creator);
	if ($p['ok']) {
		$p = $p['result'];
	} else {
		cb_reply($cbid, "❌ " . getTranslate('generalError'), false);
		die;
	}
	if (!$p['votes']) {
		cb_reply($cbid, getTranslate('rendererZeroVotedSoFar', ["0"]), true);
		die;
	}
	$lgif = loading_gif();
	cb_reply($cbid, '', false, $cbmid, "<a href='$lgif'>&#8203</>" . italic(getTranslate('loading')), false, 'def', false);
	$last_time = time();
	$ch = curl_init();
	curl_setopt_array($ch, [
		CURLOPT_URL => $url,
		CURLOPT_POST => false,
		CURLOPT_IPRESOLVE => CURL_IPRESOLVE_V4,
		CURLOPT_RETURNTRANSFER => true
	]);
	$output = curl_exec($ch);
	curl_close($ch);
	if (($last_time + 1) >= time()) sleep(1);
	if ($j = json_decode($output, true)) {
		if ($j['ok'] === true) {
			$menu[] = [
				[
					"text" => "🔄 " . getTranslate('commPageRefresh'),
					"callback_data" => "graph_$poll_id-$creator-$type"
				]
			];
			$menu[] = [
				[
					"text" => "🔙 " . getTranslate('backButton'),
					"callback_data" => "coptions_$poll_id-$creator-results"
				]
			];
			$r = "<a href='" . $j['result'] . "'>&#8203;</a>" . bold($p['title']);
			//sm(244432022, $j['result'], false, '', false, false);
			if (isset($p['description'])) $r .= "\n" . $p['description'];
			editMsg($chatID, $r, $cbmid, $menu, 'def', false);
		} else {
			editMsg($chatID, "❌ " . getTranslate('generalError'), $cbmid, $menu);
			call_error("Graph Error: {$j['description']}");
		}
	} else {
		editMsg($chatID, "❌ " . getTranslate('generalError'), $cbmid, $menu);
	}
	sleep(2);
	rdel("graph-$poll_id-$creator");
	die;
}

if (strpos($cbdata, 'addDocument') === 0) {
	$e = explode("-", str_replace("addDocument", "", $cbdata));
	$poll_id = $e[0];
	$creator = $e[1];
	$poll = sendPoll($poll_id, $creator);
	if ($poll['ok']) {
		$poll = $poll['result'];
	} else {
		cb_reply($cbid, "❌ " . getTranslate('generalError'), false);
		die;
	}
	if ($poll['status'] == "deleted") {
		cb_reply($cbid, '', false);
		die;
	}
	if (!isset($poll['settings']['web_content'])) {
		$u['settings']['cache'] = $cbmid;
		$q1 = db_query("UPDATE utenti SET page = ?, settings = ? WHERE user_id = ?", ["addDoc_$poll_id-$creator", json_encode($u['settings']), $userID], "no");
		if (!$q1['ok']) {
			cb_reply($cbid, "❌ " . getTranslate('generalError'), false);
			die;
		}
		$menu[] = [
			[
				"text" => "🔙 " . getTranslate('backButton'),
				"callback_data" => "cancelOption_$poll_id-$creator-poll"
			]
		];
		cb_reply($cbid, '', false, $cbmid, getTranslate('sendMeDocument'), $menu);
	} else {
		$cbdata = "/opt $poll_id-$creator-web_content";
	}
	die;
}

if (strpos($cbdata, "vote_") === 0) {
	$e = explode("-", str_replace("vote_", "", $cbdata));
	$poll_id = $e[0];
	$creator = $e[1];
	$update = true;
	$q = sendPoll($poll_id, $creator);
	if ($q['ok']) {
		$q = $q['result'];
	} else {
		cb_reply($cbid, "❌ " . getTranslate('generalError'), true);
		die;
	}
	if ($q['status'] == "deleted" or !$q['status']) {
		cb_reply($cbid, getTranslate('tryVotePollClosed'), false);
		die;
	}
	$poll = pollToMessage($q, false);
	$dwpp = $poll['disable_web_preview'];
	if ($poll['ok']) {
		$r = $poll['text'];
		$menu = $poll['menu'];
	} else {
		$r = $poll['text'];
	}
	cb_reply($cbid, $chosenChoice, false, $cbmid, $r, $menu, 'def', $dwpp);
	die; 
}

if (strpos($cbdata, "moderate_") === 0) {
	$e = explode("-", str_replace("moderate_", "", $cbdata));
	$poll_id = $e[0];
	$creator = $e[1];
	if (isset($e[2])) {
		$page = $e[2];
	} else {
		$page = 1;
	}
	$q = sendPoll($poll_id, $creator);
	if ($q['ok']) {
		$q = $q['result'];
	} else {
		cb_reply($cbid, "❌ " . getTranslate('generalError'), true);
		die;
	}
	if ($q['status'] == "deleted" or !$q['status']) {
		cb_reply($cbid, getTranslate('tryVotePollClosed'), false);
		die;
	}
	insertMessage($q, $cbmid, 'moderate', $chatID);
	if (in_array($q['type'], ['participation', 'rating'])) {
		cb_reply($cbid, '', false);
		die;
	}
	$poll = pollToMessage($q, 'moderate', ['board_page' => $page, 'lang' => $lang]);
	$dwpp = $poll['disable_web_preview'];
	if ($poll['ok']) {
		$r = $poll['text'];
		$menu = $poll['menu'];
	} else {
		$r = $poll['text'];
	}
	cb_reply($cbid, '', false, $cbmid, $r, $menu, 'def', $dwpp);
	die;
}

if (strpos($cbdata, "turn_anonymous") === 0) {
	$e = explode("-", str_replace("turn_anonymous", "", $cbdata));
	$poll_id = $e[0];
	$creator = $e[1];
	$p = sendPoll($poll_id, $creator);
	if ($p['ok']) {
		$p = $p['result'];
	} else {
		cb_reply($cbid, "❌ " . getTranslate('generalError'), true);
		die;
	}
	if ($p['status'] == "deleted" or !$p['status']) {
		cb_reply($cbid, getTranslate('tryVotePollClosed'), false);
		die;
	} elseif ($p['type'] == "participation") {
		cb_reply($cbid, '', false);
		die;
	}
	$menu[] = [
		[
			"text" => getTranslate('turnAnonymousSure'),
			"callback_data" => "turnanonymous$poll_id-$creator"
		],
		[
			"text" => getTranslate('no'),
			"callback_data" => "coptions_$poll_id-$creator-votes"
		]
	];
	cb_reply($cbid, '', false, $cbmid, getTranslate('turnAnonymous'), $menu);
	die;
}

if (strpos($cbdata, "turnanonymous") === 0) {
	$e = explode("-", str_replace("turnanonymous", "", $cbdata));
	$poll_id = $e[0];
	$creator = $e[1];
	$p = sendPoll($poll_id, $creator);
	if ($p['ok']) {
		$p = $p['result'];
	} else {
		cb_reply($cbid, "❌ " . getTranslate('generalError'), true);
		die;
	}
	if ($p['status'] == "deleted" or $p['type'] == "participation") {
		cb_reply($cbid, '', false);
		die;
	}
	$q1 = db_query("UPDATE polls SET anonymous = ? WHERE poll_id = ? and user_id = ?", [1, $poll_id, $creator], "no");
	if (!$q1['ok']) {
		cb_reply($cbid, "❌ " . getTranslate('generalError'), false);
		die;
	}
	cb_reply($cbid, '✅', false);
	$cbdata = "coptions_$poll_id-$creator-votes";
}

if (strpos($cmd, "delete_") === 0) {
	$base = str_replace("delete_", '', $cmd);
	$base64 = bot_decode($base);
	if (!$base64) {
		sm($chatID, '❌ An error has occurred with this command, please try again later...');
		die;
	}
	$e = explode("-", $base64, 3);
	if (count($e) !== 3) {
		sm($chatID, "⚠️ " . getTranslate('invalidCommand'));
		die;
	}
	$poll_id = $e[0];
	if (!is_numeric($poll_id)) {
		sm($chatID, "⚠️ " . getTranslate('invalidCommand'));
		die;
	}
	$creator = $e[1];
	if (!is_numeric($creator)) {
		sm($chatID, "⚠️ " . getTranslate('invalidCommand'));
		die;
	}
	$choice = $e[2];
	if (!is_numeric($choice)) {
		sm($chatID, "⚠️ " . getTranslate('invalidCommand'));
		die;
	}
	$poll = sendPoll($poll_id, $creator);
	if ($poll['ok']) {
		$poll = $poll['result'];
	} else {
		sm($chatID, "❌ " . getTranslate('generalError'));
		die;
	}
	if (round($creator) !== round($userID) and !$isadmin) {
		$isAdmin = isAdmin($userID, $poll);
		if ($isAdmin['ok']) {
			$isAdmin = $isAdmin['result'];
		} else {
			sm($chatID, "❌ " . getTranslate('generalError'));
			die;
		}
		if (!$isAdmin) {
			sm($chatID, "⚠️ Access denied");
			die;
		}
	}
	if ($poll['status'] == "deleted") {
		sm($chatID, getTranslate('pollDoesntExist'));
		die;
	}
	if (!in_array($poll['type'], ['vote', 'doodle', 'limited doodle', 'board'])) {
		// Tipo di sondaggio non supportato per questo comando
		sm($chatID, "⚠️ " . getTranslate('invalidCommand'));
		die;
	}
	if ($poll['type'] !== "board") $choice = array_keys($poll['choice'])[round($choice)];
	if (count($poll['choice']) === 1 and $poll['type'] !== "board") {
		sm($chatID, getTranslate('deleteLastOption'));
		die;
	}
	if (!in_array($choice, array_keys($poll['choices']))) {
		sm($chatID, "❌ " . getTranslate('generalError'));
		die;
	}
	$remove = removeChoicePoll($poll, $choice);
	if (!$remove['ok']) {
		sm($chatID, "❌ " . getTranslate('generalError'));
		die;
	}
	if ($poll['type'] == "board" and $poll['anonymous']) {
		if (!$isadmin) $choice = "anonymous";
	}
	sm($chatID, getTranslate('deleteOption', [$choice, $poll['title']]));
	rdel("update-$poll_id-$creator");
	$config['console'] = false;
	updatePoll($poll_id, $creator);
	die;
}

if (strpos($cbdata, "erandom_") === 0) {
	$e = explode("-", str_replace("erandom_", '', $cbdata));
	$poll_id = $e[0];
	$creator = $e[1];
	$p = sendPoll($poll_id, $creator);
	if ($p['ok']) {
		$p = $p['result'];
	} else {
		cb_reply($cbid, "❌ " . getTranslate('generalError'), true);
		die;
	}
	if ($p['status'] == "deleted") {
		cb_reply($cbid, '', false);
		die;
	}
	if ($p['choice']['participants'] >= 2) {
		cb_reply($cbid, '', false, $cbmid, getTranslate('loading'));
		$m = ss($chatID, "https://t.me/MinecraftGroupITA/800841");
		//$m = sdice($chatID);
		sleep(3);
		$id = array_values($p['usersvotes']['participants'])[rand(0, count($p['usersvotes']['participants']) - 1)];
		$menu[] = [
			[
				"text" => "🔙 " . getTranslate('backButton'),
				"callback_data" => "cancelOption_$poll_id-$creator-votes"
			]
		];
		editMsg($chatID, str_replace("[0]", tag($id, getName($id)['result']), getTranslate('randomlyExtracted')), $cbmid, $menu, 'html');
		dm($chatID, $m['result']['message_id']);
	} else {
		cb_reply($cbid, getTranslate('rendererZeroVotedSoFar', [0]), false);
	}
	die;
}

if (strpos($cbdata, "blfine_") === 0) {
	$e = explode("-", str_replace("blfine_", '', $cbdata), 3);
	$poll_id = $e[0];
	$creator = $e[1];
	$q1 = db_query("UPDATE utenti SET page = ? WHERE user_id = ?", ["", $userID], "no");
	if (!$q1['ok']) {
		cb_reply($cbid, "❌ " . getTranslate('generalError'), true);
		die;
	}
	$cbdata = "blwords_$poll_id-$creator";
}

if (strpos($cbdata, "bl") === 0) {
	if (strpos($cbdata, "blwords_") === 0) {
		$e = explode("-", str_replace("blwords_", '', $cbdata), 3);
		$poll_id = $e[0];
		$creator = $e[1];
		$p = sendPoll($poll_id, $creator);
		if ($p['ok']) {
			$p = $p['result'];
		} else {
			cb_reply($cbid, '', false);
			die;
		}
		if ($p['status'] == "deleted") {
			cb_reply($cbid, getTranslate('tryVotePollClosed'), false);
			die;
		}
		$menu[] = [
			[
				"text" => "➕ " . getTranslate('addButton'),
				"callback_data" => "bladd_$poll_id-$creator"
			],
			[
				"text" => "➖ " . getTranslate('removeButton'),
				"callback_data" => "blremove_$poll_id-$creator"
			]
		];
		$menu[] = [
			[
				"text" => "🔙 " . getTranslate('backButton'),
				"callback_data" => "cancelOption_$poll_id-$creator-votes"
			]
		];
		if ($p['settings']['bl_words']) {
			$r = "\n";
			foreach ($p['settings']['bl_words'] as $word) {
				$r .= "\n⛔️ " . $word;
			}
		} else {
			$r = "\n" . getTranslate('listEmpty');
		}
		cb_reply($cbid, '', false, $cbmid, "#️⃣ " . bold(getTranslate('optionsBlacklistWords')) . $r, $menu);
		die;
	} elseif (strpos($cbdata, "bladd_") === 0) {
		$e = explode("-", str_replace("bladd_", '', $cbdata));
		$poll_id = $e[0];
		$creator = $e[1];
		$p = sendPoll($poll_id, $creator);
		if ($p['ok']) {
			$p = $p['result'];
		} else {
			cb_reply($cbid, '', false);
			die;
		}
		if ($p['status'] == "deleted") {
			cb_reply($cbid, getTranslate('tryVotePollClosed'), false);
			die;
		}
		$q1 = db_query("UPDATE utenti SET page = ? WHERE user_id = ?", ["bl_add_$poll_id" . "_$creator", $userID], "no");
		if (!$q1['ok']) {
			cb_reply($cbid, "❌ " . getTranslate('generalError'), false);
			die;
		}
		cb_reply($cbid, '', false, $cbmid, getTranslate('insertForbiddenWords', [$p['title']]), $menu);
		die;
	} elseif (strpos($cbdata, "blremove") === 0) {
		$e = explode("-", str_replace("blremove_", '', $cbdata));
		$poll_id = $e[0];
		$creator = $e[1];
		$p = sendPoll($poll_id, $creator);
		if ($p['ok']) {
			$p = $p['result'];
		} else {
			cb_reply($cbid, '', false);
			die;
		}
		if ($p['status'] == "deleted") {
			cb_reply($cbid, getTranslate('tryVotePollClosed'), false);
			die;
		}
		$q1 = db_query("UPDATE utenti SET page = ? WHERE user_id = ?", ["bl_remove_$poll_id" . "_$creator", $userID], "no");
		if (!$q1['ok']) {
			cb_reply($cbid, "❌ " . getTranslate('generalError'), false);
			die;
		}
		cb_reply($cbid, '', false, $cbmid, getTranslate('removeForbiddenWords', [$p['title']]), $menu);
		die;
	} else {
		cb_reply($cbid, 'Error', false);
		die;
	}
}

if (strpos($u['page'], "bl_") === 0 and $messageType == "text message") {
	$config['json_payload'] = false;
	$e = explode("_", $u['page'], 5);
	$action = $e[1];
	$poll_id = $e[2];
	$creator = $e[3];
	$cache = $e[4];
	fastcgi_finish_request();
	$f = noflood("bl-$poll_id-$creator");
	if($f == "1") {
		if (is_numeric($cache)) {
			editMenu($chatID, $cache, []);
		}
	} elseif ($f == "2") {
		$u = db_query("SELECT * FROM utenti WHERE user_id = ?", [$userID], true);
		if ($u['ok']) {
			$u = $u['result'];
		} else {
			sm($chatID, "❌ " . getTranslate('generalError'));
			die;
		}
		$u['settings'] = json_decode($u['settings'], true);
		$u['banlist'] = json_decode($u['banlist'], true);
		$e = explode("_", $u['page'], 5);
		$action = $e[1];
		$poll_id = $e[2];
		$creator = $e[3];
		$cache = $e[4];
		if (is_numeric($cache)) {
			editMenu($chatID, $cache, []);
			rdel("bl-$poll_id-$creator");
		}
	}
	if ($action == "add") {
		$menu[] = [
			[
				"text" => "💾 " . getTranslate('done'),
				"callback_data" => "blfine_$poll_id-$creator"
			]
		];
		$m = sm($chatID, getTranslate('wordAdded'), $menu, 'def', $msgID);
		$msid = $m['result']['message_id'];
		$q1 = db_query("UPDATE utenti SET page = ? WHERE user_id = ?", ["bl_$action" . "_$poll_id" . "_$creator" . "_$msid", $userID], "no");
		if (!$q1['ok']) {
			editMsg($chatID, "❌ " . getTranslate('generalError'), $msid);
			die;
		}
		$p = sendPoll($poll_id, $creator);
		if ($p['ok']) {
			$p = $p['result'];
		} else {
			editMsg($chatID, "❌ " . getTranslate('generalError'), $msid);
			die;
		}
		if ($p['status'] == "deleted") {
			rdel("bl-$poll_id-$creator");
			$q1 = db_query("UPDATE utenti SET page = ? WHERE user_id = ?", ["", $userID], "no");
			if (!$q1['ok']) {
				editMsg($chatID, "❌ " . getTranslate('generalError'), $msid);
				die;
			}
			editMsg($chatID, getTranslate('pollDoesntExist'), $msid);
			die;
		}
		if (!isForbidden($msg, $p['settings']['bl_words'])) {
			$p['settings']['bl_words'][] = strtolower($msg);
			$q1 = db_query("UPDATE polls SET settings = ? WHERE user_id = ? and poll_id = ?", [json_encode($p['settings']), $creator, $poll_id], "no");
			if (!$q1['ok']) {
				sm($chatID, "❌ " . getTranslate('generalError'));
				die;
			}
		}
		rdel("bl-$poll_id-$creator");
		die;
	} elseif ($action == "remove") {
		$menu[] = [
			[
				"text" => "💾 " . getTranslate('done'),
				"callback_data" => "blfine_$poll_id-$creator"
			]
		];
		$m = sm($chatID, getTranslate('wordRemoved'), $menu, 'def', $msgID);
		$msid = $m['result']['message_id'];
		$q1 = db_query("UPDATE utenti SET page = ? WHERE user_id = ?", ["bl_$action" . "_$poll_id" . "_$creator" . "_$msid", $userID], "no");
		if (!$q1['ok']) {
			editMsg($chatID, "❌ " . getTranslate('generalError'), $msid);
			die;
		}
		$p = sendPoll($poll_id, $creator);
		if ($p['ok']) {
			$p = $p['result'];
		} else {
			editMsg($chatID, "❌ " . getTranslate('generalError'), $msid);
			die;
		}
		if ($p['status'] == "deleted") {
			rdel("bl-$poll_id-$creator");
			$q1 = db_query("UPDATE utenti SET page = ? WHERE user_id = ?", ["", $userID], "no");
			if (!$q1['ok']) {
				editMsg($chatID, "❌ " . getTranslate('generalError'), $msid);
				die;
			}
			sm($chatID, getTranslate('pollDoesntExist'));
			die;
		}
		if (isForbidden($msg, $p['settings']['bl_words'])) {
			$p['settings']['bl_words'] = array_diff($p['settings']['bl_words'], [strtolower($msg)]);
			$q1 = db_query("UPDATE polls SET settings = ? WHERE user_id = ? and poll_id = ?", [json_encode($p['settings']), $creator, $poll_id], "no");
			if (!$q1['ok']) {
				sm($chatID, "❌ " . getTranslate('generalError'));
				die;
			}
		}
		rdel("bl-$poll_id-$creator");
		die;
	} else {
		rdel("bl-$poll_id-$creator");
		$q1 = db_query("UPDATE utenti SET page = ? WHERE user_id = ?", ["", $userID], "no");
		if (!$q1['ok']) {
			sm($chatID, "❌ " . getTranslate('generalError'));
			die;
		}
	}
}

if (strpos($cbdata, "/opt ") === 0) {
	$s = [
		null => 1,
		0 => 1,
		1 => 0
	];
	$e = explode("-", str_replace("/opt ", '', $cbdata), 3);
	$poll_id = $e[0];
	$creator = $e[1];
	$set = $e[2];
	$po = sendPoll($poll_id, $creator);
	if ($po['ok']) {
		$po = $po['result'];
	} else {
		cb_reply($cbid, "❌ " . getTranslate('generalError'), false);
		die;
	}
	if ($p['status'] == "deleted") {
		cb_reply($cbid, getTranslate('tryVotePollClosed'), false);
		die;
	}
	$type = "poll";
	if ($set == "message") {
		$type = "votes";
		if ($po['settings']['open_close'] == "message") {
			unset($po['settings']['open_close']);
		} else {
			$po['settings']['open_close'] = "message";
		}
	} elseif ($set == "options") {
		$type = "votes";
		if ($po['settings']['open_close'] == "options") {
			unset($po['settings']['open_close']);
		} else {
			$po['settings']['open_close'] = "options";
		}
	} elseif ($set == "buttons") {
		$type = "votes";
		if ($po['settings']['open_close'] == "buttons") {
			unset($po['settings']['open_close']);
		} else {
			$po['settings']['open_close'] = "buttons";
		}
	} elseif ($set == "in_options-0") {
		$type = "votes";
		$po['settings']['in_options'] = 0;
	} elseif ($set == "in_options-1") {
		$type = "votes";
		$po['settings']['in_options'] = 1;
	} elseif ($set == "in_options-2") {
		$type = "votes";
		$po['settings']['in_options'] = 2;
	} elseif ($set == "numr") {
		$type = "votes";
		unset($po['settings']['num_style']);
	} elseif ($set == "numr1") {
		$type = "votes";
		$po['settings']['num_style'] = 1;
	} elseif ($set == "numr2") {
		$type = "votes";
		$po['settings']['num_style'] = 2;
	} elseif ($set == "numr3") {
		$type = "votes";
		$po['settings']['num_style'] = 3;
	} elseif ($set == "numr4") {
		$type = "votes";
		$po['settings']['num_style'] = 4;
	} elseif ($set == "menu1") {
		$type = "votes";
		unset($po['settings']['menu_strings']);
	} elseif ($set == "menu2") {
		$type = "votes";
		$po['settings']['menu_strings'] = 2;
	} elseif ($set == "menu3") {
		$type = "votes";
		$po['settings']['menu_strings'] = 3;
	} elseif ($set == "menu4") {
		$type = "votes";
		$po['settings']['menu_strings'] = 4;
	} elseif ($set == "menu5") {
		$type = "votes";
		$po['settings']['menu_strings'] = 5;
	} elseif ($set == "antispam") {
		$type = "votes";
		$v = $po['settings']['antispam'];
		$po['settings']['antispam'] = $s[$v];
	} elseif ($set == "hide_voters") {
		$type = "votes";
		$v = $po['settings']['hide_voters'];
		$po['settings']['hide_voters'] = $s[$v];
	} elseif ($set == "sharable") {
		$type = "poll";
		$v = $po['settings']['sharable'];
		$po['settings']['sharable'] = $s[$v];
	} elseif ($set == "web_content") {
		$type = "poll";
		unset($po['settings']['web_content']);
	} elseif ($set == "web_page" and !isset($po['settings']['web_content'])) {
		$type = "poll";
		$v = $po['settings']['web_page'];
		$po['settings']['web_page'] = $s[$v];
	} elseif ($set == "maxv") {
		$type = "votes";
		unset($po['settings']['max_voters']);
	} elseif ($set == "sort") {
		$type = "votes";
		$v = $po['settings']['sort'];
		$po['settings']['sort'] = $s[$v];
	} elseif ($set == "append") {
		$type = "votes";
		$v = $po['settings']['appendable'];
		$po['settings']['appendable'] = $s[$v];
	} elseif ($set == "per1") {
		$type = "votes";
		$po['settings']['percentage'] = "1";
	} elseif ($set == "per2") {
		$type = "votes";
		$po['settings']['percentage'] = "2";
	} elseif ($set == "per3") {
		$type = "votes";
		$po['settings']['percentage'] = "3";
	} elseif ($set == "nogroup") {
		$type = "votes";
		$po['settings']['group'] = "no";
	} elseif ($set == "group1") {
		$type = "votes";
		$po['settings']['group'] = "1";
	} elseif ($set == "group2") {
		$type = "votes";
		$po['settings']['group'] = "2";
	} elseif ($set == "nobars") {
		$type = "votes";
		unset($po['settings']['bars']);
	} elseif ($set == "dot") {
		$type = "votes";
		$po['settings']['bars'] = "dot";
	} elseif ($set == "quad") {
		$type = "votes";
		$po['settings']['bars'] = "quad";
	} elseif ($set == "like") {
		$type = "votes";
		$po['settings']['bars'] = "like";
	} elseif ($set == "notification") {
		$type = "votes";
		$v = $po['settings']['notification'];
		$po['settings']['notification'] = $s[$v];
	}
	$q1 = db_query("UPDATE polls SET settings = ? WHERE poll_id = ? and user_id = ?", [json_encode($po['settings']), $poll_id, $creator], "no");
	if (!$q1['ok']) {
		cb_reply($cbid, "❌ " . getTranslate('generalError'), false);
		die;
	}
	$cbdata = "coptions_$poll_id-$creator-$type";
}

if (strpos($cbdata, "setOTime_") === 0) {
	$e = explode("-", str_replace("setOTime_", '', $cbdata));
	$poll_id = $e[0];
	$creator = $e[1];
	$p = sendPoll($poll_id, $creator);
	if ($p['ok']) {
		$p = $p['result'];
	} else {
		cb_reply($cbid, "❌ " . getTranslate('generalError'), true);
		die;
	}
	if ($p['status'] == "deleted") {
		cb_reply($cbid, getTranslate('tryVotePollClosed'), false);
		die;
	}
	if ($p['status'] == "open") {
		cb_reply($cbid, '', false);
		die;
	}
	if ($e[2] === "false") {
		unset($p['settings']['opentime']);
	} else {
		$p['settings']['opentime'] = $e[2];
	}
	$q1 = db_query("UPDATE polls SET settings = ? WHERE user_id = ? and poll_id = ?", [json_encode($p['settings']), $creator, $poll_id], "no");
	if (!$q1['ok']) {
		cb_reply($cbid, "❌ " . getTranslate('generalError'), false);
		die;
	}
	$cbdata = "coptions_$poll_id-$creator-votes";
}

if (strpos($cbdata, "setCTime_") === 0) {
	$e = explode("-", str_replace("setCTime_", '', $cbdata));
	$poll_id = $e[0];
	$creator = $e[1];
	$p = sendPoll($poll_id, $creator);
	if ($p['ok']) {
		$p = $p['result'];
	} else {
		cb_reply($cbid, "❌ " . getTranslate('generalError'), true);
		die;
	}
	if ($p['status'] == "deleted") {
		cb_reply($cbid, getTranslate('tryVotePollClosed'), false);
		die;
	}
	if ($p['status'] !== "open") {
		cb_reply($cbid, '', false);
		die;
	}
	if ($e[2] === "false") {
		unset($p['settings']['closetime']);
	} else {
		$p['settings']['closetime'] = $e[2];
	}
	$q1 = db_query("UPDATE polls SET settings = ? WHERE user_id = ? and poll_id = ?", [json_encode($p['settings']), $creator, $poll_id], "no");
	if (!$q1['ok']) {
		cb_reply($cbid, "❌ " . getTranslate('generalError'), false);
		die;
	}
	$cbdata = "coptions_$poll_id-$creator-votes";
}

if (strpos($cbdata, "editTitle_") === 0) {
	$e = explode("-", str_replace("editTitle_", '', $cbdata));
	$poll_id = $e[0];
	$creator = $e[1];
	$p = sendPoll($poll_id, $creator);
	if ($p['ok']) {
		$p = $p['result'];
	} else {
		cb_reply($cbid, "❌ " . getTranslate('generalError'), true);
		die;
	}
	if ($p['status'] == "deleted") {
		cb_reply($cbid, getTranslate('tryVotePollClosed'), true);
		db_query("UPDATE utenti SET page = ? WHERE user_id = ?", ["", $userID], "no");
		die;
	}
	if ($p['votes'] < 1) {
		$r = getTranslate('sendTitle');
		$u['settings']['cache'] = $cbmid;
		$q1 = db_query("UPDATE utenti SET page = ?, settings = ? WHERE user_id = ?", ['setTitle' . "$poll_id-$creator", json_encode($u['settings']), $userID], "no");
		if (!$q1['ok']) {
			cb_reply($cbid, "❌ " . getTranslate('generalError'), false);
			die;
		}
		$menu[] = [
			[
				"text" => "🔙 " . getTranslate('backButton'),
				"callback_data" => "cancelOption_$poll_id-$creator-poll"
			]
		];
		cb_reply($cbid, "", false, $cbmid, $r, $menu);
		die;
	} else {
		cb_reply($cbid, getTranslate('cantEditPollTitle'), true);
		$cbdata = "rvotes_$poll_id-$creator-false";
	}
}

if (strpos($cbdata, "setDescription_") === 0) {
	$e = explode("-", str_replace("setDescription_", '', $cbdata));
	$poll_id = $e[0];
	$creator = $e[1];
	$cbdata = "editDescription_$poll_id-$creator";
}

if (strpos($cbdata, "editDescription_") === 0) {
	$e = explode("-", str_replace("editDescription_", '', $cbdata));
	$poll_id = $e[0];
	$creator = $e[1];
	$p = sendPoll($poll_id, $creator);
	if ($p['ok']) {
		$p = $p['result'];
	} else {
		cb_reply($cbid, "❌ " . getTranslate('generalError'), true);
		die;
	}
	if ($p['status'] == "deleted") {
		cb_reply($cbid, getTranslate('tryVotePollClosed'), true);
		db_query("UPDATE utenti SET page = ? WHERE user_id = ?", ["", $userID], "no");
		die;
	}
	if ($p['votes'] < 1) {
		$r = getTranslate('sendDescription');
		$u['settings']['cache'] = $cbmid;
		$q1 = db_query("UPDATE utenti SET page = ?, settings = ? WHERE user_id = ?", ['setDescription' . "$poll_id-$creator", json_encode($u['settings']), $userID], "no");
		if (!$q1['ok']) {
			cb_reply($cbid, "❌ " . getTranslate('generalError'), false);
			die;
		}
		$menu[] = [
			[
				"text" => "🔙 " . getTranslate('backButton'),
				"callback_data" => "cancelOption_$poll_id-$creator-poll"
			]
		];
		cb_reply($cbid, "", false, $cbmid, $r, $menu);
		die;
	} else {
		cb_reply($cbid, getTranslate('cantEditPollDescription'), true);
		$cbdata = "rvotes_$poll_id-$creator-false";
	}
}

if (strpos($cbdata, "removeDescription_") === 0) {
	$e = explode("-", str_replace("removeDescription_", '', $cbdata));
	$poll_id = $e[0];
	$creator = $e[1];
	$p = sendPoll($poll_id, $creator);
	if ($p['ok']) {
		$p = $p['result'];
	} else {
		cb_reply($cbid, "❌ " . getTranslate('generalError'), true);
		die;
	}
	if ($p['status'] == "deleted") {
		cb_reply($cbid, getTranslate('tryVotePollClosed'), true);
		db_query("UPDATE utenti SET page = ? WHERE user_id = ?", ["", $userID], "no");
		die;
	} else {
		$q1 = db_query("UPDATE polls SET description = ? WHERE user_id = ? and poll_id = ?", [null, $creator, $poll_id], "no");
		if (!$q1['ok']) {
			cb_reply($cbid, "❌ " . getTranslate('generalError'), false);
			die;
		}
		cb_reply($cbid, '👍🏻', false);
		$cbdata = "coptions_$poll_id-$creator-poll";
	}
}

if (strpos($u['page'], "setDescription") === 0 and $messageType == "text message") {
	$e = explode("-", str_replace("setDescription", '', $u['page']));
	$poll_id = $e[0];
	$creator = $e[1];
	$p = sendPoll($poll_id, $creator);
	if ($p['ok']) {
		$p = $p['result'];
	} else {
		sm($chatID, "❌ " . getTranslate('generalError'));
		die;
	}
	if ($p['status'] == "deleted") {
		sm($chatID, getTranslate('tryVotePollClosed'));
		db_query("UPDATE utenti SET page = ? WHERE user_id = ?", ['', $userID], "no");
		die;
	}
	if (strlen($msg) <= 512) {
		if ($u['settings']['cache']) {
			editMenu($chatID, $u['settings']['cache'], []);
			unset($u['settings']['cache']);
		}
		$q1 = db_query("UPDATE utenti SET page = ?, settings = ? WHERE user_id = ?", ['', json_encode($u['settings']), $userID], "no");
		if (!$q1['ok']) {
			sm($chatID, "❌ " . getTranslate('generalError'));
			die;
		}
		$q1 = addDescriptionPoll($poll_id, $creator, $msg);
		if (!$q1['ok']) {
			sm($chatID, "❌ " . getTranslate('generalError'));
			die;
		}
		$p['description'] = $msg;
		$poll = pollToMessage($p, true, ['lang' => $lang]);
		sm($chatID, $poll['text'], $poll['menu'], 'def', false, $poll['disable_web_preview']);
		die;
	} else {
		sm($chatID, getTranslate('tooManyChars', [strlen($msg), 512]));
		die;
	}
	die;
}

if (strpos($u['page'], "setTitle") === 0 and $messageType == "text message") {
	$e = explode("-", str_replace("setTitle", '', $u['page']));
	$poll_id = $e[0];
	$creator = $e[1];
	$p = sendPoll($poll_id, $creator);
	if ($p['ok']) {
		$p = $p['result'];
	} else {
		sm($chatID, "❌ " . getTranslate('generalError'));
		die;
	}
	if ($p['status'] == "deleted") {
		sm($chatID, getTranslate('tryVotePollClosed'));
		db_query("UPDATE utenti SET page = ? WHERE user_id = ?", ['', $userID], "no");
		die;
	}
	if (strlen($msg) <= 512) {
		if ($u['settings']['cache']) {
			editMenu($chatID, $u['settings']['cache'], []);
			unset($u['settings']['cache']);
		}
		$q1 = db_query("UPDATE utenti SET page = ?, settings = ? WHERE user_id = ?", ['', json_encode($u['settings']), $userID], "no");
		if (!$q1['ok']) {
			sm($chatID, "❌ " . getTranslate('generalError'));
			die;
		}
		$q1 = addTitlePoll($poll_id, $creator, $msg);
		if (!$q1['ok']) {
			sm($chatID, "❌ " . getTranslate('generalError'));
			die;
		}
		$p['title'] = $msg;
		$poll = pollToMessage($p, true, ['lang' => $lang]);
		sm($chatID, $poll['text'], $poll['menu'], 'def', false, $poll['disable_web_preview']);
		die;
	} else {
		sm($chatID, getTranslate('tooManyChars', [strlen($msg), 512]));
		die;
	}
	die;
}

if (strpos($cbdata, "rvotes_") === 0) {
	$e = explode("-", str_replace("rvotes_", "", $cbdata), 3);
	$poll_id = $e[0];
	$creator = $e[1];
	$ok = json_decode($e[2], true);
	if (!$ok) {
		$menu[] = [
			[
				"text" => getTranslate('sure'),
				"callback_data" => "rvotes_$poll_id-$creator-1"
			],
			[
				"text" => getTranslate('no'),
				"callback_data" => "coptions_$poll_id-$creator-votes"
			]
		];
		cb_reply($cbid, '', false, $cbmid, getTranslate('resetAllVotesWarning'), $menu);
		die;
	} else {
		$p = sendPoll($poll_id, $creator);
		if ($p['ok']) {
			$p = $p['result'];
		} else {
			cb_reply($cbid, "❌ " . getTranslate('generalError'), true);
			die;
		}
		if ($p['status'] == "deleted") {
			cb_reply($cbid, '', false);
			die;
		}
		if ($p['type'] == "board") {
			$choices = [];
		} elseif ($p['type'] == "participation") {
			$choices['participants'] = [];
		} else {
			$cs = array_keys($p['choices']);
			foreach ($cs as $c) {
				$choices[$c] = [];
			}
		}
		$q1 = db_query("UPDATE polls SET choices = ? WHERE user_id = ? and poll_id = ?", [json_encode($choices), $creator, $poll_id], "no");
		if (!$q1['ok']) {
			cb_reply($cbid, "❌ " . getTranslate('generalError'), false);
			die;
		}
		$cbdata = "coptions_$poll_id-$creator-votes";
	}
}

if (strpos($cbdata, "maxvc_") === 0) {
	$e = explode("-", str_replace("maxvc_", '', $cbdata));
	$poll_id = $e[0];
	$creator = $e[1];
	$q1 = db_query("UPDATE utenti SET page = ? WHERE user_id = ?", ['', $userID], "no");
	if (!$q1['ok']) {
		cb_reply($cbid, "❌ " . getTranslate('generalError'), false);
		die;
	}
	$cbdata = "coptions_$poll_id-$creator-votes";
}

if (strpos($u['page'], "maxv_") === 0 and $messageType == "text message") {
	$e = explode("-", str_replace("maxv_", '', $u['page']));
	$poll_id = $e[0];
	$creator = $e[1];
	if (isset($redis)) {
		$id = rget("cache-msg-$userID")['result'];
		editMenu($chatID, $id, []);
		rdel("cache-msg-$userID");
	}
	$p = sendPoll($poll_id, $creator);
	if ($p['ok']) {
		$p = $p['result'];
	} else {
		sm($chatID, "❌ " . getTranslate('generalError'));
		die;
	}
	if ($p['status'] == "deleted") {
		sm($chatID, getTranslate('tryVotePollClosed'));
		db_query("UPDATE utenti SET page = ? WHERE user_id = ?", ["", $userID], "no");
		die;
	}
	if (is_numeric($msg)) {
		if ($msg > 0 and $msg > $p['votes']) {
			$config['response'] = true;
			$m = sm($chatID, "✅...");
			$cbmid = $m['result']['message_id'];
			$p['settings']['max_voters'] = $msg;
			$q1 = db_query("UPDATE utenti SET page = ? WHERE user_id = ?", ['', $userID], "no");
			if (!$q1['ok']) {
				sm($chatID, "❌ " . getTranslate('generalError'));
				die;
			}
			$q1 = db_query("UPDATE polls SET settings = ? WHERE poll_id = ? and user_id = ?", [json_encode($p['settings']), $poll_id, $creator], "no");
			if (!$q1['ok']) {
				sm($chatID, "❌ " . getTranslate('generalError'));
				die;
			}
			$cbdata = "coptions_$poll_id-$creator-votes";
		} else {
			$menu[] = [
				[
					"text" => "🔙 " . getTranslate('backButton'),
					"callback_data" => "maxvc_$poll_id-$creator"
				]
			];
			$m = sm($chatID, getTranslate('maxVotesMessage', [$p['votes']]), $menu);
			rset("cache-msg-$userID", $m['result']['message_id']);
			die;
		}
	} else {
		sm($chatID, getTranslate('giveMeANumberNothingElse'));
		die;
	}
}

if (strpos($cbdata, "/option ") === 0) {
	$e = explode("-", str_replace("/option ", '', $cbdata));
	$poll_id = $e[0];
	$creator = $e[1];
	$q = sendPoll($poll_id, $creator);
	if ($q['ok']) {
		$q = $q['result'];
	} else {
		cb_reply($cbid, "❌ " . getTranslate('generalError'), true);
		die;
	}
	if ($q['status'] == "deleted") {
		cb_reply($cbid, getTranslate('tryVotePollClosed'), false);
		die;
	}
	insertMessage($q, $cbmid, 'options', $chatID);
	$s = [
		0 => "❌",
		1 => "✅"
	];
	$p = [
		0 => "",
		1 => "🔘"
	];
	if ($isadmin) {
		$admin = true;
	} elseif ($userID == $creator) {
		$admin = true;
	} elseif (isAdmin($userID, $q)['result']) {
		$admin = $userID;
	} else {
		$admin = false;
	}
	if (!$admin) {
		$cbdata = "update_$poll_id-$creator-false";
	} else {
		if ($q['anonymous']) {
			$anoval = 0;
			$ano = "✅";
		} else {
			$anoval = 1;
			$ano = "☑";
		}
		if ($q['settings']['web_page']) {
			$allegato = 0;
			$allegat = $s[0];
		} elseif (!isset($q['settings']['web_page'])) {
			$allegato = 0;
			$allegat = $s[0];
		} else {
			$allegato = 1;
			$allegat = $s[1];
		}
		if ($q['settings']['sharable']) {
			$shar = 1;
			$share = $s[1];
		} else {
			$shar = 0;
			$share = $s[0];
		}
		if (isset($q['settings']['hide_voters'])) {
			$hvoters = $s[$q['settings']['hide_voters']];
		} else {
			$hvoters = $s[0];
		}
		if ($q['settings']['web_content']) {
			$sp = "/opt $poll_id-$creator-web_content";
			$allegato = 1;
			$allegat = "🔒";
			$cont = $s[1];
		} else {
			$sp = "addDocument$poll_id-$creator";
			$cont = $s[0];
		}
		if (isset($q['settings']['antispam'])) {
			$aspam = $s[$q['settings']['antispam']];
		} else {
			$aspam = $s[0];
		}
		if (isset($q['settings']['notification'])) {
			$notify = $s[$q['settings']['notification']];
		} else {
			$notify = $s[0];
		}
		if ($isadmin and $config['devmode']) {
			/*$menu[] = [
				[
					"text" => getTranslate('anonymousButton', [$ano]),
					"callback_data" => "//anonymous $poll_id-$creator-$anoval"
				]
			];*/
		}
		$poll = pollToMessage($q, $admin, ['lang' => $lang]);
		if (round($userID) === round($creator) or $isadmin) {
			if (!$q['admins']) {
				$adic = "🕸";
			} elseif (count($q['admins']) == 1) {
				$adic = "👤";
			} else {
				$adic = "👥";
			}
			$menu[] = [
				[
					"text" => getTranslate('optionsAdminsButton', [$adic]),
					"callback_data" => "admins_$poll_id-$creator"
				]
			];
		} elseif (round($userID) === round($admin)) {
			$getPollAdmin = getPollAdmin($q, $admin);
			if ($getPollAdmin['ok']) {
				$getPollAdmin = $getPollAdmin['result'];
			} else {
				cb_reply($cbid, "❌ " . getTranslate('generalError'), true);
				die;
			}
			$perms = $getPollAdmin['perms'];
		} else {
			cb_reply($cbid, '', false);
			die;
		}
		if ($poll['ok']) {
			$r = bold(getTranslate('optionsForPoll')) . "\n\n" . $poll['text'];
			$dwpp = $poll['disable_web_preview'];
			if (round($admin) == round($userID)){
				if (!$perms['votes']) {
					$blockperms['votes'] = "🔒";
				}
				if (!$perms['poll']) {
					$blockperms['poll'] = "🔒";
				}
			}
			$menu[] = [
				[
					"text" => "📁 " . getTranslate('optionsCategoryVotes') . $blockperms['votes'],
					"callback_data" => "coptions_$poll_id-$creator-votes"
				],
				[
					"text" => "📁 " . getTranslate('optionsCategoryPoll') . $blockperms['poll'],
					"callback_data" => "coptions_$poll_id-$creator-poll"
				]
			];
			if (in_array($q['type'], ["vote", "doodle", "limited doodle", "rating", "quiz"])) {
				$menu[] = [
					[
						"text" => "📁 " . getTranslate('optionsCategoryResults'),
						"callback_data" => "coptions_$poll_id-$creator-results"
					]
				];
			}
			$menu[] = [
				[
					"text" => getTranslate('cloneOption'),
					"callback_data" => "/clone_$poll_id-$creator"
				],
				[
					"text" => getTranslate('exportOption'),
					"callback_data" => "export_$poll_id-$creator"
				]
			];
			$menu[] = [
				[
					"text" => "🔙 " . getTranslate('backButton'),
					"callback_data" => "update_$poll_id-$creator"
				]
			];
			cb_reply($cbid, '', false, $cbmid, mb_convert_encoding($r, "UTF-8"), $menu, 'def', $dwpp);
			die;
		} else {
			$r = $poll['text'];
		}
		cb_reply($cbid, '', false, $cbmid, $r);
		updatePoll($poll_id, $creator, $cbmid);
		die;
	}
}

if (strpos($cbdata, "coptions_") === 0) {
	$e = explode("-", str_replace("coptions_", '', $cbdata));
	$poll_id = $e[0];
	$creator = $e[1];
	$type = $e[2];
	$q = sendPoll($poll_id, $creator);
	if ($q['ok']) {
		$q = $q['result'];
	} else {
		cb_reply($cbid, "❌ " . getTranslate('generalError'), true);
		die;
	}
	insertMessage($q, $cbmid, 'options', $chatID);
	if ($q['status'] == "deleted") {
		cb_reply($cbid, getTranslate('tryVotePollClosed'), false);
		die;
	}
	$s = [
		0 => "❌",
		1 => "✅"
	];
	$p = [
		0 => "",
		1 => "🔘"
	];
	if ($isadmin) {
		$isAdmin = true;
	} else {
		$isAdmin = isAdmin($userID, $q);
		if ($isAdmin['ok']) {
			$isAdmin = $isAdmin['result'];
		} else {
			cb_reply($cbid, "❌ " . getTranslate('generalError'), true);
			die;
		}
	}
	if ($isAdmin and is_numeric($cbmid)) {
		if ($isadmin) {
			$admin = true;
		} elseif ($userID == $creator) {
			$admin = true;
		} else {
			$admin = $userID;
		}
		if ($q['anonymous']) {
			$anoval = 0;
			$ano = "✅";
		} else {
			$anoval = 1;
			$ano = "☑";
		}
		if ($q['settings']['web_page']) {
			$allegato = 0;
			$allegat = $s[0];
		} elseif (!isset($q['settings']['web_page'])) {
			$allegato = 0;
			$allegat = $s[0];
		} else {
			$allegato = 1;
			$allegat = $s[1];
		}
		if ($q['settings']['sharable']) {
			$shar = 1;
			$share = $s[1];
		} else {
			$shar = 0;
			$share = $s[0];
		}
		if (isset($q['settings']['hide_voters'])) {
			$hvoters = $s[$q['settings']['hide_voters']];
		} else {
			$hvoters = $s[0];
		}
		if ($q['settings']['web_content']) {
			$sp = "/opt $poll_id-$creator-web_content";
			$allegato = 1;
			$allegat = "🔒";
			$cont = $s[1];
		} else {
			$sp = "addDocument$poll_id-$creator";
			$cont = $s[0];
		}
		if (isset($q['settings']['antispam'])) {
			$aspam = $s[$q['settings']['antispam']];
		} else {
			$aspam = $s[0];
		}
		if (isset($q['settings']['notification'])) {
			$notify = $s[$q['settings']['notification']];
		} else {
			$notify = $s[0];
		}
		if ($isadmin and $config['devmode']) {
			/*$menu[] = [
				[
					"text" => getTranslate('anonymousButton', [$ano]),
					"callback_data" => "//anonymous $poll_id-$creator-$anoval"
				]
			];*/
		}
	} else {
		$admin = false;
	}
	if (!$admin) {
		$cbdata = "update_$poll_id-$creator-false";
	} else {
		if (round($userID) === round($creator) or $isadmin) {
			if (!$q['admins']) {
				$adic = "🕸";
			} elseif (count($q['admins']) == 1) {
				$adic = "👤";
			} else {
				$adic = "👥";
			}
			$menu[] = [
				[
					"text" => getTranslate('optionsAdminsButton', [$adic]),
					"callback_data" => "admins_$poll_id-$creator"
				]
			];
		} elseif (round($userID) === round($admin)) {
			$getPollAdmin = getPollAdmin($q, $admin);
			if ($getPollAdmin['ok']) {
				$getPollAdmin = $getPollAdmin['result'];
			} else {
				cb_reply($cbid, "❌ " . getTranslate('generalError'), true);
				die;
			}
			$perms = $getPollAdmin['perms'];
		}
		$poll = pollToMessage($q, $admin, ['lang' => $lang]);
		if ($poll['ok']) {
			if ($q['settings']['web_content']) {
				$sp = "/opt $poll_id-$creator-web_content";
				$allegato = 1;
				$allegat = "🔒";
				$cont = $s[1];
			} else {
				$sp = "addDocument$poll_id-$creator";
				$cont = $s[0];
			}
			$r = bold(getTranslate('optionsForPoll')) . "\n\n" . $poll['text'];
			$dwpp = $poll['disable_web_preview'];
			if (round($admin) == round($userID)){
				if ($type and !$perms[$type]) {
					cb_reply($cbid, '', false);
					die;
				}
				if (!$perms['votes']) {
					$blockperms['votes'] = "🔒";
				}
				if (!$perms['poll']) {
					$blockperms['poll'] = "🔒";
				}
			}
			$emo = ["votes" => "📁 ", "poll" => "📁 ", "results" => "📁 "];
			$emo[$type] = "📂 ";
			if ($emo['votes'] == "📂 ") {
				$evotes = "";
				$epoll = "poll";
				$eres = "results";
			} elseif ($emo['poll'] == "📂 ") {
				$evotes = "votes";
				$epoll = "";
				$eres = "results";
			} elseif ($emo['results'] == "📂 ") {
				$evotes = "votes";
				$epoll = "poll";
				$eres = "";
			} else {
				$evotes = "votes";
				$epoll = "poll";
				$eres = "results";
			}
			$menu[] = [
				[
					"text" => $emo['votes'] . getTranslate('optionsCategoryVotes') . $blockperms['votes'],
					"callback_data" => "coptions_$poll_id-$creator-$evotes"
				],
				[
					"text" => $emo['poll'] . getTranslate('optionsCategoryPoll') . $blockperms['poll'],
					"callback_data" => "coptions_$poll_id-$creator-$epoll"
				]
			];
			if (in_array($q['type'], ["vote", "doodle", "limited doodle", "rating", "quiz"])) {
				$menu[] = [
					[
						"text" => $emo['results'] . getTranslate('optionsCategoryResults'),
						"callback_data" => "coptions_$poll_id-$creator-$eres"
					]
				];
			}
			if ($type == "votes") {
				$c = [
					"message" => "📁",
					"buttons" => "📁",
					"options" => "📁"
				];
				if (!isset($q['settings']['open_close'])) {
					
				} else {
					$c[$q['settings']['open_close']] = "📂";
				}
				if (isset($q['settings']['closetime'])) {
					$ctime = "⏱";
				} else {
					$ctime = $s[0];
				}
				if (isset($q['settings']['opentime'])) {
					$otime = "⏱";
				} else {
					$otime = $s[0];
				}
				if (in_array($q['type'], ["vote", "doodle", "limited doodle", "rating", "board"])) {
					if (in_array($q['type'], ["vote", "doodle", "limited doodle", "rating"])) {
						$menud[] = [
							"text" => $c['buttons'] . " " . getTranslate('buttonsButton'),
							"callback_data" => "/opt $poll_id-$creator-buttons"
						];
					}
					if (in_array($q['type'], ["vote", "doodle", "limited doodle", "board"])) {
						$menud[] = [
							"text" => $c['message'] . " " . getTranslate('messageButton'),
							"callback_data" => "/opt $poll_id-$creator-message"
						];
					}
					if (in_array($q['type'], ["vote", "doodle", "limited doodle", "board"])) {
						$menud[] = [
							"text" => $c['options'] . " " . getTranslate('optionsButton'),
							"callback_data" => "/opt $poll_id-$creator-options"
						];
					}
					if ($menud) $menu[] = $menud;
				}
				if (in_array($q['type'], ['vote', 'doodle', 'limited doodle', 'rating'])) {
					if ($q['settings']['open_close'] == "buttons") {
						if (!isset($q['settings']['percentage'])) {
							$q['settings']['percentage'] = 3;
						}
						$per = [
							1 => $p[0],
							2 => $p[0],
							3 => $p[0]
						];
						$per[$q['settings']['percentage']] = $p[1];
						$menu[] = [
							[
								"text" => "0 " . $per[1],
								"callback_data" => "/opt $poll_id-$creator-per1"
							],
							[
								"text" => "0% - 0 " . $per[2],
								"callback_data" => "/opt $poll_id-$creator-per2"
							],
							[
								"text" => "0 - 0% " . $per[3],
								"callback_data" => "/opt $poll_id-$creator-per3"
							]
						];
						if ($q['type'] !== 'rating') {
							if (!isset($q['settings']['menu_strings'])) {
								$q['settings']['menu_strings'] = 1;
							}
							$per = [
								1 => $p[0],
								2 => $p[0],
								3 => $p[0],
								4 => $p[0],
								5 => $p[0]
							];
							$per[$q['settings']['menu_strings']] = $p[1];
							$menu[] = [
								[
									"text" => "Strings",
									"callback_data" => "int"
								],
								[
									"text" => "1  " . $per[1],
									"callback_data" => "/opt $poll_id-$creator-menu1"
								],
								[
									"text" => "2 " . $per[2],
									"callback_data" => "/opt $poll_id-$creator-menu2"
								],
								[
									"text" => "3 " . $per[3],
									"callback_data" => "/opt $poll_id-$creator-menu3"
								],
								[
									"text" => "4 " . $per[4],
									"callback_data" => "/opt $poll_id-$creator-menu4"
								],
								[
									"text" => "5 " . $per[5],
									"callback_data" => "/opt $poll_id-$creator-menu5"
								]
							];
						}
					}
				}
				if ($q['type'] == "board") {
					if ($q['settings']['open_close'] == "message") {
						$menu[] = [
							[
								"text" => getTranslate('optionsHideVotersButton', [$hvoters]),
								"callback_data" => "/opt $poll_id-$creator-hide_voters"
							]
						];
					}
					if ($q['settings']['open_close'] == "options") {
						$menu[] = [
							[
								"text" => getTranslate('notificationOptionButton', [$notify]),
								"callback_data" => "/opt $poll_id-$creator-notification"
							],
							[
								"text" => getTranslate('antiSpamOptionButton', [$aspam]),
								"callback_data" => "/opt $poll_id-$creator-antispam"
							]
						];
						$menu[] = [
							[
								"text" => "✏ " . getTranslate('moderate'),
								"callback_data" => "moderate_$poll_id-$creator"
							],
							[
								"text" => "#️⃣ " . getTranslate('optionsBlacklistWords'),
								"callback_data" => "blwords_$poll_id-$creator"
							]
						];
					}
				} elseif ($q['type'] == "participation") {
					$menu[] = [
						[
							"text" => "📋 " . getTranslate('participationList'),
							"callback_data" => "poll_list_$poll_id-$creator"
						],
						[
							"text" => "🎲 " . getTranslate('randomlyExtract'),
							"callback_data" => "erandom_$poll_id-$creator"
						]
					];
				} elseif ($q['type'] == "rating") {
					if ($q['settings']['open_close'] == "buttons") {
						if (isset($q['settings']['num_style'])) {
							$select[$q['settings']['num_style']] = 1;
							$nr = $p[0];
							$nr1 = $p[$select[1]];
							$nr2 = $p[$select[2]];
							$nr3 = $p[$select[3]];
							$nr4 = $p[$select[4]];
						} else {
							$nr = $p[1];
							$nr1 = $p[0];
							$nr2 = $p[0];
							$nr3 = $p[0];
							$nr4 = $p[0];
						}
						$menu[] = [
							[
								"text" => "1 " . $nr,
								"callback_data" => "/opt $poll_id-$creator-numr"
							],
							[
								"text" => "1️⃣ " . $nr1,
								"callback_data" => "/opt $poll_id-$creator-numr1"
							],
							[
								"text" => "¹ " . $nr2,
								"callback_data" => "/opt $poll_id-$creator-numr2"
							],
							[
								"text" => "① " . $nr3,
								"callback_data" => "/opt $poll_id-$creator-numr3"
							],
							[
								"text" => "⑴ " . $nr4,
								"callback_data" => "/opt $poll_id-$creator-numr4"
							]
						];
					}
				} else {
					if ($q['settings']['open_close'] == "message") {
						if ($q['settings']['bars'] == "like") {
							$like = $p[1];
							$dot = $p[0];
							$quad = $p[0];
							$no = $p[0];
						} elseif ($q['settings']['bars'] == "quad") {
							$like = $p[0];
							$dot = $p[0];
							$quad = $p[1];
							$no = $p[0];
						} elseif ($q['settings']['bars'] == "dot") {
							$like = $p[0];
							$dot = $p[1];
							$quad = $p[0];
							$no = $p[0];
						} else {
							$like = $p[0];
							$dot = $p[0];
							$quad = $p[0];
							$no = $p[1];
						}
						$menu[] = [
							[
								"text" => getTranslate('optionsHideVotersButton', [$hvoters]),
								"callback_data" => "/opt $poll_id-$creator-hide_voters"
							]
						];
						if (isset($q['settings']['in_options'])) {
							$select[$q['settings']['in_options']] = 1;
							$ivnr = $p[$select[0]];
							$ivnr1 = $p[$select[1]];
							$ivnr2 = $p[$select[2]];
						} else {
							$ivnr = $p[0];
							$ivnr1 = $p[0];
							$ivnr2 = $p[1];
						}
						$menu[] = [
							[
								"text" => getTranslate('optionsPercentageNone', [$ivnr]),
								"callback_data" => "/opt $poll_id-$creator-in_options-0"
							],
							[
								"text" => "50% " . $ivnr1,
								"callback_data" => "/opt $poll_id-$creator-in_options-1"
							],
							[
								"text" => "50 " . $ivnr2,
								"callback_data" => "/opt $poll_id-$creator-in_options-2"
							]
						];
						$menu[] = [
							[
								"text" => getTranslate('optionsPercentageNone', [$no]),
								"callback_data" => "/opt $poll_id-$creator-nobars"
							],
							[
								"text" => "●○ $dot",
								"callback_data" => "/opt $poll_id-$creator-dot"
							],
							[
								"text" => "👍 $like",
								"callback_data" => "/opt $poll_id-$creator-like"
							]
						];
						if ($q['anonymous'] == false) {
							if ($q['settings']['group'] == "1") {
								$g1 = $p[1];
								$g2 = $p[0];
								$nog = $p[0];
							} elseif ($q['settings']['group'] == "2") {
								$g1 = $p[0];
								$g2 = $p[1];
								$nog = $p[0];
							} elseif ($q['settings']['group'] == "no") {
								$g1 = $p[0];
								$g2 = $p[0];
								$nog = $p[1];
							} else {
								$g1 = $p[1];
								$g2 = $p[0];
								$nog = $p[0];
							}
							$menu[] = [
								[
									"text" => getTranslate('optionNone', [$nog]),
									"callback_data" => "/opt $poll_id-$creator-nogroup"
								],
								[
									"text" => "┌ $g1",
									"callback_data" => "/opt $poll_id-$creator-group1"
								],
								[
									"text" => "╔ $g2",
									"callback_data" => "/opt $poll_id-$creator-group2"
								]
							];
						}
					}
					if (isset($q['settings']['sort'])) {
						$sort = $s[$q['settings']['sort']];
					} else {
						$sort = $s[0];
					}
					if (isset($q['settings']['appendable'])) {
						$append = $s[$q['settings']['appendable']];
					} else {
						$append = $s[0];
					}
					if ($q['settings']['open_close'] == "options") {
						$menu[] = [
							[
								"text" => getTranslate('optionsSorted', [$sort]),
								"callback_data" => "/opt $poll_id-$creator-sort"
							],
							[
								"text" => getTranslate('optionsAppendable', [$append]),
								"callback_data" => "/opt $poll_id-$creator-append"
							]
						];
						if ($q['settings']['appendable']) {
							$menu[] = [
								[
									"text" => getTranslate('notificationOptionButton', [$notify]),
									"callback_data" => "/opt $poll_id-$creator-notification"
								],
								[
									"text" => getTranslate('antiSpamOptionButton', [$aspam]),
									"callback_data" => "/opt $poll_id-$creator-antispam"
								]
							];
						}
						$menuadd[] = [
							"text" => "✏ " . getTranslate('moderate'),
							"callback_data" => "moderate_$poll_id-$creator"
						];
						if ($q['settings']['appendable']) {
							$menuadd[] = [
								"text" => "#️⃣ " . getTranslate('optionsBlacklistWords'),
								"callback_data" => "blwords_$poll_id-$creator"
							];
						}
						$menu[] = $menuadd;
					}
				}
				if ($q['status'] == "open") {
					$menuz = [
						"text" => getTranslate('optionsClosingTimeButton', [$ctime]),
						"callback_data" => "closingTime_$poll_id-$creator"
					];
				} elseif ($q['status'] == "closed") {
					$menuz = [
						"text" => getTranslate('optionsOpeningTimeButton', [$ctime]),
						"callback_data" => "openingTime_$poll_id-$creator"
					];
				} else {
					$menuz = [
						"text" => getTranslate('optionsClosingTimeButton', [$ctime]),
						"callback_data" => "closingTime_$poll_id-$creator"
					];
					call_error("Errore su c-options: \$q['status'] sconosciuto.\n" . $q['status']);
				}
				if (!$q['anonymous'] and $q['type'] !== "participation") {
					$menuz[] = [
						"text" => getTranslate('optionsTurnAnonymous') . " 👁‍🗨",
						"callback_data" => "turn_anonymous$poll_id-$creator"
					];
				}
				if ($q['settings']['max_voters']) {
					$maxv = "✅";
					$maxvopt = "/opt $poll_id-$creator-maxv";
				} else {
					$maxv = "☑️";
					$maxvopt = "maxv_$poll_id-$creator";
				}
				$menu[] = [
					[
						"text" => getTranslate('resetAllVotesButton'),
						"callback_data" => "rvotes_$poll_id-$creator-0"
					],
					[
						"text" => getTranslate('maxVotesButton', [$maxv]),
						"callback_data" => $maxvopt
					],
					$menuz
				];
			} elseif ($type == "poll") {
				$menu[] = [
					[
						"text" => getTranslate('editTitleButton') . " ✏️",
						"callback_data" => "editTitle_$poll_id-$creator"
					]
				];
				if (isset($q['description'])) {
					$menu[] = [
						[
							"text" => getTranslate('editDescriptionButton') . " ✏️",
							"callback_data" => "editDescription_$poll_id-$creator"
						],
						[
							"text" => getTranslate('removeButton') . " ❌",
							"callback_data" => "removeDescription_$poll_id-$creator"
						]
					];
				} else {
					$menu[] = [
						[
							"text" => getTranslate('setDescriptionButton') . " ➕",
							"callback_data" => "setDescription_$poll_id-$creator"
						]
					];
				}
				$menu[] = [
					[
						"text" => getTranslate('webContentPreview', [$cont]),
						"callback_data" => $sp
					],
					[
						"text" => getTranslate('webPagePreview', [$allegat]),
						"callback_data" => "/opt $poll_id-$creator-web_page"
					]
				];
				$menu[] = [
					[
						"text" => getTranslate('optionsShareable', [$share]),
						"callback_data" => "/opt $poll_id-$creator-sharable"
					]
				];
			} elseif ($type == "results") {
				if (in_array($q['type'], ['vote', 'doodle', 'limited doodle', 'rating', 'quiz'])) {
					$menu[] = [
						[
							"text" => getTranslate('optionGraph'),
							"callback_data" => "graph_$poll_id-$creator-bar"
						],
						[
							"text" => getTranslate('optionGraphPie'),
							"callback_data" => "graph_$poll_id-$creator-pie"
						]
					];
					if (!$q['anonymous']) {
						$menu[] = [
							[
								"text" => getTranslate('votersList') . " 👥",
								"callback_data" => "uslist_$poll_id-$creator-view"
							]
						];
					}
				}
			}
			$menu[] = [
				[
					"text" => getTranslate('cloneOption'),
					"callback_data" => "/clone_$poll_id-$creator"
				],
				[
					"text" => getTranslate('exportOption'),
					"callback_data" => "export_$poll_id-$creator"
				]
			];
			$menu[] = [
				[
					"text" => "🔙 " . getTranslate('backButton'),
					"callback_data" => "update_$poll_id-$creator"
				]
			];
			cb_reply($cbid, '', false, $cbmid, $r, $menu, 'def', $dwpp);
			die;
		} else {
			$r = $poll['text'];
		}
		cb_reply($cbid, '', false, $cbmid, $r);
		$config['console'] = false;
		updatePoll($poll_id, $creator, $cbmid);
		die;
	}
}

if (strpos($cbdata, "maxv_") === 0) {
	$e = explode("-", str_replace("maxv_", '', $cbdata));
	$poll_id = $e[0];
	$creator = $e[1];
	$p = sendPoll($poll_id, $creator);
	if ($p['ok']) {
		$p = $p['result'];
	} else {
		cb_reply($cbid, "❌ " . getTranslate('generalError'), true);
		die;
	}
	if ($p['status'] == "deleted") {
		cb_reply($cbid, getTranslate('tryVotePollClosed'), false);
		die;
	}
	$menu[] = [
		[
			"text" => "🔙 " . getTranslate('backButton'),
			"callback_data" => "maxvc_$poll_id-$creator"
		]
	];
	editMsg($chatID, getTranslate('maxVotesMessage', [$p['votes']]), $cbmid, $menu);
	if (isset($redis)) {
		rset("cache-msg-$userID", $cbmid);
	}
	cb_reply($cbid, '', false);
	db_query("UPDATE utenti SET page = ? WHERE user_id = ?", [$cbdata, $userID], "no");
	die;
}

if (strpos($u['page'], "addAdmin_") === 0 and $messageType == "text message" or strpos($u['page'], "addAdmin_") === 0 and $fuserID) {
	$e = explode("-", str_replace("addAdmin_", '', $u['page']));
	$poll_id = $e[0];
	$creator = $e[1];
	if (round($userID) !== round($creator)) {
		cb_reply($cbid, "", false);
		die;
	}
	if (!is_numeric($msg)) {
		username(str_replace("@", '', $msg));
	}
	if(isset($fuserID)) {
		$th = db_query("SELECT * FROM utenti WHERE user_id = ?", [$fuserID], true);
	} elseif ($msg) {
		$th = db_query("SELECT * FROM utenti WHERE user_id = ? or username = ?", [round($msg), str_replace("@", '', $msg)], true);
	} else {
	}
	if ($th['ok']) {
		$th = $th['result'];
	} else {
		sm($chatID, "❌ " . getTranslate('generalError'));
		die;
	}
	if ($th['status'] == "deleted" and $th['user_id']) {
		sm($chatID, "Deleted account...");
		die;
	}
	$q1 = db_query("UPDATE utenti SET page = ? WHERE user_id = ?", ["", $userID], "no");
	if (!$q1['ok']) {
		sm($chatID, "❌ " . getTranslate('generalError'));
		die;
	}
	if (is_numeric($th['user_id'])) {
		$s = text_link("✅", 'tg://user?id=' . $th['user_id']);
		$q1 = addAdmin(['poll_id' => $poll_id, 'creator' => $creator], $th['user_id']);
		if (!$q1['ok']) {
			call_error(json_encode($q1));
			sm($chatID, "❌ " . getTranslate('generalError'));
			die;
		}
	} else {
		$s = "❌";
	}
	$config['json_payload'] = false;
	$m = sm($chatID, "🕙 $s");
	$cbmid = $m['result']['message_id'];
	$cbdata = "admins_$poll_id-$creator";
}

if (strpos($cbdata, "radmin_") === 0) {
	$e = explode("-", str_replace("radmin_", '', $cbdata));
	$poll_id = $e[0];
	$creator = $e[1];
	$admin = $e[2];
	if (round($userID) !== round($creator)) {
		cb_reply($cbid, "", false);
		die;
	}
	$q1 = removeAdmin(['poll_id' => $poll_id, 'creator' => $creator], $admin);
	if (!$q1['ok']) {
		cb_reply($cbid, "❌ " . getTranslate('generalError'), true);
		die;
	}
	$cbdata = "admins_$poll_id-$creator";
}

if (strpos($cbdata, "admin_") === 0) {
	$e = explode("-", str_replace("admin_", '', $cbdata));
	$poll_id = $e[0];
	$creator = $e[1];
	$admin = $e[2];
	if (round($userID) !== round($creator)) {
		cb_reply($cbid, '', false);
		die;
	}
	$p = getPollAdmin(['poll_id' => $poll_id, 'creator' => $creator], $admin);
	if ($p['ok']) {
		$p = $p['result'];
	} else {
		cb_reply($cbid, "❌ " . getTranslate('generalError'), true);
		die;
	}
	$s = [
		null => "🔒",
		0 => "🔒",
		1 => "🔓"
	];
	$v = [
		null => 1,
		0 => 1,
		1 => 0
	];
	if ($p['status'] == "waiting" and !$e[3]) {
		$emo = "";
		$ev = $s[$p['perms']['votes']];
		$ep = $s[$p['perms']['poll']];
		$eco = $s[$p['perms']['co']];
		$menu[] = [
			[
				"text" => getTranslate('inviteAdminButton'),
				"switch_inline_query" => "administrators " . bot_encode("$poll_id-$creator")
			]
		];
		$menu[] = [
			[
				"text" => getTranslate('editButton'),
				"callback_data" => "$cbdata-edit",
			],
			[
				"text" => getTranslate('removeButton'),
				"callback_data" => "radmin_$poll_id-$creator-$admin",
			]
		];
	} elseif ($p['status'] == "administrator" and !$e[3]) {
		$emo = "👨🏻‍💻";
		$ev = $s[$p['perms']['votes']];
		$ep = $s[$p['perms']['poll']];
		$eco = $s[$p['perms']['co']];
		$menu[] = [
			[
				"text" => getTranslate('editButton'),
				"callback_data" => "$cbdata-edit",
			],
			[
				"text" => getTranslate('removeButton'),
				"callback_data" => "radmin_$poll_id-$creator-$admin",
			]
		];
	} elseif ($e[3]) {
		if ($p['status'] == "waiting") {
			$menu[] = [
				[
					"text" => getTranslate('inviteAdminButton'),
					"switch_inline_query" => "administrators " . bot_encode("$poll_id-$creator")
				]
			];
		}
		$emo = "👨🏻‍💻";
		if ($e[3] == "votes") {
			$p['perms']['votes'] = $v[$p['perms']['votes']];
		} elseif ($e[3] == "poll") {
			$p['perms']['poll'] = $v[$p['perms']['poll']];
		} elseif ($e[3] == "co") {
			$p['perms']['co'] = $v[$p['perms']['co']];
		} else {
			
		}
		$ev = $s[$p['perms']['votes']];
		$ep = $s[$p['perms']['poll']];
		$eco = $s[$p['perms']['co']];
		$q1 = editAdmin(['poll_id' => $poll_id, 'creator' => $creator], $admin, $p);
		if (!$q1['ok']) {
			cb_reply($cbid, "❌ " . getTranslate('generalError'), true);
			die;
		}
		$menu[] = [
			[
				"text" => "$ev " . getTranslate('optionsCategoryVotes'),
				"callback_data" => "admin_$poll_id-$creator-$admin-votes",
			],
			[
				"text" => "$ep " . getTranslate('optionsCategoryPoll'),
				"callback_data" => "admin_$poll_id-$creator-$admin-poll",
			]
		];
		$menu[] = [
			[
				"text" => "$eco " . getTranslate('optionsCategoryOpenClose'),
				"callback_data" => "admin_$poll_id-$creator-$admin-co",
			]
		];
		$menu[] = [
			[
				"text" => getTranslate('removeButton'),
				"callback_data" => "radmin_$poll_id-$creator-$admin",
			]
		];
	} 
	$menu[] = [
		[
			"text" => "🔙 " . getTranslate('backButton'),
			"callback_data" => "admins_$poll_id-$creator"
		]
	];
	cb_reply($cbid, '', false, $cbmid, getTranslate('administratorPermissions', [htmlspecialchars_decode(getName($admin)['result']), $ep, $ev, $eco]), $menu);
	die;
}

if (strpos($cbdata, "admins_") === 0) {
	$e = explode("-", str_replace("admins_", '', $cbdata));
	$poll_id = $e[0];
	$creator = $e[1];
	if (round($userID) !== round($creator)) {
		cb_reply($cbid, "", false);
		die;
	}
	$p = sendPoll($poll_id, $creator);
	if ($p['ok']) {
		$p = $p['result'];
	} else {
		cb_reply($cbid, "❌ " . getTranslate('generalError'), true);
		die;
	}
	if ($p['status'] == "deleted") {
		cb_reply($cbid, '', false);
		die;
	}
	$sem = [
		"waiting" => " 🕙",
		"administrator" => " 👨🏻‍💻"
	];
	if (!is_array($p['settings']['admins'])) $p['settings']['admins'] = [];
	if (count($p['settings']['admins']) == 1) {
		$admins = getTranslate('participationSingleVotedSoFar', ["1"]);
		$admin = array_keys($p['settings']['admins'])[0];
		$name = htmlspecialchars_decode(getName($admin)['result']);
		$status = $sem[$p['settings']['admins'][$admin]['status']];
		$list = "\n" . tag($admin, $name) . $status;
		$menu[0] = [
			[
				"text" => $name,
				"callback_data" => "admin_$poll_id-$creator-$admin"
			]
		];
	} elseif ($p['settings']['admins']) {
		$admins = getTranslate('participationMultiVotedSoFar', [count($p['settings']['admins'])]);
		foreach($p['settings']['admins'] as $admin => $perms) {
			$name = htmlspecialchars_decode(getName($admin)['result']);
			$status = $sem[$perms['status']];
			$list .= "\n" . tag($admin, $name) . $status;
			$menu[] = [
				[
					"text" => $name,
					"callback_data" => "admin_$poll_id-$creator-$admin"
				]
			];
		}
	} else {
		$admins = getTranslate('participationZeroVotedSoFar', ["0"]);
	}
	if (count($p['settings']['admins']) < 10) {
		$menu[] = [
			[
				"text" => getTranslate('addAdminButton'),
				"callback_data" => "addAdmin_$poll_id-$creator"
			]
		];
	}
	$menu[] = [
		[
			"text" => "🔙 " . getTranslate('backButton'),
			"callback_data" => "/option $poll_id-$creator"
		]
	];
	$list .= "\n";
	cb_reply($cbid, '', false, $cbmid, "<b>" . getTranslate('administratorsList', [htmlspecialchars_decode($p['title'])]) . "</b>\n$list\n" . $admins, $menu);
	die;
}

if (strpos($cbdata, "addAdmin_") === 0) {
	$q1 = db_query("UPDATE utenti SET page = ? WHERE user_id = ?", [$cbdata, $userID], "no");
	if (!$q1['ok']) {
		cb_reply($cbid, "❌ " . getTranslate('generalError'), true);
		die;
	}
	cb_reply($cbid, '', false, $cbmid, getTranslate('addAdminMessage'), $menu);
	die;
}

if (strpos($cbdata, "addMeAdmin_") === 0) {
	$e = explode("-", str_replace("addMeAdmin_", '', $cbdata));
	$poll_id = $e[0];
	$creator = $e[1];
	$getPollAdmin = getPollAdmin(['poll_id' => $poll_id, 'creator' => $creator], $userID);
	if ($getPollAdmin['ok']) {
		$getPollAdmin = $getPollAdmin['result'];
	} else {
		cb_reply($cbid, "❌ " . getTranslate('generalError'), true);
		die;
	}
	if (round($userID) == round($creator)) {
		cb_reply($cbid, getTranslate('administratorsAlreadyAddMe'), false);
	} elseif ($getPollAdmin['status'] == "administrator") {
		cb_reply($cbid, getTranslate('administratorsAlreadyAddMe'), false);
	} elseif ($getPollAdmin['status'] == "waiting") {
		$base64 = bot_encode("admin_$poll_id-$creator");
		cb_url($cbid, "https://t.me/" . $config['username_bot'] . "?start=" . $base64);
	} else {
		cb_reply($cbid, '', false);
	}
	die;
}

if (strpos($cbdata, "export_") === 0) {
	$e = explode("-", str_replace("export_", '', $cbdata));
	$poll_id = $e[0];
	$creator = $e[1];
	$p = sendPoll($poll_id, $creator);
	if ($p['ok']) {
		$p = $p['result'];
	} else {
		cb_reply($cbid, "❌ " . getTranslate('generalError'), true);
		die;
	}
	if ($p['status'] == "deleted") {
		cb_reply($cbid, getTranslate('tryVotePollClosed'), false);
		die;
	}
	if ($e[2]) {
		if ($config['redis'] and $isadmin) {
			$get = rget("upload-$userID");
			if ($get['ok']) {
				$get = $get['result'];
			} else {
				cb_reply($cbid, "❌ " . getTranslate('generalError'), true);
				die;
			}
			if ($get) {
				if ($get + 5 > time()) {
					cb_reply($cbid, "🥶", true);
					die;
				} else {
					rdel("upload-$userID");
				}
			} else {
				rset("upload-$userID", time());
			}
		}
		if ($e[2] == "csv") {
			$config['response'] = true;
			$config['json_payload'] = false;
		}
		cb_reply($cbid, $e[2] . '...', false);
		$cont = pollToArray($p);
		$config['response'] = true;
		scAction($chatID, 'upload_document');
		$timelastsa = time();
		$config['azioni'] = true;
		$config['method'] = "POST";
		if ($e[2] == "json") {
			$data = json_encode($cont, JSON_PRETTY_PRINT);
			if ($timelastsa + 1 < time()) {
				scAction($chatID, 'upload_document');
				$timelastsa = time();
			}
			file_put_contents("$creator-$poll_id.json", $data);
			if ($timelastsa + 1 < time()) {
				scAction($chatID, 'upload_document');
				$timelastsa = time();
			}
			$s = sd($chatID, "$creator-$poll_id.json");
			if ($s['ok'] !== true) {
				sm($chatID, "Error to uploading...");
			}
			if (file_exists("$creator-$poll_id.json")) unlink("$creator-$poll_id.json");
		} elseif ($e[2] == "yaml") {
			if (!function_exists("yaml_emit")) {
				call_error("La funzione 'yaml_emit' è inesistente e non può esportare il file $creator-$poll_id.yaml.");
				sm($chatID, "This Bot not support yaml extension now. Please contact our support Bot.");
				die;
			}
			$data = yaml_emit($cont);
			file_put_contents("$creator-$poll_id.yaml", $data);
			$s = sd($chatID, "$creator-$poll_id.yaml");
			if ($s['ok'] !== true) {
				sm($chatID, "Error to uploading...");
			}
			if (file_exists("$creator-$poll_id.yaml")) unlink("$creator-$poll_id.yaml");
		} elseif ($e[2] == "xml") {
			require("/home/masterpoll-documents/lib/XMLSerializer.php");
			if (!class_exists("XMLSerializer")) {
				call_error("La classe 'XMLSerializer' è inesistente e non può esportare il file $creator-$poll_id.xml.");
				sm($chatID, "This Bot not support xml extension now. Please contact our support Bot.");
				die;
			}
			$xml_generater = new XMLSerializer;
			$std_class = json_decode(json_encode($cont));
			$data = $xml_generater->generateValidXmlFromObj($std_class);
			file_put_contents("$creator-$poll_id.xml", $data);
			$s = sd($chatID, "$creator-$poll_id.xml");
			if ($s['ok'] !== true) {
				sm($chatID, "Error to uploading...");
			}
			if (file_exists("$creator-$poll_id.xml")) unlink("$creator-$poll_id.xml");
		} elseif ($e[2] == "csv") {
			require("/home/masterpoll-documents/lib/ExcelSerializer.php");
			$file_name = "$creator-$poll_id.csv";
			if (!function_exists("doXLS")) {
				call_error("La funzione 'doXLS' è inesistente e non può esportare il file $file_name.");
				sm($chatID, "This Bot not support csv extension now. Please contact our support Bot.");
				die;
			}
			$data = doXLS($cont, $file_name);
			file_put_contents($file_name, $data);
			$s = sd($chatID, $file_name);
			if ($s['ok'] !== true) {
				sm($chatID, "Error to uploading...");
			}
			if (file_exists($file_name)) unlink($file_name);
		}
	} else {
		$menu[] = [
			[
				"text" => "📦 JSON",
				"callback_data" => "export_$poll_id-$creator-json"
			],
			[
				"text" => "📦 YAML",
				"callback_data" => "export_$poll_id-$creator-yaml"
			],
			[
				"text" => "📦 XML",
				"callback_data" => "export_$poll_id-$creator-xml"
			]
		];
		$menu[] = [
			[
				"text" => "📦 Excel",
				"callback_data" => "export_$poll_id-$creator-csv"
			]
		];
		$menu[] = [
			[
				"text" => "🔙 " . getTranslate('backButton'),
				"callback_data" => "/option $poll_id-$creator"
			]
		];
		cb_reply($cbid, '', false, $cbmid, getTranslate('exportFileDescription'), $menu);
	}
}

if (strpos($cbdata, "openingTime_") === 0) {
	$e = explode("-", str_replace("openingTime_", '', $cbdata));
	$poll_id = $e[0];
	$creator = $e[1];
	$p = sendPoll($poll_id, $creator);
	if ($p['ok']) {
		$p = $p['result'];
	} else {
		cb_reply($cbid, "❌ " . getTranslate('generalError'), true);
		die;
	}
	if ($p['status'] == "deleted") {
		cb_reply($cbid, '', false);
		die;
	}
	if ($p['status'] == "open") {
		cb_reply($cbid, '', false);
		die;
	}
	if (isset($e[2])) {
		$date = $e[2];
		if ($date <= time()) {
			$date = time() + 60;
		}
		$datetime = date("\n\n⏱ " . $u['settings']['date_format'], $date);
		$menu[] = [
			[
				"text" => date("j", $date),
				"callback_data" => "oTime_$poll_id-$creator-$date-day"
			],
			[
				"text" => date("M", $date),
				"callback_data" => "oTime_$poll_id-$creator-$date-month"
			],
			[
				"text" => date("Y", $date),
				"callback_data" => "oTime_$poll_id-$creator-$date-year"
			]
		];
		$menu[] = [
			[
				"text" => date("H", $date),
				"callback_data" => "oTime_$poll_id-$creator-$date-hour"
			],
			[
				"text" => date("i", $date),
				"callback_data" => "oTime_$poll_id-$creator-$date-minute"
			]
		];
		$menu[] = [
			[
				"text" => "✅ " . getTranslate('applyButton'),
				"callback_data" => "setOTime_$poll_id-$creator-$date"
			]
		];
	} elseif (isset($p['settings']['opentime'])) {
		$date = $p['settings']['opentime'];
		$datetime = date("\n\n🕰 " . $u['settings']['date_format'], $date);
		$menu[] = [
			[
				"text" => getTranslate('editButton'),
				"callback_data" => "openingTime_$poll_id-$creator-$date"
			],
			[
				"text" => getTranslate('removeButton'),
				"callback_data" => "/setOTime_$poll_id-$creator-false"
			],
		];
	} else {
		$date = time();
		$menu[] = [
			[
				"text" => date("j", $date),
				"callback_data" => "oTime_$poll_id-$creator-$date-day"
			],
			[
				"text" => date("M", $date),
				"callback_data" => "oTime_$poll_id-$creator-$date-month"
			],
			[
				"text" => date("Y", $date),
				"callback_data" => "oTime_$poll_id-$creator-$date-year"
			]
		];
		$menu[] = [
			[
				"text" => date("H", $date),
				"callback_data" => "oTime_$poll_id-$creator-$date-hour"
			],
			[
				"text" => date("i", $date),
				"callback_data" => "oTime_$poll_id-$creator-$date-minute"
			]
		];
	}
	$menu[] = [
		[
			"text" => "🔙 " . getTranslate('backButton'),
			"callback_data" => "coptions_$poll_id-$creator-votes"
		]
	];
	if ($datetime) $datetime .= date("\n🕰 " . $u['settings']['date_format']);
	cb_reply($cbid, '', false, $cbmid, getTranslate('openingTimeSet') . $datetime, $menu);
	die;
}

if (strpos($cbdata, "oTime_") === 0) {
	$e = explode("-", str_replace("oTime_", '', $cbdata));
	$poll_id = $e[0];
	$creator = $e[1];
	$p = sendPoll($poll_id, $creator);
	if ($p['ok']) {
		$p = $p['result'];
	} else {
		cb_reply($cbid, "❌ " . getTranslate('generalError'), true);
		die;
	}
	if ($p['status'] == "deleted") {
		cb_reply($cbid, '', false);
		die;
	}
	if ($p['status'] !== "closed") {
		cb_reply($cbid, '', false);
		die;
	}
	$date = $e[2];
	$type = $e[3];
	if ($type == "year") {
		$r = range(date("Y"), 2030);
		foreach($r as $year) {
			if (isset($num)) {
				if (count($menu[$num]) >= 4) $num = $num + 1;
			} else {
				$num = 0;
			}
			$dat = mktime(date("H", $date), date("i", $date), date("s", $date), date("n", $date), date("j", $date), $year);
			$menu[$num][] = [
				"text" => $year,
				"callback_data" => "openingTime_$poll_id-$creator-" . $dat
			];
		}
	} elseif ($type == "month") {
		$r = range(1, 12);
		foreach($r as $month) {
			if (isset($num)) {
				if (count($menu[$num]) >= 4) $num = $num + 1;
			} else {
				$num = 0;
			}
			$dat = mktime(date("H", $date), date("i", $date), date("s", $date), $month, date("j", $date), date("Y", $date));
			$menu[$num][] = [
				"text" => date("M", mktime(0, 0, 0, $month)),
				"callback_data" => "openingTime_$poll_id-$creator-" . $dat
			];
		}
	} elseif ($type == "day") {
		$r = range(1, 31);
		foreach($r as $day) {
			if (isset($num)) {
				if (count($menu[$num]) >= 5) $num = $num + 1;
			} else {
				$num = 0;
			}
			$dat = mktime(date("H", $date), date("i", $date), date("s", $date), date("n", $date), $day, date("Y", $date));
			$menu[$num][] = [
				"text" => $day,
				"callback_data" => "openingTime_$poll_id-$creator-" . $dat
			];
		}
	} elseif ($type == "hour") {
		$r = range(0, 23);
		foreach($r as $hour) {
			if (isset($num)) {
				if (count($menu[$num]) >= 5) $num = $num + 1;
			} else {
				$num = 0;
			}
			$dat = mktime($hour, date("i", $date), date("s", $date), date("n", $date), date("j", $date), date("Y", $date));
			$menu[$num][] = [
				"text" => $hour,
				"callback_data" => "openingTime_$poll_id-$creator-" . $dat
			];
		}
	} elseif ($type == "minute") {
		$menu[] = [
			[
				"text" => "1 - 29",
				"callback_data" => "oTime_$poll_id-$creator-$date-minute1"
			],
			[
				"text" => "30 - 59",
				"callback_data" => "oTime_$poll_id-$creator-$date-minute2"
			]
		];
	} elseif ($type == "minute1") {
		$r = range(0, 29);
		foreach($r as $minute) {
			if (isset($num)) {
				if (count($menu[$num]) >= 6) $num = $num + 1;
			} else {
				$num = 0;
			}
			$dat = mktime(date("H", $date), $minute, date("s", $date), date("n", $date), date("j", $date), date("Y", $date));
			$menu[$num][] = [
				"text" => $minute,
				"callback_data" => "openingTime_$poll_id-$creator-" . $dat
			];
		}
	} elseif ($type == "minute2") {
		$r = range(30, 59);
		foreach($r as $minute) {
			if (isset($num)) {
				if (count($menu[$num]) >= 6) $num = $num + 1;
			} else {
				$num = 0;
			}
			$dat = mktime(date("H", $date), $minute, date("s", $date), date("n", $date), date("j", $date), date("Y", $date));
			$menu[$num][] = [
				"text" => $minute,
				"callback_data" => "openingTime_$poll_id-$creator-" . $dat
			];
		}
	} else {
	}
	$menu[] = [
		[
			"text" => "🔙 " . getTranslate('backButton'),
			"callback_data" => "openingTime_$poll_id-$creator-$date"
		]
	];
	cb_reply($cbid, '', false, $cbmid, getTranslate('openingTimeSet'), $menu);
	die;
}

if (strpos($cbdata, "closingTime_") === 0) {
	$e = explode("-", str_replace("closingTime_", '', $cbdata));
	$poll_id = $e[0];
	$creator = $e[1];
	$p = sendPoll($poll_id, $creator);
	if ($p['ok']) {
		$p = $p['result'];
	} else {
		cb_reply($cbid, "❌ " . getTranslate('generalError'), true);
		die;
	}
	if ($p['status'] == "deleted") {
		cb_reply($cbid, '', false);
		die;
	}
	if ($p['status'] == "closed") {
		cb_reply($cbid, '', false);
		die;
	}
	if (isset($e[2])) {
		$date = $e[2];
		if ($date <= time()) {
			$date = time() + 60;
		}
		$datetime = date("\n\n⏱ " . $u['settings']['date_format'], $date);
		$menu[] = [
			[
				"text" => date("j", $date),
				"callback_data" => "cTime_$poll_id-$creator-$date-day"
			],
			[
				"text" => date("M", $date),
				"callback_data" => "cTime_$poll_id-$creator-$date-month"
			],
			[
				"text" => date("Y", $date),
				"callback_data" => "cTime_$poll_id-$creator-$date-year"
			]
		];
		$menu[] = [
			[
				"text" => date("H", $date),
				"callback_data" => "cTime_$poll_id-$creator-$date-hour"
			],
			[
				"text" => date("i", $date),
				"callback_data" => "cTime_$poll_id-$creator-$date-minute"
			]
		];
		$menu[] = [
			[
				"text" => "✅ " . getTranslate('applyButton'),
				"callback_data" => "setCTime_$poll_id-$creator-$date"
			]
		];
	} elseif (isset($p['settings']['closetime'])) {
		$date = $p['settings']['closetime'];
		$datetime = date("\n\n⏱ " . $u['settings']['date_format'], $date);
		$menu[] = [
			[
				"text" => getTranslate('editButton'),
				"callback_data" => "closingTime_$poll_id-$creator-$date"
			],
			[
				"text" => getTranslate('removeButton'),
				"callback_data" => "setCTime_$poll_id-$creator-false"
			],
		];
	} else {
		$date = time();
		$menu[] = [
			[
				"text" => date("j", $date),
				"callback_data" => "cTime_$poll_id-$creator-$date-day"
			],
			[
				"text" => date("M", $date),
				"callback_data" => "cTime_$poll_id-$creator-$date-month"
			],
			[
				"text" => date("Y", $date),
				"callback_data" => "cTime_$poll_id-$creator-$date-year"
			]
		];
		$menu[] = [
			[
				"text" => date("H", $date),
				"callback_data" => "cTime_$poll_id-$creator-$date-hour"
			],
			[
				"text" => date("i", $date),
				"callback_data" => "cTime_$poll_id-$creator-$date-minute"
			]
		];
	}
	$menu[] = [
		[
			"text" => "🔙 " . getTranslate('backButton'),
			"callback_data" => "coptions_$poll_id-$creator-votes"
		]
	];
	if ($datetime) $datetime .= date("\n🕰 " . $u['settings']['date_format']);
	cb_reply($cbid, '', false, $cbmid, getTranslate('closingTimeSet') . $datetime, $menu);
	die;
}

if (strpos($cbdata, "cTime_") === 0) {
	$e = explode("-", str_replace("cTime_", '', $cbdata));
	$poll_id = $e[0];
	$creator = $e[1];
	$p = sendPoll($poll_id, $creator);
	if ($p['ok']) {
		$p = $p['result'];
	} else {
		cb_reply($cbid, "❌ " . getTranslate('generalError'), true);
		die;
	}
	if ($p['status'] == "deleted") {
		cb_reply($cbid, '', false);
		die;
	}
	if ($p['status'] !== "open") {
		cb_reply($cbid, '', false);
		die;
	}
	$date = $e[2];
	$type = $e[3];
	if ($type == "year") {
		$r = range(date("Y"), 2030);
		foreach($r as $year) {
			if (isset($num)) {
				if (count($menu[$num]) >= 4) $num = $num + 1;
			} else {
				$num = 0;
			}
			$dat = mktime(date("H", $date), date("i", $date), date("s", $date), date("n", $date), date("j", $date), $year);
			$menu[$num][] = [
				"text" => $year,
				"callback_data" => "closingTime_$poll_id-$creator-" . $dat
			];
		}
	} elseif ($type == "month") {
		$r = range(1, 12);
		foreach($r as $month) {
			if (isset($num)) {
				if (count($menu[$num]) >= 4) $num = $num + 1;
			} else {
				$num = 0;
			}
			$dat = mktime(date("H", $date), date("i", $date), date("s", $date), $month, date("j", $date), date("Y", $date));
			$menu[$num][] = [
				"text" => date("M", mktime(0, 0, 0, $month)),
				"callback_data" => "closingTime_$poll_id-$creator-" . $dat
			];
		}
	} elseif ($type == "day") {
		$r = range(1, 31);
		foreach($r as $day) {
			if (isset($num)) {
				if (count($menu[$num]) >= 5) $num = $num + 1;
			} else {
				$num = 0;
			}
			$dat = mktime(date("H", $date), date("i", $date), date("s", $date), date("n", $date), $day, date("Y", $date));
			$menu[$num][] = [
				"text" => $day,
				"callback_data" => "closingTime_$poll_id-$creator-" . $dat
			];
		}
	} elseif ($type == "hour") {
		$r = range(0, 23);
		foreach($r as $hour) {
			if (isset($num)) {
				if (count($menu[$num]) >= 5) $num = $num + 1;
			} else {
				$num = 0;
			}
			$dat = mktime($hour, date("i", $date), date("s", $date), date("n", $date), date("j", $date), date("Y", $date));
			$menu[$num][] = [
				"text" => $hour,
				"callback_data" => "closingTime_$poll_id-$creator-" . $dat
			];
		}
	} elseif ($type == "minute") {
		$menu[] = [
			[
				"text" => "0 - 29",
				"callback_data" => "cTime_$poll_id-$creator-$date-minute1"
			],
			[
				"text" => "30 - 59",
				"callback_data" => "cTime_$poll_id-$creator-$date-minute2"
			]
		];
	} elseif ($type == "minute1") {
		$r = range(0, 29);
		foreach($r as $minute) {
			if (isset($num)) {
				if (count($menu[$num]) >= 6) $num = $num + 1;
			} else {
				$num = 0;
			}
			$dat = mktime(date("H", $date), $minute, date("s", $date), date("n", $date), date("j", $date), date("Y", $date));
			$menu[$num][] = [
				"text" => $minute,
				"callback_data" => "closingTime_$poll_id-$creator-" . $dat
			];
		}
	} elseif ($type == "minute2") {
		$r = range(30, 59);
		foreach($r as $minute) {
			if (isset($num)) {
				if (count($menu[$num]) >= 6) $num = $num + 1;
			} else {
				$num = 0;
			}
			$dat = mktime(date("H", $date), $minute, date("s", $date), date("n", $date), date("j", $date), date("Y", $date));
			$menu[$num][] = [
				"text" => $minute,
				"callback_data" => "closingTime_$poll_id-$creator-" . $dat
			];
		}
	} else {
	}
	$menu[] = [
		[
			"text" => "🔙 " . getTranslate('backButton'),
			"callback_data" => "closingTime_$poll_id-$creator-$date"
		]
	];
	cb_reply($cbid, '', false, $cbmid, getTranslate('closingTimeSet'), $menu);
	die;
}

if (strpos($cbdata, "psts_") === 0) {
	$q1 = db_query("UPDATE utenti SET page = ? WHERE user_id = ?", ["", $userID], "no");
	if (!$q1['ok']) {
		cb_reply($cbid, "❌ " . getTranslate('generalError'), true);
		die;
	}
	if (!isPremium($userID, $u)) {
		cb_reply($cbid, getTranslate('premiumFunction'), true);
		die;
	}
	$e = explode("-", str_replace("psts_", '', $cbdata));
	$poll_id = $e[0];
	$creator = $e[1];
	$chaID = "-100" . $e[2];
	if ($chaID == "-100") {
		cb_reply($cbid, "", false);
		call_error("cbdata non valido in psts_*: $cbdata da $userID");
		die;
	}
	$typedb = $e[3];
	if (!isset($e[3])) {
		cb_reply($cbid, "", false);
		call_error("cbdata non valido in psts_*: $cbdata da $userID");
		die;
	}
	$next = "psts_" . $e[0] . "-" . $e[1] . "-" . $e[2] . "-" . $e[3] . "-";
	$date = $e[4];
	$action = $e[5];
	$t = getTranslate('sendPollSelectDate');
	if ($action == "save") {
		if ($date <= time() + (60 * 5)) {
			$date = time() + (60 * 10);
		}
		$json = [
			"poll_id" => $poll_id,
			"creator" => $creator,
			"chat_id" => $chaID
		];
		$q1 = db_query("INSERT INTO schedule (poll_data, date, bot) VALUES (?,?,?)", [json_encode($json), $date, $config['username_bot']], "no");
		if (!$q1['ok']) {
			cb_reply($cbid, "❌ " . getTranslate('generalError'), true);
			die;
		}
		cb_reply($cbid, '✅', false);
		$cbdata = "update_$poll_id-$creator-false";
	} else {
		if (!$action) {
			if (!$date) {
				$date = time() + (60 * 10);
			} elseif ($date <= time() + 60) {
				$date = time() + (60 * 2);
			}
			$menu[] = [
				[
					"text" => date("j", $date),
					"callback_data" => $next . "$date-day"
				],
				[
					"text" => date("M", $date),
					"callback_data" => $next . "$date-month"
				],
				[
					"text" => date("Y", $date),
					"callback_data" => $next . "$date-year"
				]
			];
			$menu[] = [
				[
					"text" => date("H", $date),
					"callback_data" => $next . "$date-hour"
				],
				[
					"text" => date("i", $date),
					"callback_data" => $next . "$date-minute"
				]
			];
			$menu[] = [
				[
					"text" => "💾 " . getTranslate('done'),
					"callback_data" => $next . "$date-save"
				],
				[
					"text" => "🔙 " . getTranslate('backButton'),
					"callback_data" => "psend_$poll_id-$creator"
				]
			];
		} elseif ($action == "year") {
			$r = range(date("Y"), 2027);
			foreach($r as $year) {
				if (isset($num)) {
					if (count($menu[$num]) >= 4) $num = $num + 1;
				} else {
					$num = 0;
				}
				$dat = mktime(date("H", $date), date("i", $date), date("s", $date), date("n", $date), date("j", $date), $year);
				$menu[$num][] = [
					"text" => $year,
					"callback_data" => $next . $dat
				];
			}
		} elseif ($action == "month") {
			$r = range(1, 12);
			foreach($r as $month) {
				if (isset($num)) {
					if (count($menu[$num]) >= 4) $num = $num + 1;
				} else {
					$num = 0;
				}
				$dat = mktime(date("H", $date), date("i", $date), date("s", $date), $month, date("j", $date), date("Y", $date));
				$menu[$num][] = [
					"text" => date("M", mktime(0, 0, 0, $month)),
					"callback_data" => $next . $dat
				];
			}
		} elseif ($action == "day") {
			$r = range(1, 31);
			foreach($r as $day) {
				if (isset($num)) {
					if (count($menu[$num]) >= 5) $num = $num + 1;
				} else {
					$num = 0;
				}
				$dat = mktime(date("H", $date), date("i", $date), date("s", $date), date("n", $date), $day, date("Y", $date));
				$menu[$num][] = [
					"text" => $day,
					"callback_data" => $next . $dat
				];
			}
		} elseif ($action == "hour") {
			$r = range(0, 23);
			foreach($r as $hour) {
				if (isset($num)) {
					if (count($menu[$num]) >= 5) $num = $num + 1;
				} else {
					$num = 0;
				}
				$dat = mktime($hour, date("i", $date), date("s", $date), date("n", $date), date("j", $date), date("Y", $date));
				$menu[$num][] = [
					"text" => $hour,
					"callback_data" => $next . $dat
				];
			}
		} elseif ($action == "minute") {
			$r = [0, 5, 10, 15, 20, 25, 30, 35, 40, 45, 50, 55, 60];
			foreach($r as $minute) {
				if (isset($num)) {
					if (count($menu[$num]) >= 6) $num = $num + 1;
				} else {
					$num = 0;
				}
				$dat = mktime(date("H", $date), $minute, date("s", $date), date("n", $date), date("j", $date), date("Y", $date));
				$menu[$num][] = [
					"text" => $minute,
					"callback_data" => $next . $dat
				];
			}
		} else {
			$t = getTranslate('voteDoesntExist');
		}
		cb_reply($cbid, '', false, $cbmid, $t, $menu);
		die;
	}
}

if (strpos($cbdata, "pstnow_") === 0) {
	$q1 = db_query("UPDATE utenti SET page = ? WHERE user_id = ?", ["", $userID], "no");
	if (!$q1['ok']) {
		cb_reply($cbid, "❌ " . getTranslate('generalError'), true);
		die;
	}
	$e = explode("-", str_replace("pstnow_", '', $cbdata));
	$poll_id = $e[0];
	$creator = $e[1];
	$chaID = "-100" . $e[2];
	if (!isPremium($userID, $u)) {
		cb_reply($cbid, getTranslate('premiumFunction'), true);
		die;
	}
	if ($chaID == "-100") {
		cb_reply($cbid, "", false);
		call_error("cbdata non valido in pstnow_*: $cbdata da $userID");
		die;
	}
	$typedb = $e[3];
	if (!isset($e[3])) {
		cb_reply($cbid, "", false);
		call_error("cbdata non valido in pstnow_*: $cbdata da $userID");
		die;
	}
	$poll = sendPoll($poll_id, $creator);
	if ($poll['ok']) {
		$poll = $poll['result'];
	} else {
		cb_reply($cbid, '', false);
		die;
	}
	$p = pollToMessage($poll, false, ['lang' => $lang]);
	$config['response'] = true;
	$m = sm($chaID, $p['text'], $p['menu'], 'def', false, $p['disable_web_preview']);
	if ($m['ok'] === false) {
		$menu[] = [
			[
				"text" => "◀️",
				"callback_data" => "pstime_$poll_id-$creator-" . $e[2] . "-$typedb"
			]
		];
		cb_reply($cbid, "Telegram Error!", false, $cbmid, getTranslate('telegramError', [$m['error_code'], $m['description']]), $menu);
		die;
	} else {
		cb_reply($cbid, '✅', false);
		if ($m['result']['message_id']) insertMessage($poll, $m['result']['message_id'], false, $chaID);
	}
	$cbdata = "update_$poll_id-$creator-false";
}

if (strpos($cbdata, "pclose_") === 0) {
	$e = explode("-", str_replace("pclose_", '', $cbdata));
	$poll_id = $e[0];
	$creator = $e[1];
	$q = sendPoll($poll_id, $creator);
	if ($q['ok']) {
		$q = $q['result'];
	} else {
		cb_reply($cbid, "❌ " . getTranslate('generalError'), false);
		die;
	}
	if ($q['status'] == "deleted" or !$q['status']) {
		cb_reply($cbid, getTranslate('tryVotePollClosed'), false);
		db_query("UPDATE utenti SET page = ? WHERE user_id = ?", ["", $userID], "no");
		die;
	}
	if (round($userID) !== round($creator)) {
		$p = getPollAdmin($q, $userID);
		if ($p['ok']) {
			$p = $p['result'];
		} else {
			cb_reply($cbid, "❌ " . getTranslate('generalError'), false);
			die;
		}
		if ($p) {
			if ($p['status'] !== "administrator") {
				cb_reply($cbid, '', false);
				die;
			} elseif ($p['perms']['co']) {
				
			} else {
				cb_reply($cbid, '', false);
				die;
			}
		} else {
			cb_reply($cbid, '', false);
			die;
		}
	}
	$q1 = db_query("UPDATE polls SET status = ? WHERE poll_id = ? and user_id = ?", ['closed', $poll_id, $creator], "no");
	if ($q1['ok']) {
		$q1 = $q1['result'];
	} else {
		cb_reply($cbid, "❌ " . getTranslate('generalError'), false);
		die;
	}
	if (isset($e[2])) {
		$cbdata = "list_" . $e[2];
	} else {
		$cbdata = "update_$poll_id-$creator-true";
	}
}

if (strpos($cbdata, "popen_") === 0) {
	$e = explode("-", str_replace("popen_", '', $cbdata));
	$poll_id = $e[0];
	$creator = $e[1];
	$q = sendPoll($poll_id, $creator);
	if ($q['ok']) {
		$q = $q['result'];
	} else {
		cb_reply($cbid, "❌ " . getTranslate('generalError'), false);
		die;
	}
	if ($q['status'] == "deleted" or !$q['status']) {
		cb_reply($cbid, getTranslate('tryVotePollClosed'), false);
		db_query("UPDATE utenti SET page = ? WHERE user_id = ?", ["", $userID], "no");
		die;
	}
	if (round($userID) !== round($creator)) {
		$p = getPollAdmin($q, $userID);
		if ($p['ok']) {
			$p = $p['result'];
		} else {
			cb_reply($cbid, "❌ " . getTranslate('generalError'), false);
			die;
		}
		if ($p) {
			if ($p['status'] !== "administrator") {
				cb_reply($cbid, '', false);
				die;
			} elseif ($p['perms']['co']) {
				
			} else {
				cb_reply($cbid, '', false);
				die;
			}
		} else {
			cb_reply($cbid, '', false);
			die;
		}
	}
	$q1 = db_query("UPDATE polls SET status = ? WHERE poll_id = ? and user_id = ?", ['open', $poll_id, $creator], "no");
	if ($q1['ok']) {
		$q1 = $q1['result'];
	} else {
		cb_reply($cbid, "❌ " . getTranslate('generalError'), false);
		die;
	}
	if (isset($e[2])) {
		$cbdata = "list_" . $e[2];
	} else {
		$cbdata = "update_$poll_id-$creator-true";
	}
}

if (strpos($u['page'], "addDoc_") === 0 and !isset($cbdata)) {
	$e = explode("-", str_replace("addDoc_", '', $u['page']));
	$poll_id = $e[0];
	$creator = $e[1];
	if ($messageType == "text message") {
		$link = $msg;
	} else {
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, "https://telegra.ph/upload");
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		$ex = $config['console'];
		$config['console'] = false;
		if ($foto) {
			$file = getFile($foto);
			if (strpos($file, "https://api.telegram.org/file") === 0) {} else {
				sm($chatID, "$file...");
				die;
			}
			copy($file, "cache-$userID.jpeg");
			$args = ['file' => curl_file_create("cache-$userID.jpeg", "image/jpeg")];
		} elseif ($video) {
			$file = getFile($video);
			if (strpos($file, "https://api.telegram.org/file") === 0) {} else {
				sm($chatID, "$file...");
				die;
			}
			copy($file, "cache-$userID.mp4");
			$args = ['file' => curl_file_create("cache-$userID.mp4", "video/mp4")];
		} elseif ($gif) {
			$file = getFile($gif);
			if (strpos($file, "https://api.telegram.org/file") === 0) {} else {
				sm($chatID, "$file...");
				die;
			}
			copy($file, "cache-$userID.mp4");
			$args = ['file' => curl_file_create("cache-$userID.mp4", "video/mp4")];
		} else {
			sm($chatID, "Message not supported for media uploads...");
			die;
		}
		curl_setopt($ch, CURLOPT_POSTFIELDS, $args);
		$res = curl_exec($ch);
		$res = json_decode($res, true);
		$config['console'] = $ex;
		if (isset($res[0]['src'])) {
			$link = "https://telegra.ph" . $res[0]['src'];
		}
		if (file_exists("cache-$userID.jpeg")) unlink("cache-$userID.jpeg");
		if (file_exists("cache-$userID.mp4")) unlink("cache-$userID.mp4");
	}
	if ($link) {
		if ($u['settings']['cache']) editMenu($chatID, $u['settings']['cache'], []);
		$poll = sendPoll($poll_id, $creator);
		if ($poll['ok']) {
			$poll = $poll['result'];
		} else {
			sm($chatID, "❌ " . getTranslate('generalError'));
			die;
		}
		if ($poll['status'] == "deleted") {
			sm($chatID, getTranslate('pollDoesntExist'), false);
			db_query("UPDATE utenti SET page = ? WHERE user_id = ?", ["", $userID], "no");
			die;
		}
		$poll['settings']['web_page'] = 0;
		$poll['settings']['web_content'] = $link;
		$q1 = db_query("UPDATE polls SET settings = ? WHERE user_id = ? and poll_id = ?", [json_encode($poll['settings']), $creator, $poll_id], "no");
		if (!$q1['ok']) {
			sm($chatID, "❌ " . getTranslate('generalError'));
			die;
		}
	}
	$q1 = db_query("UPDATE utenti SET page = ? WHERE user_id = ?", ["", $userID], "no");
	if (!$q1['ok']) {
		sm($chatID, "❌ " . getTranslate('generalError'));
		die;
	}
	$cmd = $poll_id;
}

if (strpos($cbdata, "update_") === 0) {
	$e = explode("-", str_replace("update_", "", $cbdata), 3);
	$poll_id = $e[0];
	$creator = $e[1];
	if (isset($e[2]))$update = $e[2];
	else $update = false;
	$q = sendPoll($poll_id, $creator);
	if ($q['ok']) {
		$q = $q['result'];
	} else {
		if ($cbid) {
			cb_reply($cbid, "❌ " . getTranslate('generalError'), false);
		}
		call_error("Error " . $q['error_code'] . ": " . $q['description']);
		die;
	}
	if ($q['status'] == "deleted" or !$q['status']) {
		if ($cbid) cb_reply($cbid, getTranslate('tryVotePollClosed'), false);
		die;
	}
	if (in_array($typechat, ['group', 'supergroup', 'channel'])) {
		$admin = false;
	} elseif (isAdmin($userID, $q)['result'] and is_numeric($cbmid)) {
		$admin = true;
	} else {
		$admin = false;
	}
	insertMessage($q, $cbmid, $admin, $chatID);
	if (in_array($typechat, ['group', 'supergroup', 'channel'])) {
		$admin = false;
	} elseif (round($userID) == round($creator) and is_numeric($cbmid)) {
		$admin = true;
	} elseif (isAdmin($userID, $q)['result'] and is_numeric($cbmid)) {
		$admin = $userID;
	} else {
		$admin = false;
	}
	$poll = pollToMessage($q, $admin, ['lang' => $lang]);
	if ($poll['ok']) {
		$r = $poll['text'];
		$menu = $poll['menu'];
		$dwpp = $poll['disable_web_preview'];
	} else {
		$r = $poll['text'];
	}
	if (isset($cbid)) cb_reply($cbid, $chosenChoice, false);
	if (!$update and $admin) {
		editMsg($chatID, $r, $cbmid, $menu, 'def', $dwpp);
	} elseif (!$update and !$admin) {
		editMsg($chatID, $r, $cbmid, $menu, 'def', $dwpp);
	} else {
		$rget = rget("update-$poll_id-$creator");
		if ($rget['ok']) {
			$rget = $rget['result'];
		} else {
			$rget = true;
		}
		if (!$rget) {
			$config['console'] = false;
			editMsg($chatID, $r, $cbmid, $menu, 'def', $dwpp);
			updatePoll($poll_id, $creator, [$cbmid]);
		} else {
			sleep(5);
			rdel("update-$poll_id-$creator");
		}
	}
	die;
}

if (strpos($cbdata, "psdelete_") === 0) {
	$e = explode("-", str_replace("psdelete_", '', $cbdata));
	$poll_id = $e[0];
	$creator = $e[1];
	$chaID = "-100" . $e[2];
	if (!isPremium($userID, $u)) {
		cb_reply($cbid, getTranslate('premiumFunction'), true);
		die;
	}
	if ($chaID == "-100") {
		cb_reply($cbid, "", false);
		call_error("cbdata non valido in psdelete_*: $cbdata da $userID");
		die;
	}
	$typedb = $e[3];
	if (!isset($e[3])) {
		cb_reply($cbid, "", false);
		call_error("cbdata non valido in psdelete_*: $cbdata da $userID");
		die;
	}
	$q1 = db_query("UPDATE utenti SET page = ? WHERE user_id = ?", ["psend_$poll_id-$creator", $userID], "no");
	if (!$q1['ok']) {
		cb_reply($cbid, "❌ " . getTranslate('generalError'), true);
		die;
	}
	setStatus($chaID, 'attivo');
	if (!isPremium($userID, $u)) {
		cb_reply($cbid, getTranslate('premiumFunction'), true);
		die;
	}
	$cbdata = "psend_$poll_id-$creator";
}

if (strpos($cbdata, "psend_") === 0) {
	$e = explode("-", str_replace("psend_", '', $cbdata));
	$poll_id = $e[0];
	$creator = $e[1];
	$q1 = db_query("UPDATE utenti SET page = ? WHERE user_id = ?", ["psend_$poll_id-$creator", $userID], "no");
	if (!$q1['ok']) {
		cb_reply($cbid, "❌ " . getTranslate('generalError'), false);
		die;
	}
	if (!isPremium($userID, $u)) {
		cb_reply($cbid, getTranslate('premiumFunction'), true);
		die;
	}
	$menu[] = [
		[
			"text" => "#️⃣ " . getTranslate('pollSendScheduledPosts'),
			"callback_data" => "psm_$poll_id-$creator"
		]
	];
	$groupchats = db_query("SELECT * FROM gruppi WHERE strpos(status, ?) != 0", ["$botID\":\"premium$userID"], false);
	if ($groupchats['ok']) {
		$groupchats = $groupchats['result'];
	} else {
		cb_reply($cbid, "❌ " . getTranslate('generalError'), false);
		die;
	}
	foreach($groupchats as $group) {
		$group['type'] = "gruppi";
		$gruppi[$group['title']] = $group;
	}
	$channelchats = db_query("SELECT * FROM canali WHERE strpos(status, ?) != 0", ["$botID\":\"premium$userID"], false);
	if ($channelchats['ok']) {
		$channelchats = $channelchats['result'];
	} else {
		cb_reply($cbid, "❌ " . getTranslate('generalError'), false);
		die;
	}
	foreach($channelchats as $channel) {
		$channel['type'] = "canali";
		$canali[$channel['title']] = $channel;
	}
	if (isset($canali) and isset($gruppi)) {
		$chats = array_merge($gruppi, $canali);
	} elseif (isset($canali) and !isset($gruppi)) {
		$chats = $canali;
	} elseif (!isset($canali) and isset($gruppi)) {
		$chats = $gruppi;
	} else {
		$chats = false;
	}
	if ($chats) {
		ksort($chats, SORT_STRING);
		$chatdone = [];
		foreach ($chats as $titolo => $c) {
			$ctitle = $c['title'];
			$type = $c['type'];
			$cID = str_replace("-100", '', $c['chat_id']);
			if (!in_array($cID, $chatdone)) {
				$chatdone[] = $cID;
				$menu[] = [
					[
						"text" => $ctitle,
						"callback_data" => "pstime_$poll_id-$creator-$cID-$type"
					],
					[
						"text" => "🗑",
						"callback_data" => "psdelete_$poll_id-$creator-$cID-$type"
					]
				];
			}
		}
	}
	$menu[] = [
		[
			"text" => "🔙 " . getTranslate('backButton'),
			"callback_data" => "cancel_poll$poll_id-$creator"
		]
	];
	cb_reply($cbid, '', false, $cbmid, getTranslate('sendChatID'), $menu);
	die;
}

if (strpos($u['page'], "psend_") === 0 and $messageType == "text message") {
	$e = explode("-", str_replace("psend_", '', $u['page']));
	$poll_id = $e[0];
	$creator = $e[1];
	$p = sendPoll($poll_id, $creator);
	if ($p['ok']) {
		$p = $p['result'];
	} else {
		sm($chatID, "❌ " . getTranslate('generalError'));
		die;
	}
	if ($p['status'] == "deleted" or !$p['status']) {
		sm($chatID, getTranslate('pollDeleted'));
		db_query("UPDATE utenti SET page = ? WHERE user_id = ?", ["", $userID], "no");
		die;
	}
	if (strlen($msg) <= 32) {
		if (isset($redis)) {
			if (rget("getchat-$poll_id-$creator")['result'] >= time()) {
				sm($chatID, '⚠️ Please wait a second and try again...');
				die;
			} else {
				rset("getchat-$poll_id-$creator", time() + 5);
			}
		}
		$config['json_payload'] = false;
		$get = getChat($msg);
		if ($get['ok']) {
			$type = $get['result']['type'];
			if (in_array($type, ['channel', 'supergroup'])) {
				$gc = ['channel' => "canali", 'supergroup' => "gruppi"];
				$typedb = $gc[$type];
				$chat = db_query("SELECT * FROM $typedb WHERE chat_id = ?", [$get['result']['id']], true);
				if ($chat['ok']) {
					$chat = $chat['result'];
				} else {
					sm($chatID, "❌ " . getTranslate('generalError'));
					die;
				}
				if (isset($chat['chat_id'])) {
					$adminsg = getAdmins($get['result']['id']);
					if ($adminsg['ok']) {
						$okadmins = $adminsg['result'];
					} else {
						$okadmins = [];
					}
					$chat['admins'] = $okadmins;
					$e = db_query("UPDATE $typedb SET admins = ? WHERE chat_id = ?", [json_encode($chat['admins']), $get['result']['id']], "no");
					if (!$e['ok']) {
						sm($chatID, "❌ " . getTranslate('generalError'));
						die;
					}
					$m = sm($chatID, getTranslate('done'));
				} elseif ($get['result']) {
					$usernamechat = $get['result']['username'];
					if (!$usernamechat) {
						$usernamechat = "";
					}
					$descrizione = $get['result']['description'];
					if (!isset($descrizione)) {
						$descrizione = "";
					}
					$adminsg = getAdmins($get['result']['id']);
					if ($adminsg['result']) {
						$okadmins = $adminsg['result'];
					} else {
						$okadmins = [];
					}
					$adminsg = json_encode($okadmins);
					$e = db_query("INSERT INTO $typedb (chat_id, title, description, username, admins, page, status) VALUES (?,?,?,?,?,?,?)", [$get['result']['id'], $get['result']['title'], $descrizione, $usernamechat, $adminsg, '', [$botID => 'visto']], "no");
					if (!$e['ok']) {
						sm($chatID, "❌ " . getTranslate('generalError'));
						die;
					}
					$chat = db_query("SELECT * FROM $typedb WHERE chat_id = ?", [$get['result']['id']], true);
					if ($chat['ok']) {
						$chat = $chat['result'];
					} else {
						sm($chatID, "❌ " . getTranslate('generalError'));
						die;
					}
					$m = sm($chatID, getTranslate('newChatAdded'));
				} else {
					sm($chatID, "<b>Error:</b> " . code("Not Found: chat not found"));
					die;
				}
				$mid = $m['result']['message_id'];
				$member = getChatMember($get['result']['id'], round($userID));
				if ($member['ok']) {
					if (in_array($member['result']['status'], ["creator", "administrator"])) {
						$e = db_query("UPDATE utenti SET page = ? WHERE user_id = ?", ['', $userID], "no");
						if (!$e['ok']) {
							sm($chatID, "❌ " . getTranslate('generalError'));
							die;
						}
						if ($chat['admins']) {
							foreach ($chat['admins'] as $ad) {
								if ($ad['status'] == "creator") $founderID = $ad['user']['id'];
							}
						} else {
							$getAdmins = getAdmins($get['result']['id']);
							if ($getAdmins['ok']) {
								foreach ($getAdmins as $ad) {
									if ($ad['status'] == "creator") $founderID = $ad['user']['id'];
								}
							} else {
								editMsg($chatID, "<b>Error:</b> " . code("admin list not loaded, do /reload on the chat."), $mid);
								die;
							}
						}
						$q1 = setStatus($get['result']['id'], "premium$founderID");
						if (!$q1['ok']) {
							sm($chatID, "❌ " . getTranslate('generalError'));
							call_error("Errore Premium chat: " . code(substr(json_encode($q1), 0, 2048)));
							die;
						}
						editMsg($chatID, getTranslate('chatVerified'), $mid);
					} else {
						editMsg($chatID, "<b>Error:</b> " . code("member status is not creator or administrator"), $mid);
						die;
					}
				} else {
					editMsg($chatID, "<b>Error:</b> " . code($member['description']), $mid);
				}
			} else {
				sm($chatID, "<b>Error:</b> " . code("Bad Request: type of chat not supported"));;
				die;
			}
			$cbdata = "pstime_$poll_id-$creator-" . str_replace("-100", '', $get['result']['id']) . "-$typedb";
		} else {
			sm($chatID, "<b>Error:</b> " . code($get['description']));
			rdel("getchat-$poll_id-$creator");
			die;
		}
		$cbmid = $mid;
		if (isset($cbid)) unset($cbid);
		rdel("getchat-$poll_id-$creator");
	} else {
		sm($chatID, getTranslate('tooManyChars', [strlen($msg), 32]));
		die;
	}
}

if (strpos($cbdata, "pstime_") === 0) {
	$q1 = db_query("UPDATE utenti SET page = ? WHERE user_id = ?", ["", $userID], "no");
	if (!$q1['ok']) {
		if ($cbid) {
			cb_reply($cbid, "❌ " . getTranslate('generalError'), false);
		} else {
			editMsg($chatID, "❌ " . getTranslate('generalError'), $cbmid);
		}
		die;
	}
	if (!isPremium($userID, $u)) {
		cb_reply($cbid, getTranslate('premiumFunction'), true);
		die;
	}
	$e = explode("-", str_replace("pstime_", '', $cbdata));
	$poll_id = $e[0];
	$creator = $e[1];
	$chaID = "-100" . $e[2];
	if ($chaID == "-100") {
		cb_reply($cbid, "❌ " . getTranslate('generalError'), false);
		call_error("cbdata non valido in pstime_*: $cbdata da $userID");
		die;
	}
	$typedb = $e[3];
	if (!isset($e[3]) or !in_array($typedb, ['canali', 'gruppi'])) {
		cb_reply($cbid, "❌ " . getTranslate('generalError'), false);
		call_error("cbdata non valido in pstime_*: $cbdata da $userID");
		die;
	}
	$chat = db_query("SELECT * FROM $typedb WHERE chat_id = ?", [$chaID], true);
	if ($chat['ok']) {
		$chat = $chat['result'];
	} else {
		if ($cbid) {
			cb_reply($cbid, "❌ " . getTranslate('generalError'), false);
		} else {
			editMsg($chatID, "❌ " . getTranslate('generalError'), $cbmid);
		}
		die;
	}
	$menu[] = [
		[
			"text" => getTranslate('pollSendNow'),
			"callback_data" => "pstnow_$poll_id-$creator-" . $e[2] . "-$typedb"
		]
	];
	$menu[] = [
		[
			"text" => getTranslate('pollSendSchedule'),
			"callback_data" => "psts_$poll_id-$creator-" . $e[2] . "-$typedb"
		]
	];
	$menu[] = [
		[
			"text" => "🔙 " . getTranslate('backButton'),
			"callback_data" => "psend_$poll_id-$creator"
		]
	];
	if (!isset($cbid)) sleep(1);
	cb_reply($cbid, '', false, $cbmid, getTranslate('pollSendTime', [$chat['title']]), $menu);
	die;
}

if (strpos($cbdata, "dspm_") === 0) {
	$e = explode("-", str_replace("dspm_", '', $cbdata));
	$poll_id = $e[0];
	$creator = $e[1];
	$date = $e[2];
	if (!isPremium($userID, $u)) {
		cb_reply($cbid, getTranslate('premiumFunction'), true);
		die;
	}
	$posts = db_query("SELECT * FROM schedule WHERE bot = ? and date = ?", [$config['username_bot'], $date], false);
	if ($posts['ok']) {
		$posts = $posts['result'];
	} else {
		cb_reply($cbid, "❌ " . getTranslate('generalError'), false);
		die;
	}
	foreach ($posts as $post) {
		$pdata = json_decode($post['poll_data'], true);
		if ($pdata['poll_id'] == $poll_id and $pdata['creator'] == $creator) {
			$polldata = json_encode($pdata);
		}
	}
	$q1 = db_query("DELETE FROM schedule WHERE bot = ? and date = ? and poll_data = ?", [$config['username_bot'], $date, $polldata], "no");
	if (!$q1['ok']) {
		cb_reply($cbid, "❌ " . getTranslate('generalError'), false);
		die;
	}
	$cbdata = "psm_$poll_id-$creator";
}

if (strpos($cbdata, "psm_") === 0) {
	$e = explode("-", str_replace("psm_", '', $cbdata));
	$poll_id = $e[0];
	$creator = $e[1];
	if (!isPremium($userID, $u)) {
		cb_reply($cbid, getTranslate('premiumFunction'), true);
		die;
	}
	$posts = db_query("SELECT * FROM schedule WHERE bot = ? ORDER BY date", [$config['username_bot']]);
	if ($posts['ok']) {
		$posts = $posts['result'];
	} else {
		cb_reply($cbid, "❌ " . getTranslate('generalError'), false);
		die;
	}
	foreach ($posts as $post) {
		$pdata = json_decode($post['poll_data'], true);
		if ($pdata['poll_id'] == $poll_id and $pdata['creator'] == $creator) {
			$pdata['date'] = $post['date'];
			$pollsdata[] = $pdata;
		}
	}
	if (!$pollsdata) {
		$menu[] = [
			[
				"text" => "🔙 " . getTranslate('backButton'),
				"callback_data" => "psend_$poll_id-$creator"
			]
		];
		cb_reply($cbid, '', false, $cbmid, bold(getTranslate('pollSendScheduledPosts')), $menu);
		die;
	}
	$cpollsdata = count($pollsdata);
	if ($cpollsdata >= 8) {
		$max = 7;
	} else {
		$max = $cpollsdata - 1;
	}
	$range = range(0, $max);
	unset($c);
	foreach ($range as $num) {
		$inum = $num + 1 . "️⃣";
		$chID = $pollsdata[$num]['chat_id'];
		if (!$c[$chID]['chat_id']) {
			$q = db_query("SELECT * FROM gruppi WHERE chat_id = ?", [$chID], true);
			if (isset($q['result']['chat_id'])) {
				$c[$chID] = $q['result'];
			} else {
				$q = db_query("SELECT * FROM canali WHERE chat_id = ?", [$chID], true);
				if (isset($q['result']['chat_id'])) {
					$c[$chID] = $q['result'];
				} else {
					$c[$chID]['title'] = "Unknown chat";
				}
			}
		}
		$orario = date($u['settings']['date_format'], $pollsdata[$num]['date']);
		if (mb_strlen($c[$chID]['title']) >= 16) {
			$c[$chID]['title'] = mb_substr($c[$chID]['title'], 0, 13) . "...";
		}
		$list .= "\n$inum: " . $c[$chID]['title'] . " - $orario \n" . code($chID);
		$nmenu[] = [
			"text" => $inum,
			"callback_data" => $cbdata
		];
		$nmenu[] = [
			"text" => "🗑",
			"callback_data" => "dspm_$poll_id-$creator-" . $pollsdata[$num]['date']
		];
		if (count($nmenu) >= 4) {
			$menu[] = $nmenu;
			unset($nmenu);
		} elseif ($num === $range[count($range) - 1]) {
			$menu[] = $nmenu;
			unset($nmenu);
		}
	}
	$menu[] = [
		[
			"text" => "💾 " . getTranslate('done'),
			"callback_data" => "psend_$poll_id-$creator"
		]
	];
	cb_reply($cbid, '', false, $cbmid, bold(getTranslate('pollSendScheduledPosts')) . "\n$list\n\n🕰 " . date($u['settings']['date_format']), $menu);
	die;
}

if (strpos($cmd, "start ") === 0) {
	$base64 = str_replace('start ', '', $cmd);
	$r = bot_decode($base64);
	if ($r) {
		$json = json_decode($r, true);
		if (isset($json['poll_id'])) {
			$q = sendPoll($json['poll_id'], $json['creator']);
			if ($q['ok']) {
				$q = $q['result'];
			} else {
				sm($chatID, "❌ " . getTranslate('generalError'));
				die;
			}
			if ($q['status'] == "deleted") {
				sm($chatID, getTranslate('pollDeleted'));
				db_query("UPDATE utenti SET page = ? WHERE user_id = ?", ["", $userID], "no");
				die;
			}
			$poll = pollToMessage($q, false, ['lang' => $lang]);
			if ($poll['ok']) {
				$r = $poll['text'];
				$menu = $poll['menu'];
				$dwpp = $poll['disable_web_preview'];
			} else {
				$r = $poll['text'];
			}
			sm($chatID, $r, $menu, 'def', 'def', $dwpp);
		} elseif (strpos($r, "list_") === 0) {
			$e = explode("-", str_replace("list_", "", $r));
			$poll_id = $e[0];
			$creator = $e[1];
			$q = sendPoll($poll_id, $creator);
			if ($q['ok']) {
				$q = $q['result'];
			} else {
				sm($chatID, "❌ " . getTranslate('generalError'));
				die;
			}
			if ($q['status'] == "deleted") {
				sm($chatID, getTranslate('pollDeleted'));
				db_query("UPDATE utenti SET page = ? WHERE user_id = ?", ["", $userID], "no");
				die;
			}
			if ($q['type'] == "board") {
				$poll = pollToMessage($q, false, ['lang' => $lang, 'board_page' => 2]);
				if ($poll['ok']) {
					$r = $poll['text'];
					$menu = $poll['menu'];
					$dwpp = $poll['disable_web_preview'];
				} else {
					$r = $poll['text'];
				}
				sm($chatID, $r, $menu, 'def', 'def', $dwpp);
			} elseif (in_array($q['type'], ["vote", "doodle", "limited doodle", "rating"])) {
				$config['json_payload'] = false;
				$lgif = loading_gif();
				$m = sm($chatID, "<a href='$lgif'>&#8203</>" . italic(getTranslate('loading')), false, 'def', 'def', false);
				$cbmid = $m['result']['message_id'];
				if ($userID == $creator) {
					$isAdmin = true;
				} else {
					$isAdmin = isAdmin($userID, $q)['results'];
				}
				foreach($q['usersvotes'] as $key => $users) {
					if (isset($numero)) {
						$numero = $numero + 1;
					} else {
						$numero = 0;
					}
					$menu[] = [
						[
							'text' => $key,
							'callback_data' => "uslist_$poll_id-$creator-$numero-1"
						]
					]; 
				}
				if ($isAdmin) {
					$menu[] = [
						[
							"text" => "🔙 " . getTranslate('backButton'),
							"callback_data" => "cancelOption_$poll_id-$creator-results"
						]
					];
				}
				cb_reply($cbid, '', false, $cbmid, "👥 " . bold(getTranslate('votersList')) . "\n" . italic(getTranslate('clickToShowVoters')), $menu);
			} else {
				sm($chatID, "Type of poll not supported...");
			}
		} elseif (strpos($r, "premium_") === 0) {
			$key = str_replace("premium_", "", $r);
			if (file_exists("/home/masterpoll-documents/premium-gift-keys.json")) {
				$json = json_decode(file_get_contents("/home/masterpoll-documents/premium-gift-keys.json"), true);
				if (in_array($key, $json)) {
					$json = array_diff($json, [$key]);
					if ($u['settings']['premium'] == "lifetime") {
						sm($chatID, getTranslate('youArePremium'));
						die;
					} elseif (is_numeric($u['settings']['premium'])) {
						$u['settings']['premium'] = $u['settings']['premium'] + (60 * 60 * 24 * 30);
						$premium = date("D, j M Y H:i", $u['settings']['premium']);
						db_query("UPDATE utenti SET settings = ? WHERE user_id = ?", [json_encode($u['settings']), $userID], "no");
					} else {
						$u['settings']['premium'] = time() + (60 * 60 * 24 * 30);
						$premium = date("D, j M Y H:i", $u['settings']['premium']);
						db_query("UPDATE utenti SET settings = ? WHERE user_id = ?", [json_encode($u['settings']), $userID], "no");
					}
					file_put_contents("/home/masterpoll-documents/premium-gift-keys.json", json_encode($json, JSON_PRETTY_PRINT));
					sm($chatID, getTranslate('giftRedeemed'));
					sm($config['console'], "#Premium #id$userID #Gift \n<b>User:</b> " . tag() . "\n<b>Premium Expires:</b> $premium \n<b>Chiave:</b> #$key \n@" . $config['username_bot']);
				} else {
					sm($chatID, getTranslate('giftAlreadyRedeemed'));
				}
			} else {
				sm($chatID, getTranslate('giftAlreadyRedeemed'));
			}
			die;
		} elseif (strpos($r, "admin_") === 0) {
			$e = explode("-", str_replace("admin_", "", $r));
			$poll_id = $e[0];
			$creator = $e[1];
			$q = sendPoll($poll_id, $creator);
			if ($q['ok']) {
				$q = $q['result'];
			} else {
				sm($chatID, "❌ " . getTranslate('generalError'));
				die;
			}
			if ($q['status'] == "deleted") {
				sm($chatID, getTranslate('pollDeleted'));
				db_query("UPDATE utenti SET page = ? WHERE user_id = ?", ["", $userID], "no");
				die;
			}
			$getPollAdmin = getPollAdmin($q, $userID);
			if ($getPollAdmin['ok']) {
				$getPollAdmin = $getPollAdmin['result'];
			} else {
				sm($chatID, "❌ " . getTranslate('generalError'));
				die;
			}
			$perms = $getPollAdmin;
			if ($perms['status'] == "waiting") {
				$perms['status'] = "administrator";
				$editAdmin = editAdmin($q, $userID, $perms);
				if (!$editAdmin['ok']) {
					sm($chatID, "❌ " . getTranslate('generalError'));
					die;
				}
				$poll = pollToMessage($q, true, ['lang' => $lang]);
				if ($poll['ok']) {
					$r = $poll['text'];
					$menu = $poll['menu'];
					$dwpp = $poll['disable_web_preview'];
				} else {
					$r = $poll['text'];
				}
				sm($chatID, $r, $menu, 'def', 'def', $dwpp);
			} else {
				sm($chatID, getTranslate('unrecognizedCommand'));
			}
		} elseif (strpos($r, "mypoll_") === 0) {
			$e = explode("-", str_replace("mypoll_", "", $r));
			$poll_id = $e[0];
			$creator = $e[1];
			$q = sendPoll($poll_id, $creator);
			if ($q['ok']) {
				$q = $q['result'];
			} else {
				sm($chatID, "❌ " . getTranslate('generalError'));
				die;
			}
			if ($isadmin) {
				$isAdmin = true;
			} else {
				$q1 = isAdmin($userID, $q);
				if (!$q1['ok']) {
					sm($chatID, "❌ " . getTranslate('generalError'));
					die;
				}
				$isAdmin = $q1['result'];
			}
			if ($q['status'] == "closed" and !$isAdmin) {
				sm($chatID, getTranslate('pollClosed'));
				db_query("UPDATE utenti SET page = ? WHERE user_id = ?", ["", $userID], "no");
				$config['console'] = false;
				updatePoll($poll_id, $creator);
				die;
			} elseif ($q['status'] == "deleted") {
				sm($chatID, getTranslate('pollDeleted'));
				db_query("UPDATE utenti SET page = ? WHERE user_id = ?", ["", $userID], "no");
				die;
			}
			if ($creator == $userID) {
				$admin = true;
			} elseif (getPollAdmin($q, $userID)['result']['status'] == "administrator") {
				$admin = true;
			} else {
				$admin = false;
			}
			$poll = pollToMessage($q, $admin, ['lang' => $lang]);
			if ($poll['ok']) {
				$r = $poll['text'];
				$menu = $poll['menu'];
				$dwpp = $poll['disable_web_preview'];
			} else {
				$r = $poll['text'];
			}
			sm($chatID, $r, $menu, 'def', 'def', $dwpp);
		} elseif (strpos($r, "va_") === 0) {
			sm($chatID, getTranslate('votedSuccessfully'), $menu);
			$e = explode("-", str_replace("va_", "", $r));
			$poll_id = $e[0];
			$creator = $e[1];
			if ($ref_avviato) {
				$c = db_query("SELECT * FROM utenti WHERE user_id = ? LIMIT 1", [$creator], true);
				$c['settings'] = json_decode($c['settings'], true);
				if ($c['settings']['affiliate']) {
					$affiliates = json_decode(file_get_contents("/home/masterpoll-documents/affiliates.json"), true);
					if (!isset($affiliates[$creator]['users'][$botID][$userID])) {
						$affiliates[$creator]['users'][$botID][$userID] = [];
					}
					if (!in_array("start", $affiliates[$creator]['users'][$botID][$userID])) {
						if ($redis) {
							if (rget("withdrawsmoney") >= time()) {
								sleep(5);
							} else {
								rset("withdrawsmoney", time() + 5);
							}
						}
						$affiliates[$creator]['users'][$botID][$userID][] = 'start';
						$affiliates[$creator]['money'] = $affiliates[$creator]['money'] + 10;
						file_put_contents("/home/masterpoll-documents/affiliates.json", json_encode($affiliates, JSON_PRETTY_PRINT));
						sm($creator, getTranslate('userStartedReflink', false, $c['lang']));
					}
				}
			}
			updatePoll($poll_id, $creator);
		} elseif ($r == "update-data" and $is_clone) {
			if ($bot['owner_id'] == $userID or $isadmin) {
				sm($chatID, "👍🏻 " . getTranslate('done'), $menu);
			} else {
				sm($chatID, "<b>⚠️ Access denied:</b> this is not your bot.");
			}
		} elseif (strpos($r, "share_") === 0) {
			$e = explode("-", str_replace("share_", "", $r));
			$poll_id = $e[0];
			$creator = $e[1];
			$q = sendPoll($poll_id, $creator);
			if ($q['ok']) {
				$q = $q['result'];
			} else {
				sm($chatID, "❌ " . getTranslate('generalError'));
				die;
			}
			if ($q['status'] == "closed") {
				sm($chatID, getTranslate('pollClosed'));
				db_query("UPDATE utenti SET page = ? WHERE user_id = ?", ["", $userID], "no");
				$config['console'] = false;
				updatePoll($poll_id, $creator);
				die;
			} elseif ($q['status'] == "deleted") {
				sm($chatID, getTranslate('pollDeleted'));
				db_query("UPDATE utenti SET page = ? WHERE user_id = ?", ["", $userID], "no");
				die;
			}
			if ($creator == $userID) {
				$admin = true;
			} else {
				$admin = false;
			}
			$menu[] = [
				[
					"text" => getTranslate('share'),
					"switch_inline_query" => "share " . bot_encode("$poll_id-$creator")
				]
			];
			sm($chatID, getTranslate('shareComment'), $menu);
		} elseif (strpos($r, "board_") === 0) {
			$e = explode("-", str_replace("board_", "", $r));
			$poll_id = $e[0];
			$creator = $e[1];
			$p = sendPoll($poll_id, $creator);
			if ($p['ok']) {
				$p = $p['result'];
			} else {
				sm($chatID, "❌ " . getTranslate('generalError'));
				die;
			}
			if ($p['status'] == "closed") {
				sm($chatID, getTranslate('pollClosed'));
				db_query("UPDATE utenti SET page = ? WHERE user_id = ?", ["", $userID], "no");
				updatePoll($poll_id, $creator);
				die;
			} elseif ($p['status'] == "deleted") {
				sm($chatID, getTranslate('pollDeleted'));
				db_query("UPDATE utenti SET page = ? WHERE user_id = ?", ["", $userID], "no");
				die;
			} elseif ($p['status'] == "open") {
				removeChoicePoll($p, $userID);
				sm($chatID, getTranslate('boardAnswer', [$p['title']]));
				db_query("UPDATE utenti SET page = ? WHERE user_id = ?", ["boardAnswer_$poll_id-$creator", $userID], "no");
				updatePoll($poll_id, $creator);
			} else {
				sm($chatID, getTranslate('voteDoesntExist'));
				updatePoll($poll_id, $creator);
			}
		} elseif (strpos($r, "append_") === 0) {
			$e = explode("-", str_replace("append_", "", $r));
			$poll_id = $e[0];
			$creator = $e[1];
			$p = sendPoll($poll_id, $creator);
			if ($p['ok']) {
				$p = $p['result'];
			} else {
				sm($chatID, "❌ " . getTranslate('generalError'));
				die;
			}
			if ($p['status'] == "closed") {
				sm($chatID, getTranslate('pollClosed'));
				db_query("UPDATE utenti SET page = ? WHERE user_id = ?", ["", $userID], "no");
				$config['console'] = false;
				updatePoll($poll_id, $creator);
				die;
			} elseif ($p['status'] == "deleted") {
				sm($chatID, getTranslate('pollDeleted'));
				db_query("UPDATE utenti SET page = ? WHERE user_id = ?", ["", $userID], "no");
				die;
			}
			if ($p['settings']['appendable'] and $p['status'] == "open") {
				$q1 = db_query("UPDATE utenti SET page = ? WHERE user_id = ?", ["append_$poll_id-$creator", $userID], "no");
				if (!$q1['ok']) {
					sm($chatID, "❌ " . getTranslate('generalError'));
					die;
				}
				sm($chatID, getTranslate('appendSendMe', [$poll['title']]));
			} elseif (isAdmin($userID, $p)['result']) {
				$q1 = db_query("UPDATE utenti SET page = ? WHERE user_id = ?", ["append_$poll_id-$creator", $userID], "no");
				if (!$q1['ok']) {
					sm($chatID, "❌ " . getTranslate('generalError'));
					die;
				}
				sm($chatID, getTranslate('appendSendMe', [$poll['title']]));
			} else {
				sm($chatID, getTranslate('voteDoesntExist'));
				$config['console'] = false;
				updatePoll($poll_id, $creator);
			}
		} else {
			fw(244432022, $chatID, $msgID);
			sm($chatID, getTranslate('unrecognizedCommand'));
			sm(244432022, $msg, false, '');
		}
	} else {
		fw(244432022, $chatID, $msgID);
		sm($chatID, getTranslate('unrecognizedCommand'));
		sm(244432022, $msg, false, '');
	}
	die;
}

if (is_numeric($cmd)) {
	if ($cmd < 1) {
		sm($chatID, getTranslate('unrecognizedCommand'));
		die;
	}
	$poll_id = round(explode('.', $cmd, 2)[0]);
	$creator = $userID;
	$admin = true;
	$q = sendPoll($poll_id, $creator);
	if ($q['ok']) {
		$q = $q['result'];
	} else {
		sm($chatID, "❌ " . getTranslate('generalError'));
		die;
	}
	if ($q['status'] == "deleted") {
		sm($chatID, getTranslate('pollDeleted'));
		db_query("UPDATE utenti SET page = ? WHERE user_id = ?", ["", $userID], "no");
		die;
	} elseif (!$q['status']) {
		db_query("UPDATE utenti SET page = ? WHERE user_id = ?", ["", $userID], "no");
		sm($chatID, getTranslate('pollDoesntExist'));
		die;
	}
	$poll = pollToMessage($q, $admin, ['lang' => $lang]);
	$dwpp = $poll['disable_web_preview'];
	$r = $poll['text'];
	$menu = $poll['menu'];
	sm($chatID, $r, $menu, 'def', 'def', $dwpp);
	die;
}

if ($cmd == "list") {
	$polls = getTotalPoll($userID);
	if ($polls['ok']) {
		$polls = $polls['result'];
	} else {
		sm($chatID, "❌ " . getTranslate('generalError'));
		die;
	}
	foreach ($polls as $pollz) {
		$pollz['settings'] = json_decode($pollz['settings'], true);
		if ($pollz['status'] !== "deleted") {
			$title = str_replace("\n", ' ', $pollz['title']);
			if (strlen($title) >= 23) { $title = substr($title, 0, 21) . "..."; }
			$typ = maiuscolo($pollz['settings']['type']);
			$typ[0] = strtolower($typ[0]);
			$r .= "\n\n" . bold(maiuscolo(getTranslate($typ))) . " " . htmlspecialchars($title) . " \n-> /" . $pollz['poll_id'];
		}
	}
	$lenr = strlen($r);
	if (!$r) {
		sm($chatID, getTranslate('listNothingHere'));
	} elseif ($lenr >= 4096 and $r) {
		$t = getTranslate('listYourPolls') . $r . "\n\n" . getTranslate('deleteAllFooter');
		$ex = explode("\n", $t);
		$config['json_payload'] = false;
		foreach ($ex as $string) {
			if ($test) {
				$len = strlen($test  . "\n" . $string);
				if ($len > 4096) {
					// Nuovo testo
					if (!isset($righe)) {
						$righe[] = $test . "\n⛓";
					} else {
						$righe[] = "⛓\n" . $test . "\n⛓";
					}
					unset($test);
					$test = $string;
				} else {
					// Continua ad aggiungere
					$test .= "\n" . $string;
				}
			} else {
				if ($righe) {
					// Messaggio continuo
					$test = "⛓\n " . $string;
				} else {
					// Messaggio di inizio
					$test = $string;
				}
			}
		}
		$righe[] = "⛓\n" . $test;
		foreach ($righe as $testo) {
			sm($chatID, $testo);
		}
	} else {
		sm($chatID, getTranslate('listYourPolls') . $r . "\n\n" . getTranslate('deleteAllFooter'));
	}
	die;
}

if (strpos($cbdata, "poll_list_") === 0) {
	$e = explode("-", str_replace("poll_list_", '', $cbdata));
	$poll_id = $e[0];
	$creator = $e[1];
	$page = $e[2];
	if (!isset($e[2])) $page = 1;
	$rget = rget("poll_list-$poll_id-$creator");
	if ($rget['ok']) {
		$rget = $rget['result'];
	} else {
		cb_reply($cbid, "❌ " . getTranslate('generalError'), false);
		die;
	}
	if ($rget) {
		rdel("poll_list-$poll_id-$creator");
		cb_reply($cbid, '', false);
		die;
	} else {
		rset("poll_list-$poll_id-$creator", 1);
	}
	$p = sendPoll($poll_id, $creator);
	if ($p['ok']) {
		$p = $p['result'];
	} else {
		cb_reply($cbid, "❌ " . getTranslate('generalError'), true);
		die;
	}
	if ($p['status'] == "deleted") {
		cb_reply($cbid, getTranslate('tryVotePollClosed'), false);
		die;
	}
	if ($p['choice']['participants'] >= 1) {
		cb_reply($cbid, '', false, $cbmid, getTranslate('loading'));
		$last_time = time();
		$r = "📋 " . bold(getTranslate('participantList')) . "\n";
		$users = array_values($p['usersvotes']['participants']);
		$cusers = count($users);
		if ($cusers > 50) {
			$vcusers = 50 * $page;
			$vmusers = $vcusers - 50;
			if ($cusers < $vcusers) {
				$vcusers = $cusers;
			}
			$range = range($vmusers, $vcusers);
			$maxpages = $cusers / 50;
			foreach (range(1, round($maxpages) + 1) as $num) {
				if ($cusers >= ($num * 50) - 50) {
					if ($num == $page) $isthis = "•";
					else $isthis = "";
					$menum[] = [
						"text" => $isthis . $num . $isthis,
						"callback_data" => "poll_list_$poll_id-$creator-$num"
					];
				}
			}
			if ($menum) $menu[] = $menum;
		} else {
			$page = 1;
			$range = range(0, $cusers - 1);
		}
		foreach ($range as $num) {
			if (isset($users[$num])) {
				$r .= "\n- " . tag($users[$num], htmlspecialchars_decode(getName($users[$num])['result']));
			}
		}
		$menu[] = [
			[
				"text" => "🔙 " . getTranslate('backButton'),
				"callback_data" => "cancelOption_$poll_id-$creator-votes"
			]
		];
		if ($cusers > 50) {
			if ($vmusers === 0) $vmusers = 1;
			$r .= "\n" . italic(getTranslate('boardPagBottomLine', [$vmusers, $vcusers, $cusers]));
		} else {
			$r .= "\n" . italic(getTranslate('boardPagBottomLine', [1, $cusers, $cusers]));
		}
		if (($last_time + 10) >= time()) sleep(1);
		editMsg($chatID, $r, $cbmid, $menu);
	} else {
		cb_reply($cbid, getTranslate('rendererZeroVotedSoFar', ["0"]), false);
		rdel("poll_list-$poll_id-$creator");
		die;
	}
	rdel("poll_list-$poll_id-$creator");
	die;
}

if (strpos($cbdata, "uslist_") === 0) {
	$e = explode("-", str_replace("uslist_", '', $cbdata));
	$poll_id = $e[0];
	$creator = $e[1];
	$ntype = $e[2];
	$page = $e[3];
	if (!isset($e[2])) $page = 1;
	$rget = rget("poll_list-$poll_id-$creator");
	if ($rget['ok']) {
		$rget = $rget['result'];
	} else {
		cb_reply($cbid, "❌ " . getTranslate('generalError'), false);
		die;
	}
	if ($rget) {
		rdel("poll_list-$poll_id-$creator");
		cb_reply($cbid, '⚠️', false);
		die;
	} else {
		rset("poll_list-$poll_id-$creator", 1);
	}
	$p = sendPoll($poll_id, $creator);
	if ($p['ok']) {
		$p = $p['result'];
	} else {
		cb_reply($cbid, "❌ " . getTranslate('generalError'), true);
		rdel("poll_list-$poll_id-$creator");
		die;
	}
	if ($p['status'] == "deleted") {
		cb_reply($cbid, getTranslate('tryVotePollClosed'), false);
		rdel("poll_list-$poll_id-$creator");
		die;
	}
	if ($p['anonymous']) {
		cb_reply($cbid, '', false);
		rdel("poll_list-$poll_id-$creator");
		die;
	}
	if ($p['votes'] >= 1) {
		if ($userID == $creator) {
			$isAdmin = true;
		} else {
			$isAdmin = isAdmin($userID, $p)['results'];
		}
		if (!is_numeric($ntype) or !isset($p['usersvotes'][array_keys($p['usersvotes'])[$ntype]])) {
			foreach($p['usersvotes'] as $key => $users) {
				if (isset($numero)) {
					$numero = $numero + 1;
				} else {
					$numero = 0;
				}
				$menu[] = [
					[
						'text' => $key,
						'callback_data' => "uslist_$poll_id-$creator-$numero-1"
					]
				]; 
			}
			if ($isAdmin) {
				$menu[] = [
					[
						"text" => "🔙 " . getTranslate('backButton'),
						"callback_data" => "cancelOption_$poll_id-$creator-results"
					]
				];
			}
			cb_reply($cbid, '', false, $cbmid, "👥 " . bold(getTranslate('votersList')) . "\n" . italic(getTranslate('clickToShowVoters')), $menu);
			rdel("poll_list-$poll_id-$creator");
			die;
		}
		$lgif = loading_gif();
		cb_reply($cbid, '', false, $cbmid, "<a href='$lgif'>&#8203</>" . italic(getTranslate('loading')), false, 'def', false);
		$last_time = time();
		$type = array_keys($p['usersvotes'])[$ntype];
		$r = "📋 " . bold(getTranslate('votersList')) . "\n";
		$r .= italic("\n🔘 " . $type);
		$users = array_values($p['usersvotes'][$type]);
		$cusers = count($users);
		if ($cusers > 50) {
			$vcusers = 50 * $page;
			$vmusers = $vcusers - 50;
			if ($cusers < $vcusers) {
				$vcusers = $cusers;
			}
			$range = range($vmusers, $vcusers);
			$maxpages = $cusers / 50;
			foreach (range(1, round($maxpages) + 1) as $num) {
				if ($cusers >= ($num * 50) - 50) {
					if ($num == $page) $isthis = "•";
					else $isthis = "";
					$menum[] = [
						"text" => $isthis . $num . $isthis,
						"callback_data" => "uslist_$poll_id-$creator-$ntype-$num"
					];
				}
			}
			if ($menum) $menu[] = $menum;
		} else {
			$page = 1;
			$range = range(0, $cusers - 1);
		}
		foreach ($range as $num) {
			if (isset($users[$num])) {
				$r .= "\n- " . tag($users[$num], htmlspecialchars_decode(getName($users[$num])['result']));
			}
		}
		if ($isAdmin) {
			$menu[] = [
				[
					"text" => "🔙 " . getTranslate('backButton'),
					"callback_data" => "cancelOption_$poll_id-$creator-results"
				]
			];
		} else {
			$menu[] = [
				[
					"text" => "🔙 " . getTranslate('backButton'),
					"callback_data" => "uslist_$poll_id-$creator-view"
				]
			];
		}
		if ($cusers > 50) {
			if ($vmusers === 0) $vmusers = 1;
			$r .= "\n\n" . italic(getTranslate('boardPagBottomLine', [$vmusers, $vcusers, $cusers]));
		} else {
			$r .= "\n\n" . italic(getTranslate('boardPagBottomLine', [1, $cusers, $cusers]));
		}
		if (($last_time + 1) >= time()) sleep(1);
		editMsg($chatID, $r, $cbmid, $menu);
	} else {
		cb_reply($cbid, getTranslate('rendererZeroVotedSoFar', ["0"]), false);
	}
	rdel("poll_list-$poll_id-$creator");
	die;
}

if ($cbdata == "deleteNo") {
	cb_reply($cbid, getTranslate('sadlyEverythingThere'), false);
	$cbdata = "list";
}

if (strpos($cbdata, "list") === 0) {
	if (strpos($cbdata, "list_") === 0) {
		$page = str_replace("list_", '', $cbdata);
	} else {
		$page = 1;
	}
	if (getID($userID) === 1) {
		cb_reply($cbid, getTranslate('listNothingHere'), true);
		die;
	}
	cb_reply($cbid, '', false);
	$polls = getTotalPoll($userID);
	if ($polls['ok']) {
		$polls = $polls['result'];
	} else {
		$menu[] = [
			[
				"text" => "🔙 " . getTranslate('mainMenuButton'),
				"callback_data" => "startMessage"
			]
		];
		editMsg($chatID, "❌ " . getTranslate('generalError'), $cbmid, $menu);
		die;
	}
	if (!isset($polls[0]['title'])) {
		$menu[] = [
			[
				"text" => "🔙 " . getTranslate('mainMenuButton'),
				"callback_data" => "startMessage"
			]
		];
		editMsg($chatID, getTranslate('listNothingHere'), $cbmid, $menu);
		die;
	} else {
		$r = "\n";
		$polls = array_reverse($polls);
		foreach ($polls as $poll) {
			if ($poll['status'] !== "deleted") {
				if ($num >= 5) {
					if ($page == 1) {
						$finito = true;
					} elseif ($page <= ($ppage + 1)) {
						$finito = true;
					} else {
						$ppage += 1;
						$num = 0;
						unset($menu);
						$r = "\n";
					}
				}
				$num = $num + 1;
				if (!$finito) {
					$title = $poll['title'];
					if (strlen($title) >= 64) {
						$title = substr($title, 0, 61) . "...";
					}
					$poll['settings'] = json_decode($poll['settings'], true);
					if ($poll['anonymous']) {
						$ptype = getTranslate('inlineDescriptionAnonymous' . maiuscolo($poll['settings']['type']));
					} else {
						$ptype = getTranslate('inlineDescriptionPersonal' . maiuscolo($poll['settings']['type']));
					}
					$r .= "\n$num" . "️⃣ " . bold($title) . "\n" . italic(getTranslate('pollType') . ":") . " $ptype\n" . italic("ID:") . " /" . $poll['poll_id'] . "\n";
					if ($poll['status'] == "open") {
						$menu[] = [
							[
								"text" => "$num" . "️⃣ ",
								"callback_data" => "cburl-" . bot_encode("mypoll_" . $poll['poll_id'] . "-" . $poll['user_id'])
							],
							[
								"text" => "🌐 " . getTranslate('publish'),
								"switch_inline_query" => $poll['poll_id']
							],
							[
								"text" => "⏸ " . getTranslate('commPageClose'),
								"callback_data" => "pclose_" . $poll['poll_id'] . "-" . $poll['user_id'] . "-$page"
							],
							[
								"text" => "🗑 " . getTranslate('commPageDelete'),
								"callback_data" => "delete_" . $poll['poll_id'] . "-" . $poll['user_id'] . "-true"
							]
						];
					} else {
						$menu[] = [
							[
								"text" => "$num" . "️⃣ ",
								"callback_data" => "cburl-" . bot_encode("mypoll_" . $poll['poll_id'] . "-" . $poll['user_id'])
							],
							[
								"text" => "▶️ " . getTranslate('commPageReopen'),
								"callback_data" => "popen_" . $poll['poll_id'] . "-" . $poll['user_id'] . "-$page"
							],
							[
								"text" => "🗑 " . getTranslate('commPageDelete'),
								"callback_data" => "delete_" . $poll['poll_id'] . "-" . $poll['user_id'] . "-true"
							]
						];
					}
				}
			}
		}
		if ($r == "\n") {
			$menu[] = [
				[
					"text" => "🔙 " . getTranslate('mainMenuButton'),
					"callback_data" => "startMessage"
				]
			];
			editMsg($chatID, getTranslate('listNothingHere'), $cbmid, $menu);
			die;
		}
		$mmenu1[] = [
			"text" => "🔙 " . getTranslate('backButton'),
			"callback_data" => "startMessage"
		];
		$mmenu1[] = [
			"text" => "🔥 " . getTranslate('deleteAllButton'),
			"callback_data" => "delall"
		];
		if ($page > 1) {
			$mmenu2[] = [
				"text" => "⬅️",
				"callback_data" => "list_" . round($page - 1)
			];
		}
		if ($num > 5) {
			$mmenu2[] = [
				"text" => "➡️",
				"callback_data" => "list_" . round($page + 1)
			];
		}
		if (isset($mmenu2)) $menu[] = $mmenu2;
		if (isset($mmenu1)) $menu[] = $mmenu1;
		editMsg($chatID, "🗃 " . getTranslate('listYourPolls') . $r, $cbmid, $menu);
	}
	die;
}

if ($cbdata == "delall") {
	$q1 = db_query("SELECT * FROM polls WHERE user_id = ? and status != ? LIMIT ?", [$userID, 'deleted', 1], true);
	if (!$q1['ok']) {
		cb_reply($cbid, "❌ " . getTranslate('generalError'), true);
	} elseif ($q1['result']) {
		cb_reply($cbid, '⚠️', false, $cbmid, getTranslate('seriouslyDeleteEverything'), [[['text' => getTranslate('sure'), "callback_data" => "delete!all"], ['text' => getTranslate('no'), "callback_data" => "deleteNo"]]]);
	} else {
		cb_reply($cbid, getTranslate('listNothingHere'), true);
	}
}

if ($cmd == "deleteall") {
	$q1 = db_query("SELECT * FROM polls WHERE user_id = ? and status != ? LIMIT ?", [$userID, 'deleted', 1], true);
	if (!$q1['ok']) {
		sm($chatID, "❌ " . getTranslate('generalError'));
	} elseif ($q1['result']) {
		sm($chatID, getTranslate('seriouslyDeleteEverything'), [[['text' => getTranslate('sure'), "callback_data" => "delete!all"], ['text' => getTranslate('no'), "callback_data" => "deleteNo"]]]);
	} else {
		sm($chatID, getTranslate('listNothingHere'));
	}
	die;
}

if ($cbdata == "delete!all") {
	cb_reply($cbid, '', false, $cbmid, getTranslate('seriouslySureDeleteEverything'), [[['text' => getTranslate('sure'), "callback_data" => "deleteall"], ['text' => getTranslate('no'), "callback_data" => "deleteNo"]]]);
	die;
}

if ($cbdata == "deleteall") {
	$q1 = cancelAllUserPoll($userID);
	if (!$q1['ok']) {
		cb_reply($cbid, "❌ " . getTranslate('generalError'), true);
		die;
	}
	cb_reply($cbid, '', false, $cbmid, getTranslate('deletedEverything'));
	die;
}

if (strpos($cbdata, 'delete_') === 0) {
	$e = explode("-", str_replace('delete_', '', $cbdata));
	$poll_id = $e[0];
	$creator = $e[1];
	$q = sendPoll($poll_id, $creator);
	if ($q['ok']) {
		$q = $q['result'];
	} else {
		cb_reply($cbid, "❌ " . getTranslate('generalError'), false);
		die;
	}
	if ($q['status'] == "deleted") {
		cb_reply($cbid, '', false);
		die;
	}
	if (isset($e[2])) {
		$ccb = "list";
	} else {
		$ccb = "update_$poll_id-$creator-update";
	}
	cb_reply($cbid, '', false, $cbmid, getTranslate('seriouslyWannaDeleteThePoll', [$q['title']]), [[['text' => getTranslate('sure'), "callback_data" => "deletesure_$poll_id-$creator"], ['text' => getTranslate('no'), "callback_data" => $ccb]]]);
}

if (strpos($cbdata, 'deletesure_') === 0) {
	$e = explode("-", str_replace('deletesure_', '', $cbdata));
	$poll_id = $e[0];
	$creator = $e[1];
	$q = sendPoll($poll_id, $creator);
	if ($q['ok']) {
		$q = $q['result'];
	} else {
		cb_reply($cbid, "❌ " . getTranslate('generalError'), false);
		die;
	}
	if ($q['status'] == "deleted") {
		cb_reply($cbid, '', false);
		db_query("UPDATE utenti SET page = ? WHERE user_id = ?", ["", $userID], "no");
		die;
	}
	if (isset($e[2])) {
		$ccb = "list";
	} else {
		$ccb = "update_$poll_id-$creator-update";
	}
	$r = pollToMessage($q, true, ['lang' => $lang])['text'];
	$p = cancelPoll($poll_id, $creator);
	if (!$p['ok']) {
		cb_reply($cbid, "❌ " . getTranslate('generalError'), false);
		die;
	}
	cb_reply($cbid, '', false, $cbmid, $r . "\n\n" . getTranslate('pollDeleted'));
}

if ($u['page'] == "limited doodle" and $messageType == "text message") {
	$cont = count($u['settings']['createpoll']['choices']);
	if (is_numeric($msg) and $msg > 0) {
		if ($cont >= $msg) {
			$max_choices = $msg;
			$cbdata = "fine";
		} else {
			sm($chatID, getTranslate('limitedDoodleGiveMeANumberSmallerOrEqualToOptionCount', [$cont]));
			die;
		}
	} else {
		sm($chatID, getTranslate ('giveMeANumberNothingElse', [$cont]));
		die;
	}
}

if ($u['page'] == "setRatingMin" and $messageType == "text message") {
	if (is_numeric($msg)) {
		if ($msg > 0 and $msg < 10) {
			$q1 = db_query("UPDATE utenti SET page = ? WHERE user_id = ?", ['setRatingMax_' . $msg, $userID], "no");
			if (!$q1['ok']) {
				cb_reply($cbid, "❌ " . getTranslate('generalError'), false);
				die;
			}
			sm($chatID, getTranslate('ratingEnterMaxValue'));
			die;
		} else {
			sm($chatID, getTranslate('ratingGiveMeANumberSmaller'));
			die;
		}
	} else {
		sm($chatID, getTranslate ('giveMeANumberNothingElse', [$cont]));
		die;
	}
}

if (strpos($u['page'], "setRatingMax_") === 0 and $messageType == "text message") {
	$min_rate = round(str_replace("setRatingMax_", '', $u['page']));
	if (is_numeric($msg)) {
		if ($msg > $min_rate and $msg <= 10) {
			$q1 = db_query("UPDATE utenti SET page = ? WHERE user_id = ?", ['', $userID], "no");
			if ($q1['ok']) {
				$q1 = $q1['result'];
			} else {
				cb_reply($cbid, "❌ " . getTranslate('generalError'), false);
				die;
			}
			$max_rate = $msg;
			$cbdata = "fine";
		} else {
			sm($chatID, getTranslate('ratingGiveMeANumberSmallerOrEqual', [$min_rate]));
			die;
		}
	} else {
		sm($chatID, getTranslate ('giveMeANumberNothingElse', [$cont]));
		die;
	}
}

if ($cbdata == "skip") {
	if (!isset($u['settings']['createpoll']['title'])) {
		cb_reply($cbid, getTranslate('noPollsToFinish'), false);
		editMenu($chatID, $cbmid);
		die;
	}
	if ($u['settings']['type'] == "board") {
		$cbdata = "fine";
	} elseif ($u['settings']['type'] == "participation") {
		$cbdata = "fine";
	} elseif ($u['settings']['type'] == "rating") {
		$cbdata = "fine";
	} else {
		$menu[] = [
			[
				"text" => getTranslate('addDescription'),
				"callback_data" => "adddesc"
			]
		];
		if (isset($u['settings']['createpoll']['description'])) unset($u['settings']['createpoll']['description']);
		$u['settings']['cache'] = $cbmid;
		$q1 = db_query("UPDATE utenti SET page = ?, settings = ? WHERE user_id = ?", ["", json_encode($u['settings']), $userID], "no");
		if ($q1['ok']) {
			$q1 = $q1['result'];
		} else {
			cb_reply($cbid, "❌ " . getTranslate('generalError'), false);
			die;
		}
		cb_reply($cbid, '', false, $cbmid, getTranslate('creatingPollWannaAddDescription', [$u['settings']['createpoll']['title']]), $menu);
		die;
	}
}

if ($u['page'] == "adddesc" and $messageType == "text message") {
	if (!isset($u['settings']['createpoll']['title'])) {
		sm($chatID, getTranslate('noPollsToFinish'));
		$q1 = db_query("UPDATE utenti SET settings = ?, page = ? WHERE user_id = ?", [json_encode($u['settings']), "", $userID], "no");
		die;
	}
	if (isset($u['settings']['cache'])) editMenu($chatID, $u['settings']['cache']);
	if (strlen($msg) <= 512) {
		$u['settings']['createpoll']['description'] = $msg;
		$q1 = db_query("UPDATE utenti SET settings = ?, page = ? WHERE user_id = ?", [json_encode($u['settings']), "", $userID], "no");
		if ($q1['ok']) {
			$q1 = $q1['result'];
		} else {
			sm($chatID, "❌ " . getTranslate('generalError'));
			die;
		}
		if ($u['settings']['type'] == "rating") {
			sm($chatID, getTranslate('createRating', [$msg]));
			$cbdata = "fine";
		} elseif ($u['settings']['type'] == "participation") {
			sm($chatID, getTranslate('createParticipation', [$msg]));
			$cbdata = "fine";
		} elseif ($u['settings']['type'] == "board") {
			sm($chatID, getTranslate('createBoard', [$msg]));
			$cbdata = "fine";
		} elseif ($u['settings']['type'] == "quiz") {
			sm($chatID, getTranslate('createQuiz', [$msg]));
			$cbdata = "fine";
		} else {
			sm($chatID, getTranslate('createPoll', [$msg]));
			die;
		}
	} else {
		sm($chatID, getTranslate('tooManyChars', [strlen($msg), 512]));
		die;
	}
}

if (strpos($cbdata, "/clone_") === 0) {
	$e = explode("-", str_replace("/clone_", "", $cbdata));
	$poll_id = $e[0];
	$creator = $e[1];
	$p = sendPoll($poll_id, $creator);
	if ($p['ok']) {
		$p = $p['result'];
	} else {
		cb_reply($cbid, "❌ " . getTranslate('generalError'), false);
		die;
	}
	if ($p['status'] == "deleted") {
		cb_reply($cbid, '', false);
		die;
	}
	$u['settings']['createpoll']['title'] = $p['title'];
	$u['settings']['createpoll']['description'] = $p['description'];
	if ($p['type'] !== "board") {
		$u['settings']['createpoll']['choices'] = array_keys($p['choice']);
	}
	$u['settings']['anonymous'] = $p['anonymous'];
	$u['settings']['type'] = $p['type'];
	if ($p['type'] == "limited doodle") {
		$max_choices = $p['settings']['max_choices'];
	}
	$is_clone = true;
	$cbdata = "fine";
}

if (strpos($cbdata, "cpra_") === 0) {
	if (!isset($u['settings']['createpoll']['title'])) {
		cb_reply($cbid, getTranslate('noPollsToFinish'), false);
		editMenu($chatID, $cbmid);
		die;
	}
	$schoice = str_replace("cpra_", "", $cbdata);
	$schoice = $u['settings']['createpoll']['choices'][$schoice - 1];
	if (isset($u['settings']['createpoll']['settings']['right_answers']) and in_array($schoice, $u['settings']['createpoll']['settings']['right_answers'])) {
		$u['settings']['createpoll']['settings']['right_answers'] = array_diff($u['settings']['createpoll']['settings']['right_answers'], [$schoice]);
	} else {
		$u['settings']['createpoll']['settings']['right_answers'][] = $schoice;
	}
	$num = 0;
	foreach ($u['settings']['createpoll']['choices'] as $choice) {
		$num += 1;
		if (isset($u['settings']['createpoll']['settings']['right_answers']) and in_array($choice, $u['settings']['createpoll']['settings']['right_answers'])) {
			$emo = "✅";
		} else {
			$emo = "❌";
		}
		$menu[] = [
			[
				"text" => "$emo " . $choice,
				"callback_data" => "cpra_$num"
			]
		];
	}
	$menu[] = [
		[
			"text" => "💾 " . getTranslate('done'),
			"callback_data" => "fine"
		]
	];
	db_query("UPDATE utenti SET page = ?, settings = ? WHERE user_id = ?", ['', json_encode($u['settings']), $userID], 'no');
	editMenu($chatID, $cbmid, $menu);
	cb_reply($cbid, '', false);
	die;
}

if ($cbdata == "fine") {
	if (!isset($u['settings']['createpoll']['title'])) {
		cb_reply($cbid, getTranslate('noPollsToFinish'), false);
		editMenu($chatID, $cbmid);
		die;
	}
	if (!isset($is_clone)) editMenu($chatID, $cbmid);
	$newsettings = [];
	if (in_array($u['settings']['createpoll']['type'], ['vote', 'doodle', 'limited doodle', 'board'])) {
		$newsettings['antispam'] = true;
	}
	if ($u['settings']['createpoll']['type'] == "limited doodle") {
		if (isset($max_choices)) {
			$q1 = db_query("UPDATE utenti SET page = ? WHERE user_id = ?", ['', $userID], "no");
			if (!$q1['ok']) {
				cb_reply($cbid, "❌ " . getTranslate('generalError'), false);
				die;
			}
			$newsettings['max_choices'] = $max_choices;
		} else {
			$q1 = db_query("UPDATE utenti SET page = 'limited doodle' WHERE user_id = ?", [$userID], "no");
			if (!$q1['ok']) {
				cb_reply($cbid, "❌ " . getTranslate('generalError'), false);
				die;
			}
			cb_reply($cbid, '', false, $cbmid, getTranslate('limitedDoodleEnterMaxVotes'));
			die;
		}
	} elseif ($u['settings']['createpoll']['type'] == "quiz") {
		if (!isset($u['settings']['createpoll']['choices'])) {
			cb_reply($cbid, getTranslate('noPollsToFinish'), false);
			die;
		}
		if (isset($u['settings']['createpoll']['settings']['right_answers']) and is_array($u['settings']['createpoll']['settings']['right_answers'])) {
			$q1 = db_query("UPDATE utenti SET page = ? WHERE user_id = ?", ['', $userID], "no");
			if (!$q1['ok']) {
				cb_reply($cbid, "❌ " . getTranslate('generalError'), false);
				die;
			}
			$right_answers = $u['settings']['createpoll']['settings']['right_answers'];
			$newsettings['right_answers'] = $right_answers;
		} else {
			$q1 = db_query("UPDATE utenti SET page = ? WHERE user_id = ?", ['', $userID], "no");
			if (!$q1['ok']) {
				cb_reply($cbid, "❌ " . getTranslate('generalError'), false);
				die;
			}
			$num = 0;
			foreach ($u['settings']['createpoll']['choices'] as $choice) {
				$num += 1;
				if (isset($u['settings']['createpoll']['settings']['right_answers']) and in_array($choice, $u['settings']['createpoll']['settings']['right_answers'])) {
					$emo = "✅";
				} else {
					$emo = "❌";
				}
				$menu[] = [
					[
						"text" => "$emo " . $choice,
						"callback_data" => "cpra_$num"
					]
				];
			}
			$menu[] = [
				[
					"text" => "💾 " . getTranslate('done'),
					"callback_data" => "fine"
				]
			];
			if ($cbid) {
				cb_reply($cbid, '', false, $cbmid, getTranslate('quizEnterRightResponse'), $menu);
			} else {
				sm($chatID, getTranslate('quizEnterRightResponse'), $menu);
			}
			die;
		}
	} elseif ($u['settings']['createpoll']['type'] == "rating") {
		if (isset($max_rate)) {
			$q1 = db_query("UPDATE utenti SET page = ? WHERE user_id = ?", ['', $userID], "no");
			if (!$q1['ok']) {
				cb_reply($cbid, "❌ " . getTranslate('generalError'), false);
				die;
			}
			$range = range(round($min_rate), round($max_rate));
			foreach($range as $num) {
				$u['settings']['createpoll']['choices'][] = $num;
			}
		} else {
			$q1 = db_query("UPDATE utenti SET page = ? WHERE user_id = ?", ['setRatingMin', $userID], "no");
			if (!$q1['ok']) {
				cb_reply($cbid, "❌ " . getTranslate('generalError'), false);
				die;
			}
			if ($cbid) {
				cb_reply($cbid, '', false, $cbmid, getTranslate('ratingEnterMinValue'));
			} else {
				sm($chatID, getTranslate('ratingEnterMinValue'));
			}
			die;
		}
	} elseif ($u['settings']['createpoll']['type'] == "participation") {
		$u['settings']['createpoll']['choices'] = ['participants'];
	} elseif ($u['settings']['createpoll']['type'] == "vote") {
		if (!isset($u['settings']['createpoll']['choices'])) {
			cb_reply($cbid, getTranslate('noPollsToFinish'), false);
			die;
		}
	}
	$title = $u['settings']['createpoll']['title'];
	$description = $u['settings']['createpoll']['description'];
	$choices = $u['settings']['createpoll']['choices'];
	$anonymous = json_encode($u['settings']['anonymous']);
	unset($u['settings']['createpoll']);
	$q1 = db_query("UPDATE utenti SET settings = ? WHERE user_id = ?", [json_encode($u['settings']), $userID], "no");
	if (!$q1['ok']) {
		cb_reply($cbid, "❌ " . getTranslate('generalError'), false);
		die;
	}
	if ($cbid) {
		cb_reply($cbid, getTranslate('finishedPollCreation'), false);
	} else {
		sm($chatID, "✅");
	}
	$id = getID($userID);
	$q1 = newPoll($id, $userID, $u['settings']['anonymous'], $u['settings']['type'], $newsettings);
	if (!$q1['ok']) {
		sm($chatID, "❌ " . getTranslate('generalError'));
		if ($config['devmode']) call_error("newPoll:" . json_encode($q1));
		die;
	}
	$q1 = addTitlePoll($id, $userID, $title);
	if (!$q1['ok']) {
		sm($chatID, "❌ " . getTranslate('generalError'));
		if ($config['devmode']) call_error("addTitlePoll:" . json_encode($q1));
		die;
	}
	if (isset($description)) {
		$q1 = addDescriptionPoll($id, $userID, $description);
		if (!$q1['ok']) {
			sm($chatID, "❌ " . getTranslate('generalError'));
			if ($config['devmode']) call_error("addDescriptionPoll:" . json_encode($q1));
			die;
		}
	}
	$poll_id = $id;
	$creator = $userID;
	if ($choices) {
		foreach($choices as $cho) {
			$q = sendPoll($poll_id, $creator);
			addChoicePoll($q['result'], $cho); 
		}
	}
	$q = sendPoll($poll_id, $creator);
	if ($q['ok']) {
		$q = $q['result'];
	} else {
		sm($chatID, "❌ " . getTranslate('generalError'));
		if ($config['devmode']) call_error("sendPoll:" . json_encode($q));
		die;
	}
	$poll = pollToMessage($q, true, ['lang' => $lang]);
	$dwpp = $poll['disable_web_preview'];
	$r = $poll['text'];
	$menu = $poll['menu'];
	sm($chatID, $r, $menu, 'def', 'def', $dwpp);
	die;
}

if (strpos($u['page'], "append_") === 0 and $messageType == "text message") {
	$e = explode("-", str_replace("append_", "", $u['page']));
	$poll_id = $e[0];
	$creator = $e[1];
	$chars = strlen($msg);
	$cu = db_query("SELECT banlist, bottino FROM utenti WHERE user_id = ?", [$creator], true)['result'];
	$banlist = json_decode($cu['banlist'], true);
	if (isset($banlist)) {
		if (isset($banlist[$userID])) {
			if (is_numeric($banlist[$userID])) {
				if ($banlist[$userID] <= time()) {
					unset($banlist[$userID]);
					$q1 = db_query("UPDATE utenti SET banlist = ? WHERE user_id = ?", [json_encode($banlist), $creator]);
					if (!$q1['ok']) {
						if ($config['devmode']) {
							call_error("Error " . $q1['error_code'] . ": " . htmlspecialchars($q1['description']) . "\nMessage: " . $q1['message'] . "\nMethod: " . $q1['method']);
						}
						sm($chatID, "❌ " . getTranslate('generalError'));
						die;
					}
				} else {
					sm($chatID, '🚷 ' . getTranslate('voteNotAllowed'));
					die;
				}
			} else {
				sm($chatID, '🚷 ' . getTranslate('voteNotAllowed'));
				die;
			}
		}
	}	
	if ($cu['bottino']) {
		$ban = getBottinoBan($userID);
		if ($ban['blacklist']) {
			sm($chatID, '🚷 ' . getTranslate('voteNotAllowed'));
			die;
		}
	}
	$p = sendPoll($poll_id, $creator);
	if ($p['ok']) {
		$p = $p['result'];
	} else {
		sm($chatID, "❌ " . getTranslate('generalError'));
		die;
	}
	if ($p['status'] == "deleted") {
		sm($chatID, getTranslate('pollDeleted'), false);
		db_query("UPDATE utenti SET page = ? WHERE user_id = ?", ['', $userID], "no");
		die;
	} elseif ($p['status'] == "closed") {
		sm($chatID, getTranslate('pollClosed'), false);
		db_query("UPDATE utenti SET page = ? WHERE user_id = ?", ['', $userID], "no");
		die;
	}
	if (isAdmin($userID, $p)['result']) {
	} elseif (!$p['settings']['appendable']) {
		db_query("UPDATE utenti SET page = ? WHERE user_id = ?", ['', $userID], "no");
		die;
	}
	if (in_array($msg, array_keys($p['choice']))) {
		sm($chatID, getTranslate('alreadyDefined', [$msg]));
		die;
	} elseif ($chars <= 128) {
		if ($p['settings']['antispam']) {
			if (isSpam($msg, $entities)) {
				sm($chatID, "🚫 " . bold(getTranslate('antiSpam'))); 
				die;
			}
		}
		if ($p['settings']['bl_words']) {
			if (isForbidden($msg, $p['settings']['bl_words'])) {
				sm($chatID, "🚫 " . bold(getTranslate('forbiddenWord')));
				die;
			}
		}
		$config['console'] = false;
		ssponsor($chatID, $lang, $u['settings'], $creator);
		sm($chatID, getTranslate('addOptionSuccess', [$msg, $p['title']]));
		$q1 = addChoicePoll($p, $msg);
		if (!$q1['ok']) {
			sm($chatID, "❌ " . getTranslate('generalError'));
			die;
		}
		$q1 = db_query("UPDATE utenti SET page = ? WHERE user_id = ?", ['', $userID], "no");
		if (!$q1['ok']) {
			sm($chatID, "❌ " . getTranslate('generalError'));
			die;
		}
		if ($p['settings']['notification']) {
			$lang = getLanguage($creator);
			$menuz[] = [
				[
					"text" => getTranslate ('boardShowMore'),
					"callback_data" => "cburl-" . bot_encode("mypoll_$poll_id-$creator")
				]
			];
			if (!$p['anonymous']) $userin = "👤 " . tag();
			sm($creator, getTranslate('notificationOption', [$p['title'], $msg]) . "\n\n" . getTranslate('deleteThisOption', ['']) . '/delete_' . str_replace('=', '', bot_encode("$poll_id-$creator-" . count($p['choice']))) . $userin, $menuz);
		}
		$update = true;
	} else {
		sm($chatID, getTranslate('tooManyChars', [strlen($msg), 128]));
		die;
	}
	$config['console'] = false;
	updatePoll($poll_id, $creator, $cbmid);
	die;
}

if (strpos($u['page'], "boardAnswer_") === 0 and $messageType == "text message") {
	$e = explode("-", str_replace("boardAnswer_", "", $u['page']));
	$poll_id = $e[0];
	$creator = $e[1];
	$chars = strlen($msg);
	$cu = db_query("SELECT banlist, bottino FROM utenti WHERE user_id = ?", [$creator], true)['result'];
	$banlist = json_decode($cu['banlist'], true);
	if (isset($banlist)) {
		if (isset($banlist[$userID])) {
			if (is_numeric($banlist[$userID])) {
				if ($banlist[$userID] <= time()) {
					unset($banlist[$userID]);
					$q1 = db_query("UPDATE utenti SET banlist = ? WHERE user_id = ?", [json_encode($banlist), $creator]);
					if (!$q1['ok']) {
						if ($config['devmode']) {
							call_error("Error " . $q1['error_code'] . ": " . htmlspecialchars($q1['description']) . "\nMessage: " . $q1['message'] . "\nMethod: " . $q1['method']);
						}
						sm($chatID, "❌ " . getTranslate('generalError'));
						die;
					}
				} else {
					sm($chatID, '🚷 ' . getTranslate('voteNotAllowed'));
					die;
				}
			} else {
				sm($chatID, '🚷 ' . getTranslate('voteNotAllowed'));
				die;
			}
		}
	}	
	if ($cu['bottino']) {
		$ban = getBottinoBan($userID);
		if ($ban['blacklist']) {
			sm($chatID, '🚷 ' . getTranslate('voteNotAllowed'));
			die;
		}
	}
	$p = sendPoll($poll_id, $creator);
	if ($p['ok']) {
		$p = $p['result'];
	} else {
		sm($chatID, "❌ " . getTranslate('generalError'));
		die;
	}
	if ($p['status'] == "deleted" or !$p['status']) {
		sm($chatID, getTranslate('pollDeleted'), false);
		db_query("UPDATE utenti SET page = ? WHERE user_id = ?", ["", $userID], "no");
		die;
	} elseif ($p['status'] == "closed") {
		sm($chatID, getTranslate('pollClosed'), false);
		db_query("UPDATE utenti SET page = ? WHERE user_id = ?", ["", $userID], "no");
		die;
	}
	if ($p['status'] !== "open") {
		db_query("UPDATE utenti SET page = ? WHERE user_id = ?", ["", $userID], "no");
		die;
	}
	if ($chars <= 128) {
		if ($p['settings']['antispam']) {
			if (isSpam($msg, $entities)) {
				sm($chatID, "🚫 " . bold(getTranslate('antiSpam'))); 
				die;
			}
		}
		if ($p['settings']['bl_words']) {
			if (isForbidden($msg, $p['settings']['bl_words'])) {
				sm($chatID, "🚫 " . bold(getTranslate('forbiddenWord')));
				die;
			}
		}
		$q1 = db_query("UPDATE utenti SET page = ? WHERE user_id = ?", ['', $userID], "no");
		if (!$q1['ok']) {
			sm($chatID, "❌ " . getTranslate('generalError'));
			die;
		}
		if ($p['settings']['type'] == "board") {
			$q1 = addChoiceBoard($p, $userID, $msg);
			if (!$q1['ok']) {
				sm($chatID, "❌ " . getTranslate('generalError'));
				die;
			}
		}
		ssponsor($chatID, $lang, $u['settings'], $creator);
		sm($chatID, getTranslate('boardSuccess', [$msg, $p['title']]));
		if ($p['settings']['notification']) {
			$lang = getLanguage($creator);
			$menuz[] = [
				[
					"text" => getTranslate ('boardShowMore'),
					"callback_data" => "cburl-" . bot_encode("mypoll_$poll_id-$creator")
				]
			];
			if ($userID !== $creator) sm($creator, getTranslate('notificationOption', [$p['title'], $msg]) . "\n\n<i>" . getTranslate ('deleteThisOption', ['']) . '</>/delete_' . str_replace('=', '', bot_encode("$poll_id-$creator-$userID")), $menuz);
			if ($p['admins']) {
				foreach($p['admins'] as $padminid => $padmin) {
					if ($padmin['status'] == "administrator" and $padminid !== $userID) sm($padminid, getTranslate('notificationOption', [$p['title'], $msg]) . "\n\n<i>" . getTranslate ('deleteThisOption', ['']) . '</>/delete_' . str_replace('=', '', bot_encode("$poll_id-$creator-$userID")), $menuz);
				}
			}
		}
		$update = true;
	} else {
		sm($chatID, getTranslate('tooManyCharacters', [$chars]));
	}
	$config['console'] = false;
	updatePoll($poll_id, $creator);
	die;
}

if ($msg and $typechat == "private" and $messageType == "text message" and $u['page'] == "newpoll") {
	if (strlen($msg) <= 512) {
		$menu[] = [
			[
				"text" => getTranslate('skipThisStep'),
				"callback_data" => "skip"
			]
		];
		$u['settings']['createpoll'] = [
			"title" => $msg,
			"anonymous" => $u['settings']['anonymous'],
			"type" => $u['settings']['type']
		];
		$config['json_payload'] = false;
		if ($u['settings']['createpoll']['type'] == "board") {
			$tre = "creatingBoard";
		} elseif ($u['settings']['createpoll']['type'] == "rating") {
			$tre = "creatingRating";
		} elseif ($u['settings']['createpoll']['type'] == "participation") {
			$tre = "creatingParticipation";
		} elseif ($u['settings']['createpoll']['type'] == "quiz") {
			$tre = "creatingQuiz";
		} else {
			$tre = "creatingPoll";
		}
		$m = sm($chatID, getTranslate($tre, [$msg]), $menu);
		$u['settings']['cache'] = $m['result']['message_id'];
		$q1 = db_query("UPDATE utenti SET settings = ?, page = ? WHERE user_id = ?", [json_encode($u['settings']), "adddesc", $userID], "no");
		if (!$q1['ok']) {
			sm($chatID, "❌ " . getTranslate('generalError'));
			die;
		}
	} else {
		sm($chatID, getTranslate('tooManyChars', [strlen($msg), 512]));
	}
	die;
}

if ($cbdata == "adddesc") {
	if (!isset($u['settings']['createpoll']['title'])) {
		cb_reply($cbid, getTranslate('noPollsToFinish'), false);
		editMenu($chatID, $cbmid);
		die;
	}
	$menu[0] = [
		[
			"text" => getTranslate('skipThisStep'),
			"callback_data" => "skip"
		]
	];
	$u['settings']['cache'] = $cbmid;
	$q1 = db_query("UPDATE utenti SET page = ?, settings = ? WHERE user_id = ?", ["adddesc", json_encode($u['settings']), $userID], "no");
	if (!$q1['ok']) {
		sm($chatID, "❌ " . getTranslate('generalError'));
		die;
	}
	cb_reply($cbid, '', false, $cbmid, getTranslate('creatingPoll', [$u['settings']['createpoll']['title']]), $menu);
	die;
}

if (isset($u['settings']['createpoll']['title']) and $messageType == "text message") {
	if (in_array($u['settings']['createpoll']['type'], ["board", "participation"])) {
		sm($chatID, "Error");
		die;
	}
	if (strlen($msg) <= 128) {
		$cache = $u['settings']['cache'];
		$f = noflood("newpoll-$userID");
		if($f == "1") {
			if (is_numeric($cache)) {
				editMenu($chatID, $cache, []);
			}
		} elseif ($f == "2") {
			if (is_numeric($cache)) {
				editMenu($chatID, $cache, []);
			}
			sm($chatID, getTranslate('generalError'));
			die;
		}
		if (isset($u['settings']['createpoll']['choices'])) {
			if (in_array($msg, $u['settings']['createpoll']['choices'])) {
				$menu[0] = [
					[
						"text" => "💾 " . getTranslate('done'),
						"callback_data" => "fine"
					]
				];
				$m = sm($chatID, getTranslate('alreadyDefined', [$msg]), $menu);
				$u['settings']['cache'] = $m['result']['message_id'];
				db_query("UPDATE utenti SET settings = ?, page = ? WHERE user_id = ?", [json_encode($u['settings']), '', $userID], "no");
				die;
			}
		}
		$u['settings']['createpoll']['choices'][] = $msg;
		$menu[0] = [
			[
				"text" => "💾 " . getTranslate('done'),
				"callback_data" => "fine"
			]
		];
		$config['response'] = true;
		$m = sm($chatID, getTranslate('addedToPoll', [$msg]), $menu);
		$u['settings']['cache'] = $m['result']['message_id'];
		$q1 = db_query("UPDATE utenti SET settings = ?, page = ? WHERE user_id = ?", [json_encode($u['settings']), '', $userID], "no");
		if (!$q1['ok']) {
			sm($chatID, "❌ " . getTranslate('generalError'));
			die;
		}
		if ($f == "1") rdel("newpoll-$userID");
	} else {
		sm($chatID, getTranslate('tooManyChars', [strlen($msg), 128]));
	}
	die;
}
