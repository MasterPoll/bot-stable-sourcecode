<?php 

if ($isadmin or $u['settings']['admin_perms']['gestione']) {
	function progressbar($now, $tot) {
		$p = ($now / $tot) * 100;
		if ($p <= 10) {
			return "🕑";
		} elseif ($p >=11 and $p <= 20) {
			return "🕒";
		} elseif ($p >= 21 and $p <= 30) {
			return "🕓";
		} elseif ($p >= 31 and $p <= 40) {
			return "🕔";
		} elseif ($p >= 41 and $p <= 50) {
			return "🕕";
		} elseif ($p >= 51 and $p <= 60) {
			return "🕖";
		} elseif ($p >= 61 and $p <= 70) {
			return "🕗";
		} elseif ($p >= 71 and $p <= 80) {
			return "🕘";
		} elseif ($p >= 81 and $p <= 90) {
			return "🕙";
		} elseif ($p >= 91 and $p <= 100) {
			return "🕛";
		} else {
			return $p;
		}
		return false;
	}
	
	if ($cmd == "system" and $isadmin) {
		$testo = bold("Statistiche di sistema 🗄");
		$disco_libero = disk_free_space("/");
		$disco_totale = disk_total_space("/");
		$disco_utilizzato = $disco_totale - $disco_libero;
		$testo .= "\n\nUtilizzo disco: " . round($disco_utilizzato / 1024 / 1024) . " su " . round($disco_totale / 1024 / 1024) . " MB (" . round($disco_utilizzato / $disco_totale * 100) . "%)";
		$ram_utilizzata = memory_get_usage(true);
		$ram_totale = $phpinfo['PHP Core']['memory_limit'];
		if ($ram_totale) {
			$testo .= "\nUtilizzo RAM: " . round($ram_utilizzata / 1024 / 1024) . " su " . round($ram_totale / 1024 / 1024) . " MB (" . round($ram_utilizzato / $ram_totale * 100) . "%)";
		} else {
			$testo .= "\nUtilizzo RAM: " . round($ram_utilizzata / 1024 / 1024, 1) . " MB";
		}
		$cpu = sys_getloadavg();
		$testo .= "\nCarico: " . json_encode($cpu);
		foreach($cpu as $loadm) {
			$cputot += $loadm;
		}
		$testo .= "\nMedia carico totale: " . round($cputot / count($cpu));
		sm($chatID, $testo);
		die;
	}
	
	if ($config['usa_il_db']) {
		
		if (strpos($cmd, "dc_") === 0 and $isadmin) {
			fastcgi_finish_request();
			$db = str_replace("dc_", '', $cmd);
			if (!in_array($db, ['utenti', 'gruppi', 'canali'])) {
				sm($chatID, "⚠️ Invalid database name");
				die;
			}
			if ($db == "utenti") {
				$type = "user_id";
			} else {
				$type = "chat_id";
			}
			$config['json_payload'] = false;
			$menu[] = [
				[
					"text" => "Annulla",
					"callback_data" => "finito"
				]
			];
			$m = sm($chatID, "Controllo il db $db...", $menu);
			$chats = db_query("SELECT * FROM $db", false, false);
			if ($chats['ok']) {
				$chats = $chats['result'];
			} else {
				editMsg($chatID, "❌ " . getTranslate('generalError'), $m['result']['message_id']);
				die;
			}
			$dbi = [
				'utenti' => "(user_id, nome, cognome, username, lang, page, settings, status, last_update) VALUES (?,?,?,?,?,?,?,?,?)",
				'gruppi' => "(chat_id, title, description, username, admins, page, status, permissions) VALUES (?,?,?,?,?,?,?,?)",
				'canali' => "(chat_id, title, description, username, admins, page, status) VALUES (?,?,?,?,?,?,?)"
			];
			$oks = $dbi[$db];
			$extime = time();
			$dchats = [];
			if (file_exists("finito")) unlink("finito");
			if ($chats) {
				foreach ($chats as $chat) {
					if (!file_exists("finito") and isset($chat)) {
						if (isset($chat[$type])) {
							if ($exists[$chat[$type]]) {
								if ($deleted[$chat[$type]]) {
									
								} else {
									$dchats[] = $chat[$type];
									if ($db == "utenti") {
										$dup .= "\n- " . bold($chat['nome']) . " (" . code($chat[$type]) . ")";
										$args = [
											$chat['user_id'],
											$chat['nome'],
											$chat['cognome'],
											$chat['username'],
											$chat['lang'],
											$chat['page'],
											$chat['settings'],
											$chat['status'],
											$chat['last_update']
										];
									} elseif ($db == "gruppi") {
										$dup .= "\n- " . bold($chat['title']) . " (" . code($chat[$type]) . ")";
										$args = [
											$chat['chat_id'],
											$chat['title'],
											$chat['description'],
											$chat['username'],
											$chat['admins'],
											$chat['page'],
											$chat['status'],
											$chat['permissions']
										];
									} elseif ($db == "canali") {
										$dup .= "\n- " . bold($chat['title']) . " (" . code($chat[$type]) . ")";
										$args = [
											$chat['chat_id'],
											$chat['title'],
											$chat['description'],
											$chat['username'],
											$chat['admins'],
											$chat['page'],
											$chat['status']
										];
									} else {
										die;
									}
									db_query("DELETE FROM $db WHERE $type = ?", [$chat[$type]], "no");
									$deleted[$chat[$type]] = true;
									sleep(1);
									db_query("INSERT INTO $db $oks", $args, "no");
								}
							} else {
								$exists[$chat[$type]] = true;
							}
						}
					} else {
						if (!$fineforzata) $fineforzata = true;
					}
					if ($extime + 1 < time()) {
						editMsg($chatID, count($dchats) . " chat duplicate sono state rimosse dal database $db.", $m['result']['message_id'], $menu);
						$extime = time();
					}
				}
			}
			sleep(1);
			editMsg($chatID, "✅ " . count($dchats) . " chat duplicate sono state rimosse dal database $db.\n$fineforzata", $m['result']['message_id']);
			die;
		}
		
		if ($cbdata == "finito") {
			file_put_contents("finito", "ok");
			cb_reply($cbid, 'Annullazione richiesta');
			die;
		}
		
		if (strpos($cmd, "key ") === 0) {
			$config['response'] = true;
			$ex = explode(" ", $cmd, 2);
			$key = $ex[1];
			$q = db_query("SELECT * FROM utenti WHERE strpos(settings, ?) != 0", [$key], true);
			if ($q['ok']) {
				$q = $q['result'];
			} else {
				sm($chatID, "❌ " . getTranslate('generalError'));
				die;
			}
			if ($q) {
				$cmd = "info " . $q['user_id'];
			} else {
				sm($chatID, "Utente non trovato...");
				die;
			}
		}
			
		if (strpos($cbdata, "adminperms_") === 0 and $isadmin) {
			$e = explode("-", str_replace('adminperms_', '', $cbdata), 2);
			$q = db_query("SELECT * FROM utenti WHERE user_id = ?", [$e[0]], true);
			if ($q['ok']) {
				$q = $q['result'];
			} else {
				cb_reply($cbid, "❌ " . getTranslate('generalError'), true);
				die;
			}
			$q['settings'] = json_decode($q['settings'], true);
			if (isset($e[1]) and in_array($e[1], $config['admin_perms'])) {
				if (isset($q['settings']['admin_perms'][$e[1]])) {
					unset($q['settings']['admin_perms'][$e[1]]);
				} else {
					$q['settings']['admin_perms'][$e[1]] = true;
				}
				$q1 = db_query("UPDATE utenti SET settings = ? WHERE user_id = ?", [json_encode($q['settings']), $e[0]], 'no');
				if ($q1['ok']) {
					$q1 = $q1['result'];
				} else {
					cb_reply($cbid, "❌ " . getTranslate('generalError'), true);
					die;
				}
			}
			foreach($config['admin_perms'] as $perm) {
				if (isset($q['settings']['admin_perms'][$perm])) {
					$act = "✅";
					$perms .= "\n$perm: $act";
				} else {
					$act = "❌";
					$perms .= "\n$perm: $act";
				}
				$menu[] = [
					[
						"text" => "$perm $act",
						"callback_data" => "adminperms_{$q['user_id']}-$perm"
					]
				];
			}
			$menu[] = [
				[
					"text" => "🔙 " . getTranslate('backButton'),
					"callback_data" => "updateuser_{$q['user_id']}"
				]
			];
			cb_reply($cbid, '', false, $cbmid, bold("Permessi di ") . tag($q['user_id'], $q['nome'], $q['cognome']) . "\n$perms", $menu);
			die;
		}
		
		if (strpos($cmd, "info ") === 0) {
			dm($chatID, $msgID);
			$ex = explode(" ", $cmd, 2);
			$id = str_replace("@", '', $ex[1]);
			$msid = sm($chatID, "Verifico i dati sui vari database...")['result']['message_id'];
			if (isset($id)) {
				$q = db_query("SELECT * FROM bots WHERE bot_id = ? or username_bot = ?", [round($id), $id], true);
				if ($q['ok']) {
					$q = $q['result'];
				} else {
					editMsg($chatID, "❌ " . getTranslate('generalError'), $m['result']['message_id']);
					die;
				}
				if (!$q['bot_id']) {
					$q = db_query("SELECT * FROM utenti WHERE user_id = ? or username = ?", [round($id), $id], true);
					if ($q['ok']) {
						$q = $q['result'];
					} else {
						editMsg($chatID, "❌ " . getTranslate('generalError'), $m['result']['message_id']);
						die;
					}
					if (!$q['user_id']) {
						$q = db_query("SELECT * FROM gruppi WHERE chat_id = ? or username = ?", [round($id), $id], true);
						if ($q['ok']) {
							$q = $q['result'];
						} else {
							editMsg($chatID, "❌ " . getTranslate('generalError'), $m['result']['message_id']);
							die;
						}
						if (!$q['chat_id']) {
							$q = db_query("SELECT * FROM canali WHERE chat_id = ? or username = ?", [round($id), $id], true);
							if ($q['ok']) {
								$q = $q['result'];
							} else {
								editMsg($chatID, "❌ " . getTranslate('generalError'), $m['result']['message_id']);
								die;
							}
							if (!$q['chat_id']) {
								editMsg($chatID, "Chat non trovata...1\n$id", $msid);
								die;
							} else {
								$type = "canale";
								$id = $q['chat_id'];
							}
						} else {
							$type = "gruppo";
							$id = $q['chat_id'];
						}
					} else {
						$type = "utente";
						$id = $q['user_id'];
					}
				} else {
					$type = "bot";
					$id = $q['bot_id'];
				}
			} else {
				editMsg($chatID, "Nessun ID o username inserito...", $msid);
				die;
			}
			$q['status'] = json_decode($q['status'], true);
			if (!is_array($q['status']) and $type !== "bot") $q['status'] = [];
			if ($type == "utente") {
				if ($redis) {
					// Fix aggiornamento del nome utente
					$redis->del("name$id");
				}
				if (!is_numeric($ex[1])) {
					// Aggiornamento delle info utente
					$config['response'] = true;
					username(str_replace("@", '', $ex[1]));
				}
				if ($isadmin) sm($chatID, "Tag: " . tag($id, htmlspecialchars_decode($q['nome']), htmlspecialchars_decode($q['cognome'])));
				if ($q['cognome']) $qcognome = "\nCognome: " . htmlspecialchars($q['cognome']);
				if ($q['username']) $qusername = "\nUsername: @" . htmlspecialchars($q['username']);
				$q['settings'] = json_decode($q['settings'], true);
				$languages_name = json_decode(file_get_contents("/home/masterpoll-documents/tg_languages_name.json"), true);
				$languages_flag = json_decode(file_get_contents("/home/masterpoll-documents/tg_languages_flag.json"), true);
				$ulanguage = "\nLanguage: " . $languages_flag[$q['lang']] . " " . maiuscolo($languages_name[$q['lang']]) . " (" . code($q['settings']['timezone']) . ")";
				$stati_user = [
					'blocked' => "Bloccato dall'utente",
					'deleted' => "Account eliminato",
					'bot' => "Bot",
					'avviato' => "Avviato",
					'attivo' => "Non avviato",
					'visto' => "Mai incontrato",
					'ban' => "Bannato Life Time"
				];
				if ($stati_user[$q['status'][$botID]]) {
					$stat = $stati_user[$q['status'][$botID]];
				} elseif (strpos($q['status'][$botID], "ban") === 0) {
					$dateban = str_replace("ban", '', $q['status'][$botID]);
					$stat = "Bannato fino a " . date($u['date_format'], $dateban);
				} else {
					$stat = json_encode($q['status']);
				}
				$ustato = "\nStato: $stat";
				if ($q['settings']['premium']) {
					if ($q['settings']['premium'] == "lifetime") {
						$p = "Life Time";
					} else {
						$p = date("D, j M Y H:i", $q['settings']['premium']);
					}
					$premium = "\nPremium: $p";
				}
				if ($q['settings']['token']) {
					$uapi = "\nAPI Key: " . code($q['settings']['token']);
				}
				if ($q['settings']['wallet']) {
					$uapi .= "\nPortafogli: €" . number_format($q['settings']['wallet'] / 100, 2);
				}
				if ($isadmin) {
					$menu[] = [
						[
							"text" => "✅️ Permessi",
							"callback_data" => "adminperms_$id"
						]
					];
				}
				$menu[] = [
					[
						"text" => "👮🏻‍♂️ Amministrazioni",
						"callback_data" => "useradmins_$id"
					]
				];
				$menu[] = [
					[
						"text" => "💰 Affiliato",
						"callback_data" => "useraffiliate_$id"
					],
					[
						"text" => "🔄 Aggiorna dati",
						"callback_data" => "updateuser_$id"
					]
				];
				$menu[] = [
					[
						"text" => "✅",
						"callback_data" => "fatto"
					]
				];
				editMsg($chatID, bold("Informazioni utente") . "\nID: $id \nNome: " . htmlspecialchars($q['nome']) . $qcognome . $qusername . $ulanguage . $uapi . $ustato . $premium . "\nUltimo accesso: " . date($u['settings']['date_format'], $q['last_update']) . "\nRegistrato: " . date($u['settings']['date_format'], $q['first_update']), $msid, $menu);
			} elseif ($type == "bot") {
				$t = "\nUsername: @" . htmlspecialchars($q['username_bot']);
				$mstatus = json_decode($q['master_status'], true);
				if ($mstatus) {
					$mstat = "🟢";
				} else {
					$mstat = "🔴";
				}
				$t .= "\nMaser Stato: $mstat";
				$status = json_decode($q['status'], true);
				if ($status) {
					$stat = "✅";
				} else {
					$stat = "❌";
				}
				$t .= "\nStato: $stat";
				$spremium = json_decode($q['premium'], true);
				if ($spremium) {
					$premium = "⭐️";
				} else {
					$premium = "❌";
				}
				$t .= "\nPremium: $premium";
				$t .= "\nProprietario: " . tag($q['owner_id'], $q['owner_id']);
				$menu[] = [
					[
						"text" => "👮🏻‍♂️ Creatore",
						"callback_data" => "updateuser_{$q['owner_id']}"
					],
					[
						"text" => "🔄 Aggiorna dati",
						"url" => "https://t.me/{$q['username_bot']}?start=" . bot_encode("update-data")
					]
				];
				$menu[] = [
					[
						"text" => "$mstat Master Stato",
						"callback_data" => "botmstatus_$id"
					],
					[
						"text" => "$stat Stato",
						"callback_data" => "botstatus_$id"
					],
					[
						"text" => "$premium Premium",
						"callback_data" => "botpremium_$id"
					]
				];
				$menu[] = [
					[
						"text" => "🔄 Aggiorna",
						"callback_data" => "updatebot_$id"
					],
					[
						"text" => "🗑 Elimina Bot",
						"callback_data" => "delbot_$id"
					]
				];
				$menu[] = [
					[
						"text" => "✅",
						"callback_data" => "fatto"
					]
				];
				editMsg($chatID, bold("Informazioni Bot") . "\nID: $id $t", $msid, $menu);
			} elseif ($type == "gruppo") {
				if ($q['username']) {
					$link = "https://t.me/" . $q['username'];
				} else {
					$link = "https://t.me/c/" . str_replace("-", '', str_replace("-100", '', $id)) . "/1";
					/*$q['permissions'] = json_decode($q['permissions'], true);
					$q['admins'] = json_decode($q['admins'], true);
					if ($q['permissions']) {
						if ($q['permissions']['can_invite_users']) getLink($id);
					} elseif ($q['admins']) {
						foreach($q['admins'] as $staff) {
							if ($staff['user_id'] == $botID and $staff['permissions']['can_invite_users']) $link = getLink($id);
						}
					}*/
				}
				if ($isadmin) sm($chatID, "Link: " . $link);
				if ($q['username']) $qusername = "\nUsername: @" . htmlspecialchars($q['username']);
				if ($q['description']) $qdescrizione = "\nDescrizione: " . htmlspecialchars($q['description']);
				$stati_chat = [
					'avviato' => "Avviato",
					'attivo' => "Bot non membro",
					'inattivo' => "Bot rimosso dalla chat",
					'kicked' => "Bot bannato dalla chat",
					'visto' => "Bot mai entrato",
					'ban' => "Bannato Life Time"
				];
				if ($stati_chat[$q['status'][$botID]]) {
					$stat = $stati_chat[$q['status'][$botID]];
				} elseif (strpos($q['status'][$botID], "premium") === 0) {
					$creator = str_replace("premium", '', $q['status'][$botID]);
					$stat = "Premium by " . code($creator);
				} elseif (strpos($q['status'][$botID], "ban") === 0) {
					$dateban = str_replace("ban", '', $q['status'][$botID]);
					$stat = "Bannato fino al " . date($u['date_format'], $dateban);
				} else {
					$stat = json_encode($q['status']);
				}
				$ustato = "\nStato: $stat";
				$menu[] = [
					[
						"text" => "👮🏻‍♂️ Amministratori",
						"callback_data" => "chatadmins_$id"
					]
				];
				$menu[] = [
					[
						"text" => "🔄 Aggiorna dati",
						"callback_data" => "updatechat_$id"
					]
				];
				$menu[] = [
					[
						"text" => "✅",
						"callback_data" => "fatto"
					]
				];
				editMsg($chatID, bold("Informazioni gruppo") . "\nID: $id \nTitolo: " . htmlspecialchars($q['title']) . $qdescrizione . $qusername . $ustato, $cbmid, $menu);
			} elseif ($type == "canale") {			
				if ($q['username']) {
					$link = "https://t.me/" . $q['username'];
				} else {
					$link = "https://t.me/c/" . str_replace("-", '', str_replace("-100", '', $id)) . "/1";
					if ($isadmin) {
						$q['admins'] = json_decode($q['admins'], true);
						foreach($q['admins'] as $staff) {
							if ($staff['user_id'] == $botID and $staff['permissions']['can_invite_users']) $link = getLink($id);
						}
					}
				}
				if ($isadmin) sm($chatID, "Link: " . getLink($id));
				if ($q['username']) $qusername = "\nUsername: @" . htmlspecialchars($q['username']);
				if ($q['description']) $qdescrizione = "\nDescrizione: " . htmlspecialchars($q['description']);
				$stati_chat = [
					'avviato' => "Avviato",
					'attivo' => "Bot non membro",
					'inattivo' => "Bot rimosso dalla chat",
					'kicked' => "Bot bannato dalla chat",
					'visto' => "Bot mai entrato",
					'ban' => "Bannato Life Time"
				];
				if ($stati_chat[$q['status'][$botID]]) {
					$stat = $stati_chat[$q['status'][$botID]];
				} elseif (strpos($q['status'][$botID], "premium") === 0) {
					$creator = str_replace("premium", '', $q['status'][$botID]);
					$stat = "Premium by " . code($creator);
				} elseif (strpos($q['status'][$botID], "ban") === 0) {
					$dateban = str_replace("ban", '', $q['status'][$botID]);
					$stat = "Bannato fino al " . date($u['date_format'], $dateban);
				} else {
					$stat = json_encode($q['status']);
				}
				$ustato = "\nStato: $stat";
				$menu[] = [
					[
						"text" => "👮🏻‍♂️ Amministratori",
						"callback_data" => "chatadmins_$id"
					]
				];
				$menu[] = [
					[
						"text" => "🔄 Aggiorna dati",
						"callback_data" => "updatechat_$id"
					]
				];
				$menu[] = [
					[
						"text" => "✅",
						"callback_data" => "fatto"
					]
				];
				editMsg($chatID, bold("Informazioni canale") . "\nID: $id \nTitolo: " . htmlspecialchars($q['title']) . $qdescrizione . $qusername . $ustato, $msid, $menu);
			} else {
				editMsg($chatID, "Chat sconosciuta", $msid);
			}
			if ($u['settings']['admin_perms']['gestione']) {
				$api = $config['logger'];
				sm($config['logger_chat'], "#id$userID #menu$msid #info\n<b>Utente:</> " . tag() . " [" . code($userID) . "] \n<b>getInfo:</> $id\n<b>Bot</>: #bot" . $botID);
			}
			die;
		}
		
		if (strpos($cmd, "stato ") === 0 and $isadmin) {
			dm($chatID, $msgID);
			$ex = explode(" ", $cmd, 2);
			$id = str_replace("@", '', $ex[1]);
			$msid = sm($chatID, "Verifico i dati sui vari database...")['result']['message_id'];
			if (isset($id)) {
				$q = db_query("SELECT * FROM utenti WHERE user_id = ? or username = ?", [round($id), $id], true);
				if ($q['ok']) {
					$q = $q['result'];
				} else {
					editMsg($chatID, "❌ " . getTranslate('generalError'), $m['result']['message_id']);
					die;
				}
				if (!$q['user_id']) {
					$q = db_query("SELECT * FROM gruppi WHERE chat_id = ? or username = ?", [round($id), $id], true);
					if ($q['ok']) {
						$q = $q['result'];
					} else {
						editMsg($chatID, "❌ " . getTranslate('generalError'), $m['result']['message_id']);
						die;
					}
					if (!$q['chat_id']) {
						$q = db_query("SELECT * FROM canali WHERE chat_id = ? or username = ?", [round($id), $id], true);
						if ($q['ok']) {
							$q = $q['result'];
						} else {
							editMsg($chatID, "❌ " . getTranslate('generalError'), $m['result']['message_id']);
							die;
						}
						if (!$q['chat_id']) {
							editMsg($chatID, "Chat non trovata...1\n$id", $msid);
							die;
						} else {
							$type = "canale";
							$id = $q['chat_id'];
						}
					} else {
						$type = "gruppo";
						$id = $q['chat_id'];
					}
				} else {
					$type = "utente";
					$id = $q['user_id'];
				}
			} else {
				editMsg($chatID, "Nessun ID o username inserito...", $msid);
				die;
			}
			dm($chatID, $msid);
			$q['status'] = json_decode($q['status'], true);
			if (!is_array($q['status'])) $q['status'] = [];
			if ($type == "utente") {
				if ($redis) {
					// Fix aggiornamento del nome utente
					$redis->del("name$id");
				}
				if (!is_numeric($ex[1])) {
					// Aggiornamento delle info utente
					$config['response'] = true;
					username(str_replace("@", '', $ex[1]));
				}
				$config['disabilita_notifica'] = true;
				$m = fw($q['user_id'], $config['sponsor'], 21);
				if ($m['ok']) {
					dm($q['user_id'], $m['result']['message_id']);
					if (strpos($q['status'][$botID], "ban") === 0) {
						$stat = "Bannato - Avviato.";
						$stato = $q['status'][$botID];
					} else {
						$stat = "Avviato";
						$stato = "avviato";
					}
				} else {
					if (strpos($q['status'][$botID], "ban") === 0) {
						if ($q['status'][$botID] == "ban") {
							$stat = "Bannato";
						} else {
							$stat = "Bannato a tempo";
						}
						$stato = $q['status'][$botID];
					} elseif ($m['error_code'] == 403) { // Forbidden
						if ($m['description'] == "Forbidden: bot was blocked by the user") {
							$stat = "Bloccato dall'utente";
							$stato = "blocked";
						} elseif ($m['description'] == "Forbidden: user is deactivated") {
							$stat = "Account eliminato";
							$stato = "deleted";
						} elseif ($m['description'] == "Forbidden: bot can't initiate conversation with a user") {
							$stat = "Bot non avviato";
							$stato = "attivo";
						} elseif ($m['description'] == "Forbidden: bot can't send messages to bots") {
							$stat = "Bot";
							$stato = "bot";
						} else {
							$stat = "Sconosciuto: " . $m['description'];
							$stato = $q['status'][$botID];
						}
					} elseif ($m['error_code'] == 400) { // Bad Request
						if ($m['description'] == "Bad Request: chat not found") {
							$stat = "Chat non incontrata: " . $m['description'];
							$stato = 'inattivo';
						} else {
							$stat = "Sconosciuto: " . $m['description'];
							$stato = $q['status'][$botID];
						}
					} else {
						$stat = "Errore: " . $m['description'];
						$stato = $q['status'][$botID];
					}
				}
				if ($stato !== $q['status'][$botID]) {
					setStatus($q['user_id'], $stato);
				} else {
					$stat = "$stat \nNon aggiornato: " . $q['status'][$botID];
				}
				$ustato = "\nStato: $stat";
				sm($chatID, bold("Stato utente") . "\nID: $id ". $ustato, $menu);
			} elseif ($type == "gruppo") {
				$m = fw($q['chat_id'], $config['sponsor'], 21);
				if ($m['ok']) {
					dm($q['chat_id'], $m['result']['message_id']);
					if (strpos($q['status'][$botID], "ban") === 0) {
						$stat = "Bannato - Avviato.";
						$stato = $q['status'][$botID];
					} else {
						$stat = "Avviato";
						$stato = "avviato";
					}
				} else {
					if ($m['error_code'] == 403) {
						if ($m['description'] == "Forbidden: bot is not a member of the supergroup chat") {
							$stat = "Bot non membro";
							$stato = "attivo";
						} else {
							$stat = "Sconosciuto: " . $m['description'];
							$stato = $q['status'][$botID];
						}
					} elseif ($m['error_code'] == 400) {
						if ($m['description'] == "Bad Request: chat not found") {
							$stat = "Chat non incontrata";
							$stato = 'visto';
						} else {
							$stat = "Sconosciuto: " . $m['description'];
							$stato = $q['status'][$botID];
						}
					} else {
						$stat = "Errore: " . $m['description'];
						$stato = $q['status'][$botID];
					}
				}
				if ($stato !== $q['status'][$botID]) {
					$q['status'][$botID] = $stato;
					$q1 = db_query("UPDATE utenti SET status = ? WHERE user_id = ?", [json_encode($q['status']), $q['user_id']]);
					if (!$q1['ok']) {
						editMsg($chatID, "❌ " . getTranslate('generalError'), $m['result']['message_id']);
						die;
					}
				}
				$ustato = "\nStato: $stat";
				sm($chatID, bold("Stato gruppo") . "\nID: $id " . $ustato, $menu);
			} elseif ($type == "canale") {
				$m = fw($q['chat_id'], $config['sponsor'], 21);
				if ($m['ok']) {
					dm($q['chat_id'], $m['result']['message_id']);
					if (strpos($q['status'][$botID], "ban") === 0) {
						$stat = "Bannato - Avviato.";
						$stato = $q['status'][$botID];
					} else {
						$stat = "Avviato";
						$stato = "avviato";
					}
				} else {
					if ($m['error_code'] == 403) {
						if ($m['description'] == "Forbidden: bot is not a member of the channel chat") {
							$stat = "Bot non membro";
							$stato = "attivo";
						} else {
							$stat = "Sconosciuto: " . $m['description'];
							$stato = $q['status'][$botID];
						}
					} elseif ($m['error_code'] == 400) {
						if ($m['description'] == "Bad Request: chat not found") {
							$stat = "Chat non incontrata";
							$stato = 'visto';
						} else {
							$stat = "Sconosciuto: " . $m['description'];
							$stato = $q['status'][$botID];
						}
					} else {
						$stat = "Errore: " . $m['description'];
						$stato = $q['status'][$botID];
					}
				}
				if ($stato !== $q['status'][$botID]) {
					$q['status'][$botID] = $stato;
					$q1 = db_query("UPDATE utenti SET status = ? WHERE user_id = ?", [json_encode($q['status']), $q['user_id']]);
					if (!$q1['ok']) {
						editMsg($chatID, "❌ " . getTranslate('generalError'), $m['result']['message_id']);
						die;
					}
				}
				$ustato = "\nStato: $stat";
				sm($chatID, bold("Stato canale") . "\nID: $id " . $ustato, $menu);
			} else {
				sm($chatID, "Chat sconosciuta");
			}
			die;
		}
		
		if ($cmd == "gestione" or $cbdata == "gestione") {
			$menu[] = [
				[
					'text' => "Utenti 👤",
					'callback_data' => 'gestione_utenti_1'
				],
				[
					'text' => "Bot 🤖",
					'callback_data' => 'gestione_bots_1'
				]
			];
			$menu[] = [
				[
					'text' => "Gruppi 👥",
					'callback_data' => 'gestione_gruppi_1'
				],
				[
					'text' => "Canali 📢",
					'callback_data' => 'gestione_canali_1'
				]
			];
			$menu[] = [
				[
					'text' => "Fatto ✅",
					'callback_data' => 'fatto'
				]
			];
			$testo = "Cosa vuoi gestire?";
			if ($cbdata) {
				cb_reply($cbid, '', false, $cbmid, $testo, $menu);
			} else {
				dm($chatID, $msgID);
				$m = sm($chatID, $testo, $menu);
				if (!$isadmin) {
					$api = $config['logger'];
					sm($config['logger_chat'], "#id$userID #menu{$m['result']['message_id']} \n<b>Utente:</b> " . tag() . " [" . code($userID) . "]\n<b>Aperto gestione</>\n<b>Bot</>: #bot" . $botID);
				}
			}
			die;
		}
		
		if (strpos($cbdata, "gespag_") === 0) {
			$idb = ['utenti', 'gruppi', 'canali'];
			$e = explode("_", $cbdata);
			$db = $e[1];
			$page = $e[2];
			if (in_array($db, $idb)) {
				if ($db == "utenti") {
					$typech = "user_id";
				} else {
					$typech = "chat_id";
				}
				$cosi = db_query("SELECT $typech FROM $db LIMIT 10000", false, false);
				if ($cosi['ok']) {
					$cosi = $cosi['result'];
				} else {
					cb_reply($cbid, "❌ " . getTranslate('generalError'), true);
					die;
				}
				$quanti = count($cosi);
				$pages = round($quanti / 5);
				if ($page == 0) {
					$prim = 1;
				} else {
					$prim = (25 * $page) + 1;
				}
				$fin = $prim + 25;
				$range = range($prim, $pages);
				foreach ($range as $num) {
					if ($prim + 4 < $num) {
						$test .= "$prim poi $num \n";
						$prim = $num;
					}
					if ($num < $fin) {
						$menu[$prim][] = [
							"text" => "$num",
							"callback_data" => "gestione_$db" . "_$num"
						];
					} else {
						$dopo = true;
					}
				}
				if ($page != 0) {
					$menufrecce[] = [
						"text" => "⏮",
						"callback_data" => "gespag_$db" . "_" . round($page - 1)
					];
				}
				if (isset($dopo)) {
					$menufrecce[] = [
						"text" => "⏭",
						"callback_data" => "gespag_$db" . "_" . round($page + 1)
					];
				}
				if (isset($menufrecce)) $menu[] = $menufrecce;
				$menu[] = [
					[
						"text" => "Indietro",
						"callback_data" => "gestione"
					],
				];
				$menu = array_values($menu);
				cb_reply($cbid, "$pages pagine", false, $cbmid, bold("Gestione $db") . " \n\nSeleziona la pagina da visualizzare.", $menu);
			}
			die;
		}
	
		if (strpos($cmd, "premium ") === 0 and $isadmin) {
			$ex = explode(" ", $cmd, 3);
			if (!is_numeric($ex[1])) {
				username(str_replace("@", '', $ex[1]));
			}
			$id = str_replace("@", '', $ex[1]);
			if (isset($id)) {
				$q = db_query("SELECT * FROM utenti WHERE user_id = ? or username = ?", [round($id), $id], true);
				if ($q['ok']) {
					$q = $q['result'];
				} else {
					sm($chatID, "❌ " . getTranslate('generalError'));
					die;
				}
				$q['settings'] = json_decode($q['settings'], true);
				$id = $q['user_id'];
				if (!$q['user_id']) {
					sm($chatID, "User not found in the database...");
					die;
				} elseif (isset($q['settings']['premium'])) {
					sm($chatID, "This user is already premium...");
					die;
				} else {
					$id = $q['user_id'];
				}
				date_default_timezone_set(getTimeZone($id));
				if ($config['usa_redis'] and isset($redis)) {
					rdel("name$id");
				}
				if (isset($ex[2])) {
					$dat = $ex[2];
					if (strpos($dat, "/") !== false) {
						$exp = explode(" ", $dat);
						$data = explode("/", $exp[0]);
						if (isset($exp[1])) {
							$ora = explode(":", $exp[1]);
						} else {
							$ora = "000";
						}
						$timep = mktime($ora[0], $ora[1], $ora[2], $data[1], $data[0], $data[2]);
						if ($timep < time()) {
							sm($chatID, "Invalid date. \n It would be out of order to the " . date("d/m/Y", $timep) . " at " . date("H:i:s", $timep));
							die;
						}
					} elseif (strpos($dat, "for ") !== false) {
						$exp = strtolower(str_replace("for ", '', $dat));
						$timep = strtotime($exp);
						if ($timep <= time()) {
							sm($chatID, "Invalid date. \n It would be out of order to the " . date("d/m/Y", $timep) . " at " . date("H:i:s", $timep));
							die;
						}
					} else {
						sm($chatID, "Invalid date.");
						die;
					}
					$al = "al " . date("d/m/Y", $timep) . " alle " . date("H:i:s", $timep) . "\n$com";
					$stato = $timep;
					$expires = date("D, j M Y H:i", $timep);
				} else {
					$al = "indefinitely.";
					$stato = 'lifetime';
					$expires = $stato;
				}
				$q['settings']['premium'] = $stato;
				$q1 = db_query("UPDATE utenti SET settings = ? WHERE user_id = ?", [json_encode($q['settings']), $id], "no");
				if (!$q1['ok']) {
					sm($chatID, "❌ " . getTranslate('generalError'));
					die;
				}
				sm($chatID, tag($id, getName($id)['result']) . " is now a Premium user!");
				sm($id, getTranslate('youArePremium', $q['lang']));
				$api = $config['shop_logger'];

				sm($config['shop_logger_chat'], "#Premium #id$userID \n<b>User:</b> " . tag($id, getName($id)['result']) . " \n<b>Premium Expires:</b> " . $expires . " \n<b>Premium by:</b> " . tag() . " \n@" . $config['username_bot']);
			}
			die;
		}

		if (strpos($cmd, "unpremium ") === 0 and $isadmin) {
			$ex = explode(" ", $cmd, 3);
			if (!is_numeric($ex[1])) {
				username(str_replace("@", '', $ex[1]));
			}
			$id = str_replace("@", '', $ex[1]);
			if (isset($id)) {
				$q = db_query("SELECT * FROM utenti WHERE user_id = ? or username = ?", [round($id), $id], true);
				if ($q['ok']) {
					$q = $q['result'];
				} else {
					sm($chatID, "❌ " . getTranslate('generalError'));
					die;
				}
				$q['settings'] = json_decode($q['settings'], true);
				$id = $q['user_id'];
				if (!$q['user_id']) {
					sm($chatID, "User not found in the database...");
					die;
				} elseif (!isset($q['settings']['premium'])) {
					sm($chatID, "This user is not premium...");
					die;
				} else {
					$id = $q['user_id'];
				}
				date_default_timezone_set(getTimeZone($id));
				if ($config['usa_redis'] and isset($redis)) {
					rdel("name$id");
				}
				unset($q['settings']['premium']);
				$q1 = db_query("UPDATE utenti SET settings = ? WHERE user_id = ?", [json_encode($q['settings']), $id], "no");
				if (!$q1['ok']) {
					sm($chatID, "❌ " . getTranslate('generalError'));
					die;
				}
				sm($chatID, tag($id, getName($id)['result']) . " is no longer a Premium user!");

				$api = $config['shop_logger'];

				sm($config['shop_logger_chat'], "#UnPremium #id$userID #id$id \n<b>User:</b> " . tag($id, getName($id)['result']) . " \n<b>Premium Expires: now</b> \n<b>Unset Premium by:</b> " . tag() . " \n@" . $config['username_bot']);
			}
			die;
		}

		if (strpos($cmd, "addmoney ") === 0 and $isadmin) {
			$ex = explode(" ", $cmd);
			if (!is_numeric($ex[1])) {
				username(str_replace("@", '', $ex[1]));
			}
			$id = str_replace("@", '', $ex[1]);
			$money = str_replace("-", '', round($ex[2]));
			$tax = str_replace("-", '', $ex[3]);
			if (isset($id) and isset($money)) {
				$q = db_query("SELECT * FROM utenti WHERE user_id = ? or username = ?", [round($id), $id], true);
				if ($q['ok']) {
					$q = $q['result'];
				} else {
					sm($chatID, "❌ " . getTranslate('generalError'));
					die;
				}
				$q['settings'] = json_decode($q['settings'], true);
				$id = $q['user_id'];
				if (!$q['user_id']) {
					sm($chatID, "User not found in the database...");
					die;
				} else {
					$id = $q['user_id'];
				}
				date_default_timezone_set(getTimeZone($id));
				if ($config['usa_redis'] and isset($redis)) {
					rdel("name$id");
				}
				$q['settings']['wallet'] = $q['settings']['wallet'] + $money;
				$q1 = db_query("UPDATE utenti SET settings = ? WHERE user_id = ?", [json_encode($q['settings']), $id], "no");
				if (!$q1['ok']) {
					sm($chatID, "❌ " . getTranslate('generalError'));
					die;
				}
				if (is_numeric($tax)) {
					$tasse = italic(getTranslate('taxPaid', [number_format($tax / 100, 2)]));
					$tassec = "\n<b>Tax paid:</b> €" . number_format($tax / 100, 2);
				}
				$m = sm($q['user_id'], "✅ " . getTranslate('paymentVerified', [number_format($money / 100, 2)], $q['lang']) . "\n" . $tasse);
				if ($m['ok']) {
					sm($chatID, tag($id, getName($id)['result']) . " have paid €" . number_format($money / 100, 2) . "!\nNow have €" . number_format($q['settings']['wallet'] / 100, 2));
				} else {
					sm($chatID, tag($id, getName($id)['result']) . " have paid €" . number_format($money / 100, 2) . "!\nNow have €" . number_format($q['settings']['wallet'] / 100, 2) . "\n" . json_encode($m));
				}
				$api = $config['shop_logger'];
				sm($config['shop_logger_chat'], "#Pay #id$id \n<b>User:</b> " . tag($id, getName($id)['result']) . " \n<b>Paid:</b> €" . number_format($money / 100, 2) . "\n<b>Money added by:</b> " . tag() . "$tassec \n@" . $config['username_bot']);
			}
			die;
		}

		if (strpos($cmd, "setmoney ") === 0 and $isadmin) {
			$e = explode(" ", $cmd, 3);
			$e[1] = str_replace('@', '', $e[1]);
			$t = db_query("SELECT * FROM utenti WHERE user_id = ? or username = ? LIMIT 1", [round($e[1]), $e[1]], true);
			if ($t['ok']) {
				$t = $t['result'];
			} else {
				sm($chatID, json_encode($t));
				die;
			}
			$t['settings'] = json_decode($t['settings'], true);
			$diff = "€" . number_format($t['settings']['wallet'] / 100, 2) . " -> €" . number_format($e[2] / 100, 2);
			$t['settings']['wallet'] = $e[2];
			db_query("UPDATE utenti SET settings = ? WHERE user_id = ?", [json_encode($t['settings']), $t['user_id']], 'no');
			sm($chatID, "Done");
			$api = $config['shop_logger'];
			sm($config['shop_logger_chat'], "#SetMoney #id{$t['user_id']} \n<b>User:</b> " . tag($t['user_id'], $t['nome'], $t['cognome']) . " \n<b>Money:</b> $diff \n<b>SetBy:</b> " . tag() . "\n@" . $config['username_bot']);
			die;
		}
		
		if (strpos($cbdata, "useraffiliate_") === 0) {
			$e = explode("_", str_replace("useraffiliate_", '', $cbdata));
			$user_id = $e[0];
			$q = db_query("SELECT * FROM utenti WHERE user_id = ?", [$user_id], true);
			if ($q['ok']) {
				$q = $q['result'];
			} else {
				cb_reply($cbid, "❌ " . getTranslate('generalError'), true);
				die;
			}
			if (!isset($q['user_id'])) {
				cb_reply($cbid, "Utente non trovato nel database...", true);
				die;
			}
			$affiliati = json_decode(file_get_contents("/home/masterpoll-documents/affiliates.json"), true);
			if (!isset($affiliati[$user_id])) {
				cb_reply($cbid, 'Affiliato mai attivato da questo utente...', true);
			} else {
				$t = bold("💰 Affiliato di " . $q['nome'] . " " . $q['cognome']);
				$affiliato = $affiliati[$user_id];
				if (is_array($affiliato['users']) and $affiliato['users']) {
					foreach($affiliato['users'] as $bot_id => $users) {
						foreach($users as $user => $uinfos) {
							$acusers[$user] = true;
						}
					}
					$cusers = count($acusers);
				} else {
					$cusers = 0;
				}
				if ($affiliato['status']) {
					$stato = "Attivato";
				} else {
					$stato = "Disattivato";
				}
				$t .= "\nStato: $stato\nSoldi generati: €" . number_format($affiliato['money'] / 100, 2) . "\nUtenti totali: $cusers";
				$menu[] = [
					[
						"text" => "🔄",
						"callback_data" => $cbdata
					]
				];
				$menu[] = [
					[
						"text" => "🔙 " . getTranslate('backButton'),
						"callback_data" => "updateuser_$user_id"
					]
				];
				cb_reply($cbid, '', false, $cbmid, $t, $menu);
			}
			die;
		}
		
		if (strpos($cbdata, "useradmins_") === 0) {
			$e = explode("_", str_replace("useradmins_", '', $cbdata));
			$user_id = $e[0];
			$mtt = $e[1];
			$q = db_query("SELECT * FROM utenti WHERE user_id = ?", [$user_id], true);
			if ($q['ok']) {
				$q = $q['result'];
			} else {
				cb_reply($cbid, "❌ " . getTranslate('generalError'), true);
				die;
			}
			if (!isset($q['user_id'])) {
				cb_reply($cbid, "Utente non trovato nel database...", true);
				die;
			}
			$cadmins = array_merge(db_query("SELECT * FROM canali WHERE strpos(admins, ?) != 0", [$userID], false)['result'], db_query("SELECT * FROM gruppi WHERE strpos(admins, ?) != 0", [$userID], false)['result']);
			if (!$cadmins) {
				cb_reply($cbid, 'Nessuna chat in cui sia amministratore...', true);
			} else {
				$t = bold("👮🏻‍♂️ Chat di " . $q['nome'] . " " . $q['cognome']);
				if ($mtt) {
					$plus = "➖";
					$p = false;
				} else {
					$plus = "➕";
					$p = true;
				}
				foreach($cadmins as $chat) {
					unset($statush);
					$chat['admins'] = json_decode($chat['admins'], true);
					foreach($chat['admins'] as $ad) {
						if ($ad['user']['id'] == $user_id) {
							$statush = $ad['status'];
						}
					}
					if ($statush) {
						if ($mtt) {
							$menu[] = [
								[
									"text" => $chat['title'],
									"callback_data" => "updatechat_" . $chat['chat_id']
								]
							];
						} else {
							if ($chat['username']) {
								$title = text_link($chat['title'], "https://t.me/" . $chat['username']);
							} else {
								$title = $chat['title'];
							}
							$t .= "\n• " . $title . " [" . code($chat['chat_id']) . "] \n" . $statush;
						}
					}
				}
				$menu[] = [
					[
						"text" => "🔄",
						"callback_data" => $cbdata
					],
					[
						"text" => $plus,
						"callback_data" => "useradmins_$user_id" . "_$p"
					]
				];
				$menu[] = [
					[
						"text" => "🔙 " . getTranslate('backButton'),
						"callback_data" => "updateuser_$user_id"
					]
				];
				cb_reply($cbid, '', false, $cbmid, $t, $menu);
			}
			die;
		}
		
		if (strpos($cbdata, "chatadmins_") === 0) {
			$e = explode("_", str_replace("chatadmins_", '', $cbdata));
			$chat_id = $e[0];
			$mtt = $e[1];
			$q = db_query("SELECT * FROM canali WHERE chat_id = ?", [$chat_id], true);
			if ($q['ok']) {
				$q = $q['result'];
			} else {
				cb_reply($cbid, "❌ " . getTranslate('generalError'), true);
				die;
			}
			$type = "channel";
			if (!isset($q['chat_id'])) {
				$q = db_query("SELECT * FROM gruppi WHERE chat_id = ?", [$chat_id], true);
				if ($q['ok']) {
					$q = $q['result'];
				} else {
					cb_reply($cbid, "❌ " . getTranslate('generalError'), true);
					die;
				}
				$type = "supergroup";
				if (!isset($q['chat_id'])) {
					cb_reply($cbid, "Chat non trovata nel database...", true);
					die;
				}
			}
			$q['admins'] = json_decode($q['admins'], true);
			if (!$q['admins']) {
				cb_reply($cbid, 'Lista amministratori non disponibile...', true);
			} else {
				$t = bold("👮🏻‍♂️ Amministratori di " . $q['title']);
				$q['admins'] = array_reverse($q['admins'], true);
				if ($mtt) {
					$plus = "➖";
					$p = false;
				} else {
					$plus = "➕";
					$p = true;
				}
				foreach($q['admins'] as $ad) {
					if ($mtt) {
						$menu[] = [
							[
								"text" => $ad['user']['first_name'] . " " . $ad['user']['last_name'],
								"callback_data" => "updateuser_" . $ad['user']['id']
							]
						];
					} else {
						$t .= "\n• " . tag($ad['user']['id'], $ad['user']['first_name'] ,$ad['user']['last_name']) . " [" . code($ad['user']['id']) . "] \n" . $ad['status'];
					}
				}
				$menu[] = [
					[
						"text" => "🔄",
						"callback_data" => $cbdata
					],
					[
						"text" => $plus,
						"callback_data" => "chatadmins_$chat_id" . "_$p"
					]
				];
				$menu[] = [
					[
						"text" => "🔙 " . getTranslate('backButton'),
						"callback_data" => "updatechat_$chat_id"
					]
				];
				cb_reply($cbid, '', false, $cbmid, $t, $menu);
			}
			die;
		}
		
		if (strpos($cbdata, "botmstatus_") === 0) {
			$e = explode("_",$cbdata);
			$id = $e[1];
			$q = db_query("SELECT * FROM bots WHERE bot_id = ?", [$id], true);
			if ($q['ok']) {
				$q = $q['result'];
			} else {
				cb_reply($cbid, "❌ " . getTranslate('generalError'), true);
				die;
			}
			$status = json_decode($q['master_status'], true);
			if ($status) {
				$q['master_status'] = false;
			} else {
				$q['master_status'] = true;
			}
			$q = db_query("UPDATE bots SET master_status = ? WHERE bot_id = ?", [json_encode($q['master_status']), $id], 'no');
			if ($q['ok']) {
				$q = $q['result'];
			} else {
				cb_reply($cbid, "❌ " . getTranslate('generalError'), true);
				die;
			}
			$cbdata = "updatebot_$id";
		}
		
		if (strpos($cbdata, "botstatus_") === 0) {
			$e = explode("_",$cbdata);
			$id = $e[1];
			$q = db_query("SELECT * FROM bots WHERE bot_id = ?", [$id], true);
			if ($q['ok']) {
				$q = $q['result'];
			} else {
				cb_reply($cbid, "❌ " . getTranslate('generalError'), true);
				die;
			}
			$status = json_decode($q['status'], true);
			if ($status) {
				$q['status'] = false;
			} else {
				$q['status'] = true;
			}
			$q = db_query("UPDATE bots SET status = ? WHERE bot_id = ?", [json_encode($q['status']), $id], 'no');
			if ($q['ok']) {
				$q = $q['result'];
			} else {
				cb_reply($cbid, "❌ " . getTranslate('generalError'), true);
				die;
			}
			$cbdata = "updatebot_$id";
		}
		
		if (strpos($cbdata, "botpremium_") === 0) {
			$e = explode("_",$cbdata);
			$id = $e[1];
			$q = db_query("SELECT * FROM bots WHERE bot_id = ?", [$id], true);
			if ($q['ok']) {
				$q = $q['result'];
			} else {
				cb_reply($cbid, "❌ " . getTranslate('generalError'), true);
				die;
			}
			$status = json_decode($q['premium'], true);
			if ($status) {
				$q['premium'] = false;
			} else {
				$q['premium'] = true;
			}
			$q = db_query("UPDATE bots SET premium = ? WHERE bot_id = ?", [json_encode($q['premium']), $id], 'no');
			if ($q['ok']) {
				$q = $q['result'];
			} else {
				cb_reply($cbid, "❌ " . getTranslate('generalError'), true);
				die;
			}
			$cbdata = "updatebot_$id";
		}
		
		if (strpos($cbdata, "updateuser_") === 0) {
			$e = explode("_",$cbdata);
			$user_ID = $e[1];
			$config['response'] = true;
			$q = db_query("SELECT * FROM utenti WHERE user_id = ?", [$user_ID], true);
			if ($q['ok']) {
				$q = $q['result'];
			} else {
				cb_reply($cbid, "❌ " . getTranslate('generalError'), true);
				die;
			}
			if ($redis) {
				if ($redis->get($cbdata) >= time()) {
					cb_reply($cbid, '⚠️ Please wait a second and try again...', false);
				} else {
					$redis->set($cbdata, time() + 10);
					$user = getChat($user_ID);
					if ($user['ok'] === false) {
						cb_reply($cbid, "Non sono riuscito a ricevere le sue info!\n" . $user['description'], true);
					} else {
						$user = $user['result'];
						if (!$user['first_name']) {
							setStatus($user_ID, "deleted");
							$user['first_name'] = "Deleted account";
						}
						if (!$user['last_name']) {
							$user['last_name'] = "";
						}
						if (!$user['username']) {
							$user['username'] = "";
						}
						db_query("UPDATE utenti SET nome = ?, cognome = ?, username = ? WHERE user_id = ?", [$user['first_name'], $user['last_name'], $user['username'], $user_ID]);
						cb_reply($cbid, "Aggiornato", false);
					}
				}
			}
			if ($e[1] and $e[2]) {
				$cbdata = "gestione_" . $e[1] . "-" . $e[2];
			} else {
				if ($q['cognome']) $qcognome = "\nCognome: " . htmlspecialchars($q['cognome']);
				if ($q['username']) $qusername = "\nUsername: @" . htmlspecialchars($q['username']);
				$q['status'] = json_decode($q['status'], true);
				if (!is_array($q['status'])) $q['status'] = [];
				$q['settings'] = json_decode($q['settings'], true);
				$languages_name = json_decode(file_get_contents("/home/masterpoll-documents/tg_languages_name.json"), true);
				$languages_flag = json_decode(file_get_contents("/home/masterpoll-documents/tg_languages_flag.json"), true);
				$ulanguage = "\nLanguage: " . $languages_flag[$q['lang']] . " " . maiuscolo($languages_name[$q['lang']]) . " (" . code($q['settings']['timezone']) . ")";
				$stati_user = [
					'blocked' => "Bloccato dall'utente",
					'deleted' => "Account eliminato",
					'bot' => "Bot",
					'avviato' => "Avviato",
					'attivo' => "Non avviato",
					'visto' => "Mai incontrato",
					'ban' => "Bannato Life Time"
				];
				if ($stati_user[$q['status'][$botID]]) {
					$stat = $stati_user[$q['status'][$botID]];
				} elseif (strpos($q['status'][$botID], "ban") === 0) {
					$dateban = str_replace("ban", '', $q['status'][$botID]);
					$stat = "Bannato fino a " . date($u['date_format'], $dateban);
				} else {
					$stat = json_encode($q['status']);
				}
				$ustato = "\nStato: $stat";
				if ($q['settings']['premium']) {
					if ($q['settings']['premium'] == "lifetime") {
						$p = "Life Time";
					} else {
						$p = date("D, j M Y H:i", $q['settings']['premium']);
					}
					$premium = "\nPremium: $p";
				}
				if ($q['settings']['token']) {
					$uapi = "\nAPI Key; " . code($q['settings']['token']);
				}
				if ($q['settings']['wallet']) {
					$uapi .= "\nPortafogli: €" . number_format($q['settings']['wallet'] / 100, 2);
				}
				if ($isadmin) {
					$menu[] = [
						[
							"text" => "✅️ Permessi",
							"callback_data" => "adminperms_$user_ID"
						]
					];
				}
				$menu[] = [
					[
						"text" => "👮🏻‍♂️ Amministrazioni",
						"callback_data" => "useradmins_$user_ID"
					]
				];
				$menu[] = [
					[
						"text" => "💰 Affiliato",
						"callback_data" => "useraffiliate_$user_ID"
					],
					[
						"text" => "🔄 Aggiorna dati",
						"callback_data" => "updateuser_$user_ID"
					]
				];
				$menu[] = [
					[
						"text" => "✅",
						"callback_data" => "fatto"
					]
				];
				editMsg($chatID, bold("Informazioni utente") . "\nID: $user_ID \nNome: " . htmlspecialchars($q['nome']) . $qcognome . $qusername . $ulanguage . $uapi . $ustato . $premium . "\nUltimo accesso: " . date($u['settings']['date_format'], $q['last_update']) . "\nRegistrato: " . date($u['settings']['date_format'], $q['first_update']), $cbmid, $menu);
				die;
			}
		}	
		
		if (strpos($cbdata, "updatebot_") === 0) {
			$e = explode("_",$cbdata);
			$id = $e[1];
			$config['response'] = true;
			$q = db_query("SELECT * FROM bots WHERE bot_id = ?", [$id], true);
			if ($q['ok']) {
				$q = $q['result'];
			} else {
				cb_reply($cbid, "❌ " . getTranslate('generalError'), true);
				die;
			}
			$t = "\nUsername: @" . htmlspecialchars($q['username_bot']);
			$mstatus = json_decode($q['master_status'], true);
			if ($mstatus) {
				$mstat = "🟢";
			} else {
				$mstat = "🔴";
			}
			$t .= "\nMaser Stato: $mstat";
			$status = json_decode($q['status'], true);
			if ($status) {
				$stat = "✅";
			} else {
				$stat = "❌";
			}
			$t .= "\nStato: $stat";
			$spremium = json_decode($q['premium'], true);
			if ($spremium) {
				$premium = "⭐️";
			} else {
				$premium = "❌";
			}
			$t .= "\nPremium: $premium";
			$t .= "\nProprietario: " . tag($q['owner_id'], $q['owner_id']);
			$menu[] = [
				[
					"text" => "👮🏻‍♂️ Creatore",
					"callback_data" => "updateuser_{$q['owner_id']}"
				],
				[
					"text" => "🔄 Aggiorna dati",
					"url" => "https://t.me/{$q['username_bot']}?start=" . bot_encode("update-data")
				]
			];
			$menu[] = [
				[
					"text" => "$mstat Master Stato",
					"callback_data" => "botmstatus_$id"
				],
				[
					"text" => "$stat Stato",
					"callback_data" => "botstatus_$id"
				],
				[
					"text" => "$premium Premium",
					"callback_data" => "botpremium_$id"
				]
			];
			$menu[] = [
				[
					"text" => "🔄 Aggiorna",
					"callback_data" => "updatebot_$id"
				],
				[
					"text" => "🗑 Elimina Bot",
					"callback_data" => "delbot_$id"
				]
			];
			$menu[] = [
				[
					"text" => "✅",
					"callback_data" => "fatto"
				]
			];
			cb_reply($cbid, '', false, $cbmid, bold("Informazioni Bot") . "\nID: $id $t", $menu);
			die;
		}
		
		if (strpos($cbdata, "updatechat_") === 0) {
			$e = explode("_", $cbdata);
			$chat_ID = $e[1];
			$config['response'] = true;
			$q = db_query("SELECT * FROM canali WHERE chat_id = ?", [$chat_ID], true);
			if ($q['ok']) {
				$q = $q['result'];
			} else {
				cb_reply($cbid, "❌ " . getTranslate('generalError'), true);
				die;
			}
			$type = "channel";
			if (!isset($q['chat_id'])) {
				$q = db_query("SELECT * FROM gruppi WHERE chat_id = ?", [$chat_ID], true);
				if ($q['ok']) {
					$q = $q['result'];
				} else {
					cb_reply($cbid, "❌ " . getTranslate('generalError'), true);
					die;
				}
				$type = "supergroup";
				if (!isset($q['chat_id'])) {
					cb_reply($cbid, "Chat non trovata nel database", true);
					die;
				}
			}
			if ($redis) {
				if ($redis->get($cbdata) >= time()) {
					cb_reply($cbid, '⚠️ Please wait a second and try again...', false);
				} else {
					$redis->set($cbdata, time() + 10);
					$chat = getChat($chat_ID);
					if ($chat['ok'] === false) {
						cb_reply($cbid, "Non sono riuscito a ricevere le informazioni di questa chat! \n" . $chat['description'], true);
					} else {
						$chat = $chat['result'];
						$title = $chat['title'];
						$type = $chat['type'];
						if (isset($chat['username'])) $usernamechat = $chat['username'];
						else $usernamechat = "";
						$descrizione = $chat['description'];
						if (!isset($descrizione)) {
							$descrizione = "";
						}
						$admins = getAdmins($chat_ID);
						if (isset($admins['ok'])) {
							$adminsg = json_encode($admins['result']);
						} else {
							$adminsg = "[]";
						}
						if ($type == "channel") {
							$q1 = db_query("UPDATE canali SET title = ?, username = ?, admins = ?, description = ? WHERE chat_id = ?", [$title, $usernamechat, $adminsg, $descrizione, $chat_ID]);
							if ($q1['ok']) {
								cb_reply($cbid, "❌ " . getTranslate('generalError'), true);
								die;
							}
							$q = db_query("SELECT * FROM canali WHERE chat_id = ?", [$chat_ID], true);
							if ($q['ok']) {
								$q = $q['result'];
							} else {
								cb_reply($cbid, "❌ " . getTranslate('generalError'), true);
								die;
							}
						} else {
							if (isset($chat['permissions'])) {
								$perms = $chat['permissions'];
							} else {
								$perms = ["can_send_messages" => true, "can_send_media_messages" => true, "can_send_polls" => true, "can_send_other_messages" => true, "can_add_web_page_previews" => true, "can_change_info" => false, "can_invite_users" => false, "can_pin_messages" => false];
							}
							db_query("UPDATE gruppi SET title = ?, username = ?, admins = ?, description = ?, permissions = ? WHERE chat_id = ?", [$title, $usernamechat, $adminsg, $descrizione, json_encode($perms), $chat_ID]);
							$q = db_query("SELECT * FROM gruppi WHERE chat_id = ?", [$chat_ID], true);
						}
						cb_reply($cbid, "Aggiornato", false);
					}
				}
			}
			if ($e[2] and $e[3]) {
				$cbdata = "gestione_" . $e[2] . "-" . $e[3];
			} else {
				$q['status'] = json_decode($q['status'], true);
				if (!is_array($q['status'])) $q['status'] = [];
				if ($type == "channel") {
					if ($q['username']) $qusername = "\nUsername: @" . htmlspecialchars($q['username']);
					if ($q['description']) $qdescrizione = "\nDescrizione: " . htmlspecialchars($q['description']);
					$stati_chat = [
						'avviato' => "Avviato",
						'attivo' => "Bot non membro",
						'inattivo' => "Bot rimosso dalla chat",
						'kicked' => "Bot bannato dalla chat",
						'visto' => "Bot mai entrato",
						'ban' => "Bannato Life Time"
					];
					if ($stati_chat[$q['status'][$botID]]) {
						$stat = $stati_chat[$q['status'][$botID]];
					} elseif (strpos($q['status'][$botID], "ban") === 0) {
						$dateban = str_replace("ban", '', $q['status'][$botID]);
						$stat = "Bannato fino al " . date($u['date_format'], $dateban);
					} elseif (strpos($q['status'][$botID], "premium") === 0) {
						$creator = str_replace("premium", '', $q['status'][$botID]);
						$stat = "Premium by " . code($creator);
					} else {
						$stat = json_encode($q['status']);
					}
					$ustato = "\nStato: $stat";
					$menu[] = [
						[
							"text" => "👮🏻‍♂️ Amministratori",
							"callback_data" => "chatadmins_$chat_ID"
						]
					];
					$menu[] = [
						[
							"text" => "🔄 Aggiorna dati",
							"callback_data" => "updatechat_$chat_ID"
						]
					];
				$menu[] = [
					[
						"text" => "✅",
						"callback_data" => "fatto"
					]
				];
					editMsg($chatID, bold("Informazioni canale") . "\nID: $chat_ID \nTitolo: " . htmlspecialchars($q['title']) . $qdescrizione . $qusername . $ustato, $cbmid, $menu);
				} else {
					if ($q['username']) $qusername = "\nUsername: @" . htmlspecialchars($q['username']);
					if ($q['description']) $qdescrizione = "\nDescrizione: " . htmlspecialchars($q['description']);
					$stati_chat = [
						'avviato' => "Avviato",
						'attivo' => "Bot non membro",
						'inattivo' => "Bot rimosso dalla chat",
						'kicked' => "Bot bannato dalla chat",
						'visto' => "Bot mai entrato",
						'ban' => "Bannato Life Time"
					];
					if ($stati_chat[$q['status'][$botID]]) {
						$stat = $stati_chat[$q['status'][$botID]];
					} elseif (strpos($q['status'][$botID], "ban") === 0) {
						$dateban = str_replace("ban", '', $q['status'][$botID]);
						$stat = "Bannato fino al " . date($u['date_format'], $dateban);
					} elseif (strpos($q['status'][$botID], "premium") === 0) {
						$creator = str_replace("premium", '', $q['status'][$botID]);
						$stat = "Premium by " . code($creator);
					} else {
						$stat = json_encode($q['status']);
					}
					$ustato = "\nStato: $stat";
					$menu[] = [
						[
							"text" => "👮🏻‍♂️ Amministratori",
							"callback_data" => "chatadmins_$chat_ID"
						]
					];
					$menu[] = [
						[
							"text" => "🔄 Aggiorna dati",
							"callback_data" => "updatechat_$chat_ID"
						]
					];
					$menu[] = [
						[
							"text" => "✅",
							"callback_data" => "fatto"
						]
					];
					editMsg($chatID, bold("Informazioni gruppo") . "\nID: $chat_ID \nTitolo: " . htmlspecialchars($q['title']) . $qdescrizione . $qusername . $ustato, $cbmid, $menu);
				}
				die;
			}
		}	
		
		if (strpos($cbdata, "infouser_") === 0) {
			$e = explode("_", $cbdata);
			$id = $e[1];
			$user = db_query("SELECT * FROM utenti WHERE user_id = ?", [$id], true);
			if ($user['ok']) {
				$user = $user['result'];
			} else {
				cb_reply($cbid, "❌ " . getTranslate('generalError'), true);
				die;
			}
			$user['status'] = json_decode($user['status'], true);
			$testo = "Nome: " . $user['nome'];
			if ($user['cognome']) $testo .= " ".$user['cognome'];
			if ($user['username']) $testo .= " \nUsername: @".$user['username'];
			$testo .= " \nPage: '" . $user['page'] . "'";
			if ($redis) {
				// Fix aggiornamento del nome utente
				$redis->del("name$id");
			}
			$stati_user = [
				'blocked' => "Bloccato dall'utente",
				'deleted' => "Account eliminato",
				'bot' => "Bot",
				'avviato' => "Avviato",
				'attivo' => "Non avviato",
				'visto' => "Mai incontrato",
				'ban' => "Bannato Life Time"
			];
			if ($stati_user[$user['status'][$botID]]) {
				$stat = $stati_user[$user['status'][$botID]];
			} elseif (strpos($user['status'][$botID], "ban") === 0) {
				$dateban = str_replace("ban", '', $user['status'][$botID]);
				$stat = "Bannato fino al " . date($u['date_format'], $dateban);
			} else {
				$stat = json_encode($user['status']);
			}
			$testo .= "\nStato: $stat";
			$user['settings'] = json_decode($user['settings'], true);
			if ($user['settings']['premium']) {
				if ($user['settings']['premium'] == "lifetime") {
					$p = "Life Time";
				} else {
					$p = date("D, j M Y H:i", $user['settings']['premium']);
				}
				$testo .= "\nPremium: $p";
			}
			cb_reply($cbid, "Informazioni Utente \n$testo", true);
			if ($e[2] and $e[3]) {
				unset($testo);
				unset($cbid);
				$cbdata = "gestione_" . $e[2] . "-" . $e[3];
			} else {
				die;
			}
		}
		
		if (strpos($cbdata, "infobot_") === 0) {
			$e = explode("_", $cbdata);
			$id = $e[1];
			$user = db_query("SELECT * FROM utenti WHERE user_id = ?", [$id], true);
			if ($user['ok']) {
				$user = $user['result'];
			} else {
				cb_reply($cbid, "❌ " . getTranslate('generalError'), true);
				die;
			}
			$user['status'] = json_decode($user['status'], true);
			$testo = "Nome: " . $user['nome'];
			if ($user['cognome']) $testo .= " ".$user['cognome'];
			if ($user['username']) $testo .= " \nUsername: @".$user['username'];
			$testo .= " \nPage: '" . $user['page'] . "'";
			if ($redis) {
				// Fix aggiornamento del nome utente
				$redis->del("name$id");
			}
			cb_reply($cbid, "Informazioni Utente \n$testo", true);
			if ($e[2] and $e[3]) {
				unset($testo);
				unset($cbid);
				$cbdata = "gestione_" . $e[2] . "-" . $e[3];
			} else {
				die;
			}
		}
		
		if (strpos($cbdata, "infochat_") === 0) {
			$e = explode("_", $cbdata);
			$id = $e[1];
			$chat = db_query("SELECT * FROM canali WHERE chat_id = ?", [$id], true);
			if ($chat['ok']) {
				$chat = $chat['result'];
			} else {
				cb_reply($cbid, "❌ " . getTranslate('generalError'), true);
				die;
			}
			if (!isset($chat['chat_id'])) {
				$chat = db_query("SELECT * FROM gruppi WHERE chat_id = ?", [$id], true);
				if ($chat['ok']) {
					$chat = $chat['result'];
				} else {
					cb_reply($cbid, "❌ " . getTranslate('generalError'), true);
					die;
				}
				if (!isset($chat['chat_id'])) {
					cb_reply($cbid, "Chat non trovata nel database", true);
					die;
				}
			}
			$chat['status'] = json_decode($chat['status'], true);
			$testo = "Titolo: " . $chat['title'];
			if ($chat['username']) $testo .= " \nUsername: @".$chat['username'];
			$admi = json_decode($chat['admins'], true);
			if (isset($admi['result'])) $admi = $admi['result'];
			foreach ($admi as $adminsa) {
				if ($adminsa['status'] == 'creator') {
					$founder = $adminsa['user']['first_name'] . " [" . $adminsa['user']['id'] . "]";
				}
			}
			$testo .= "\nCreatore: ".$founder;
			$testo .= " \nPage: '" . $chat['page'] . "'";
			$stati_chat = [
				'avviato' => "Avviato",
				'attivo' => "Bot non membro",
				'inattivo' => "Bot rimosso dalla chat",
				'kicked' => "Bot bannato dalla chat",
				'visto' => "Bot mai entrato",
				'ban' => "Bannato Life Time"
			];
			if ($stati_chat[$q['status'][$botID]]) {
				$stat = $stati_chat[$q['status'][$botID]];
			} elseif (strpos($q['status'][$botID], "ban") === 0) {
				$dateban = str_replace("ban", '', $q['status'][$botID]);
				$stat = "Bannato fino al " . date($u['date_format'], $dateban);
			} elseif (strpos($q['status'][$botID], "premium") === 0) {
				$creator = str_replace("premium", '', $q['status'][$botID]);
				$stat = "Premium by " . code($creator);
			} else {
				$stat = json_encode($q['status']);
			}
			$testo .= "\nStato: $stat";
			cb_reply($cbid, "Informazioni Chat \n$testo", true);
			if ($e[2] and $e[3]) {
				$cbdata = "gestione_" . $e[2] . "-" . $e[3];
			} else {
				die;
			}
		}
		
		if (strpos($cbdata, "banchat_") === 0) {
			$e = explode("_", $cbdata);
			$id = $e[1];
			$chat = db_query("SELECT * FROM canali WHERE chat_id = ?", [$id], true);
			if ($chat['ok']) {
				$chat = $chat['result'];
			} else {
				cb_reply($cbid, "❌ " . getTranslate('generalError'), true);
				die;
			}
			$db = "canali";
			if (!isset($chat['chat_id'])) {
				$chat = db_query("SELECT * FROM gruppi WHERE chat_id = ?", [$id], true);
				if ($chat['ok']) {
					$chat = $chat['result'];
				} else {
					cb_reply($cbid, "❌ " . getTranslate('generalError'), true);
					die;
				}
				$db = "gruppi";
				if (!isset($chat['chat_id'])) {
					cb_reply($cbid, "Chat non trovata nel database", true);
					die;
				}
			}
			if (strpos($chat['status'][$botID], "ban") === 0) {
				$ban = "sbannata";
				$q1 = db_query("UPDATE $db SET status = ? WHERE chat_id = ?", [json_encode([$botID => 'inattesa']), $id], 'no');
				if (!$q1['ok']) {
					cb_reply($cbid, "❌ " . getTranslate('generalError'), true);
					die;
				}
			} else {
				$ban = "bannata";
				foreach (array_keys($config['usernames']) as $idBot) {
					$new[$idBot] = "ban$dateban";
				}
				$q1 = db_query("UPDATE $db SET status = ? WHERE chat_id = ?", [json_encode($new), $id]);
				if (!$q1['ok']) {
					cb_reply($cbid, "❌ " . getTranslate('generalError'), true);
					die;
				}
			}
			cb_reply($cbid, "Chat $ban dai $db", true);
			if ($e[2] and $e[3]) {
				$cbdata = "gestione_" . $e[2] . "-" . $e[3];
			} else {
				die;
			}
		}
		
		if (strpos($cbdata, "banuser_") === 0) {
			$e = explode("_", $cbdata);
			$id = $e[1];
			$user = db_query("SELECT * FROM utenti WHERE user_id = ?", [$id], true);
			if ($user['ok']) {
				$user = $user['result'];
			} else {
				cb_reply($cbid, "❌ " . getTranslate('generalError'), true);
				die;
			}
			if (strpos($user['status'][$botID], "ban") === 0) {
				$ban = "sbannato";
				$q1 = db_query("UPDATE utenti SET status = ? WHERE user_id = ?", [json_encode([$botID => 'inattesa']), $id], "no");
				if (!$q1['ok']) {
					cb_reply($cbid, "❌ " . getTranslate('generalError'), true);
					die;
				}
			} else {
				$ban = "bannato";
				foreach (array_keys($config['usernames']) as $idBot) {
					$new[$idBot] = "ban$dateban";
				}
				$q1 = db_query("UPDATE utenti SET status = ? WHERE user_id = ?", [json_encode($new), $id], "no");
				if (!$q1['ok']) {
					cb_reply($cbid, "❌ " . getTranslate('generalError'), true);
					die;
				}
			}
			cb_reply($cbid, "Utente $ban", true);
			if ($e[2] and $e[3]) {
				$cbdata = "gestione_" . $e[2] . "-" . $e[3];
			} else {
				die;
			}
		}
		
		if (strpos($cbdata, "leavechat_") === 0) {
			$e = explode("_", $cbdata);
			$id = $e[1];
			$chat = db_query("SELECT * FROM canali WHERE chat_id = ?", [$id], true);
			if ($chat['ok']) {
				$chat = $chat['result'];
			} else {
				cb_reply($cbid, "❌ " . getTranslate('generalError'), true);
				die;
			}
			$db = "canali";
			if (!isset($chat['chat_id'])) {
				$chat = db_query("SELECT * FROM gruppi WHERE chat_id = ?", [$id], true);
				if ($chat['ok']) {
					$chat = $chat['result'];
				} else {
					cb_reply($cbid, "❌ " . getTranslate('generalError'), true);
					die;
				}
				$db = "gruppi";
				if (!isset($chat['chat_id'])) {
					cb_reply($cbid, "Chat non trovata nel database", true);
					die;
				}
			}
			lc($chat['chat_id']);
			cb_reply($cbid, "Ho abbandonato questa chat", true);
			if ($e[2] and $e[3]) {
				$cbdata = "gestione_" . $e[2] . "-" . $e[3];
			} else {
				die;
			}
		}

		if (strpos($cbdata, "deluser_") === 0) {
			$e = explode("_", $cbdata);
			$id = $e[1];
			$user = db_query("SELECT * FROM utenti WHERE user_id = ?", [$id], true);
			if ($user['ok']) {
				$user = $user['result'];
			} else {
				cb_reply($cbid, "❌ " . getTranslate('generalError'), true);
				die;
			}
			if (!isset($user['user_id'])) {
				cb_reply($cbid, "Utente non trovato nel database", true);
				die;
			}
			//db_query("DELETE FROM utenti WHERE user_id = ?", [$id]);
			cb_reply($cbid, "Ho eliminato questo utente dal database", true);
			if ($e[2] and $e[3]) {
				$cbdata = "gestione_" . $e[2] . "-" . $e[3];
			} else {
				die;
			}
		}
		
		if (strpos($cbdata, "delbot_") === 0) {
			$e = explode("_", $cbdata);
			$id = $e[1];
			$q = db_query("SELECT * FROM bots WHERE bot_id = ?", [$id], true);
			if ($q['ok']) {
				$q = $q['result'];
			} else {
				cb_reply($cbid, "❌ " . getTranslate('generalError'), true);
				die;
			}
			if (!isset($q['bot_id'])) {
				cb_reply($cbid, "Bot non trovato nel database", true);
				die;
			}
			db_query("DELETE FROM bots WHERE bot_id = ?", [$id]);
			cb_reply($cbid, "Ho eliminato questo bot dal database", true);
			if (!$e[2] and !$e[3]) {
				dm($chatID, $cbmid);
			}
			$api = $config['clones_logger'];
			sm($config['clones_logger_chat'], bold("#Deleted \nBot: ") . text_link("@" . $q['username_bot'], "t.me/" . $q['username_bot']) . " - " . code($q['bot_id']) . bold("\nOwner: ") . code($q['owner_id']) . bold("\nDeleted by: ") . tag());
			die;
		}
		
		if (strpos($cbdata, "delchat_") === 0) {
			$e = explode("_", $cbdata);
			$id = $e[1];
			$chat = db_query("SELECT * FROM canali WHERE chat_id = ?", [$id], true);
			if ($chat['ok']) {
				$chat = $chat['result'];
			} else {
				cb_reply($cbid, "❌ " . getTranslate('generalError'), true);
				die;
			}
			$db = "canali";
			if (!isset($chat['chat_id'])) {
				$chat = db_query("SELECT * FROM gruppi WHERE chat_id = ?", [$id], true);
				if ($chat['ok']) {
					$chat = $chat['result'];
				} else {
					cb_reply($cbid, "❌ " . getTranslate('generalError'), true);
					die;
				}
				$db = "gruppi";
				if (!isset($chat['chat_id'])) {
					cb_reply($cbid, "Chat non trovata nel database", true);
					die;
				}
			}
			//db_query("DELETE FROM $db WHERE chat_id = ?", [$id]);
			cb_reply($cbid, "Ho eliminato questa chat dal database", true);
			if ($e[2] and $e[3]) {
				$cbdata = "gestione_" . $e[2] . "-" . $e[3];
			} else {
				die;
			}
		}
		
		if (strpos($cbdata, "gestione_") === 0) {
			$idb = ['utenti', 'gruppi', 'canali', 'bots'];
			$e = explode("_", $cbdata);
			$db = $e[1];
			$page = $e[2];
			if ($page == 1) {
				$limit = 5;
			} elseif (is_numeric($page)) {
				$limit = 5 * $page;
			} else {
				$limit = 1000;
			}
			if (in_array($db, $idb)) {
				$limit = $limit + 1;
				$cosi = db_query("SELECT * FROM $db LIMIT $limit", false, false);
				if ($cosi['ok']) {
					$cosi = $cosi['result'];
				} else {
					cb_reply($cbid, "❌ " . getTranslate('generalError'), true);
					die;
				}
				$ultimo = (5 * $page) - 1;
				$primo = $ultimo - 4;
				$range = range($primo, $ultimo);
				foreach ($range as $num) {
					$coso = $cosi[$num];
					if (isset($coso['user_id'])) {
						if ($cognome) $coso['nome'] .= " " . $coso['cognome'];
						$num = $num + 1;
						if ($num < 10) $num .= "️⃣";
						$testo .=  "\n$num - ".textspecialchars($coso['nome']) . " [".code($coso['user_id']) . "]";
						$leggenda = italic("🔄 Aggiorna informazioni \nℹ️ Informazioni utente \n🗑 Elimina l'utente \n🚷 Banna utente");
						$menu[] = [
							[
								"text" => $num,
								"callback_data" => "updateuser_" . $coso['user_id']
							],
							[
								"text" => "🔄",
								"callback_data" => "updateuser_" . $coso['user_id'] . "_$db" . "_$page"
							],
							[
								"text" => "ℹ️",
								"callback_data" => "infouser_" . $coso['user_id'] . "_$db" . "_$page"
							],
							[
								"text" => "🗑",
								"callback_data" => "deluser_" . $coso['user_id'] . "_$db" . "_$page"
							],
							[
								"text" => "🚷",
								"callback_data" => "banuser_" . $coso['user_id'] . "_$db" . "_$page"
							]
						];
					} elseif (isset($coso['bot_id'])) {
						$num = $num + 1;
						if ($num < 10) $num .= "️⃣";
						$testo .=  "\n$num - @".textspecialchars($coso['username_bot']) . " [".code($coso['bot_id']) . "]";
						$leggenda = italic("🔄 Aggiorna informazioni \nℹ️ Informazioni Botto \n🗑 Elimina Bot \n🚷 Attiva Bot");
						$menu[] = [
							[
								"text" => $num,
								"callback_data" => "updatebot_" . $coso['bot_id']
							],
							[
								"text" => "🔄",
								"callback_data" => "updatebot_" . $coso['bot_id'] . "_$db" . "_$page"
							],
							[
								"text" => "ℹ️",
								"callback_data" => "infobot_" . $coso['bot_id'] . "_$db" . "_$page"
							],
							[
								"text" => "🗑",
								"callback_data" => "delbot_" . $coso['bot_id'] . "_$db" . "_$page"
							],
							[
								"text" => "🚷",
								"callback_data" => "botstatus_" . $coso['bot_id'] . "_$db" . "_$page"
							]
						];
					} elseif (isset($coso['chat_id'])) {
						$num = $num + 1;
						if ($num < 10) $num .= "️⃣";
						$testo .=  "\n$num - ".textspecialchars($coso['title']) . " [".code($coso['chat_id']) . "]";
						$leggenda = italic("🔄 Aggiorna informazioni \nℹ️ Informazioni sulla chat \n🚮 Lascia la chat \n🗑 Elimina la chat \n🚷 Banna chat");
						$menu[] = [
							[
								"text" => $num,
								"callback_data" => "updatechat_" . $coso['chat_id']
							],
							[
								"text" => "🔄",
								"callback_data" => "updatechat_" . $coso['chat_id'] . "_$db" . "_$page"
							],
							[
								"text" => "ℹ️",
								"callback_data" => "infochat_" . $coso['chat_id'] . "_$db" . "_$page"
							],
							[
								"text" => "🚮",
								"callback_data" => "leavechat_" . $coso['chat_id'] . "_$db" . "_$page"
							],
							[
								"text" => "🗑",
								"callback_data" => "delchat_" . $coso['chat_id'] . "_$db" . "_$page"
							],
							[
								"text" => "🚷",
								"callback_data" => "banchat_" . $coso['chat_id'] . "_$db" . "_$page"
							]
						];
					}
				}
				if ($page !== "1") {
					$menufrecce[] = [
						"text" => "⏪",
						"callback_data" => "gestione_$db" . "_" . ($page - 1)
					];
				}
				if (isset($cosi[$ultimo + 1])) {
					$menufrecce[] = [
						"text" => "⏺",
						"callback_data" => "gespag_$db"
					];
					$menufrecce[] = [
						"text" => "⏩",
						"callback_data" => "gestione_$db" . "_" . ($page + 1)
					];
				}
				if (isset($menufrecce)) $menu[] = $menufrecce;
				$menu[] = [
					[
						"text" => "Indietro",
						"callback_data" => "gestione"
					],
				];
				cb_reply($cbid, '', false, $cbmid, bold("Gestione $db\n") . $testo . "\n\n$leggenda", $menu);
			} else {
				cb_reply($cbid, "Errore: database sconosciuto");
			}
			die;
		}
		
		if ($cmd == "iscritti" or $cbdata == 'iscritti') {
			if ($cbid) cb_reply($cbid, 'Loading...', false);
			if ($cmd) {
				$config['json_payload'] = false;
				$m = sm($chatID, "Carico...");
				$cbmid = $m['result']['message_id'];
			}
			$menu[] = [
				[
					'text' => "Più informazioni ➕",
					'callback_data' => 'controllo_iscritti'
				]
			];
			$q[0] = db_query("SELECT COUNT(*) FROM utenti WHERE strpos(status,?)>0", [$botID], true);
			$q[1] = db_query("SELECT COUNT(*) FROM utenti", false, true);
			$q[2] = db_query("SELECT COUNT(*) FROM gruppi WHERE strpos(status,?)>0", [$botID], true);
			$q[3] = db_query("SELECT COUNT(*) FROM gruppi", false, true);
			$q[4] = db_query("SELECT COUNT(*) FROM canali WHERE strpos(status,?)>0", [$botID], true);
			$q[5] = db_query("SELECT COUNT(*) FROM canali", false, true);
			$q[6] = db_query("SELECT COUNT(*) FROM bots WHERE strpos(status,?)>0", ['1'], true);
			$q[7] = db_query("SELECT COUNT(*) FROM bots", false, true);
			$t = bold("♾ ISCRITTI di $botID") . "\n\n" . bold("👤 Utenti: ") . round($q[0]['result']['count']) . "/" . round($q[1]['result']['count']) . "\n" . bold("👥 Gruppi: ") . round($q[2]['result']['count']) . "/" . round($q[3]['result']['count']) . "\n" . bold("📢 Canali: ") . round($q[4]['result']['count']) . "/" . round($q[5]['result']['count']) . "\n" . bold("🤖 Bot: ") . round($q[6]['result']['count']) . "/" . round($q[7]['result']['count']);
			editMsg($chatID, $t, $cbmid, $menu);
			die;
		}
		
		if ($cbdata == "controllo_iscritti" and $isadmin) {
			cb_reply($cbid, 'Attendi', false, $cbmid, bold("Controllo il database...🕔"));
			//$config['console'] = false;
			$menu[] = [
				[
					'text' => "Indietro",
					'callback_data' => 'iscritti'
				]
			];
			$stati_user = [
				'blocked' => "Bloccato dall'utente",
				'deleted' => "Account eliminato",
				'bot' => "Bot",
				'avviato' => "Avviato",
				'attivo' => "Non avviato",
				'visto' => "Mai incontrato",
				'ban' => "Bannato Life Time"
			];
			$stati_chat = [
				'avviato' => "Avviato",
				'attivo' => "Bot non membro",
				'inattivo' => "Bot rimosso dalla chat",
				'visto' => "Bot mai entrato",
				'ban' => "Bannato Life Time"
			];
			$dbs = ["utenti", "gruppi"];
			$stato_chat['user_id'] = $stati_user;
			$stato_chat['chat_id'] =  $stati_chat;
			if ($config['post_canali']) $dbs[] = "canali";
			foreach ($dbs as $db) {
				if ($db == "utenti") {
					$r = db_query("SELECT status FROM $db WHERE strpos(status,?)>0 ORDER BY last_update DESC", [$botID], false);
				} else {
					$r = db_query("SELECT status FROM $db WHERE strpos(status,?)>0", [$botID], false);
				}
				if ($r['ok']) {
					$r = $r['result'];
				} else {
					editMsg($chatID, "❌ " . getTranslate('generalError'), $cbmid);
					die;
				}
				unset($chats);
				foreach ($r as $chat) {
					if ($db == 'utenti') {
						$type = "user_id";
					} else {
						$type = "chat_id";
					}
					$chat['status'] = json_decode($chat['status'], true);
					if (!$chat['status'][$botID]) {
						$chat['status'][$botID] = "visto";
					}
					if ($stato_chat[$type][$chat['status'][$botID]]) {
						$stat = $stato_chat[$type][$chat['status'][$botID]];
					} elseif (strpos($chat['status'][$botID], "ban") === 0) {
						$dateban = str_replace("ban", '', $chat['status'][$botID]);
						$stat = "Bannato fino al " . date($u['date_format'], $dateban);
					} elseif (strpos($chat['status'][$botID], "premium") === 0) {
						$stat = "Premium";
					} else {
						$stat = "visto";
					}
					$chat = $chat[$type];
					$types[$db][$stat][] = $chat;
				}
				foreach ($types[$db] as $type => $c) {
					$chats .= "\n$type: " . count($c);
				}
				$db[0] = strtoupper($db[0]);
				$testo .= "\n\n" . bold($db) . " (" . count($r) . ")" . $chats;
			}
			sleep(1);
			editMsg($chatID, bold("DATABASE ISCRITTI 👥") . $testo, $cbmid, $menu);
			die;
		}
		
	}
}
