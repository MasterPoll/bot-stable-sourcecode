<?php

if (isset($update['chosen_inline_result'])) {
	require($f['class']);
	if (!$classwork) {
		call_error("<b>Fatal Error:</b> le funzioni dei sondaggi non funzionano!");
		die;
	}
	$query = $update['chosen_inline_result']['result_id'];
	$e = explode('-', $query);
	$poll_id = $e[1];
	$creator = $e[0];
	if ($e[2] == "button") {
		$type = "button";
	} elseif ($e[2] == "addadmin") {
		$type = "addadmin";
	} else {
		$type = false;
	}
	insertMessage(['poll_id' => $poll_id, 'creator' => $creator], $update['chosen_inline_result']['inline_message_id'], $type);
	if ($update['chosen_inline_result']['inline_message_id']) {
		$cbmid = $update['chosen_inline_result']['inline_message_id'];
		$messageType = "callback_query";
		if ($type === false) {
			$cbdata = "update_$poll_id-$creator-false";
		} else {
			$update_poll = "poll_$poll_id-$creator";
		}
	} else {
		die;
	}
} elseif (isset($update['inline_query'])) {
	# answerInlineQuery
	function aiq($queryID, $results = [], $description = "", $spt = "asd") {
		global $api;
		global $config;
		global $userID;
		global $msg;
		if (!$results) $results = [];
		if ($spt == "asd") {
			if ($config['devmode']) {
				$spt = "Show " . count($results) . " polls...";
			} else {
				$spt = getTranslate('inlineTextCreateNewPoll');
			}
		}
		if (count($results) > 50) {
			$results = [];
			$spt = "Error: results too long.";
		}
		if ($results) {
			foreach ($results as $res) {
				if (strlen($res['message_text']) > 4096) {
					$res['message_text'] = "MESSAGE_TOO_LONG";
				}
				if (!is_bool($res['disable_web_page_preview'])) {
					$res['disable_web_page_preview'] = false;
				}
				if (!is_array($res['inline_keyboard'])) {
					unset($res['inline_keyboard']);
				}
				$resl[] = $res;
			}
			$results = $resl;
		}
		$args = [
			'inline_query_id' => $queryID,
			'results' => json_encode($results),
			'cache_time' => 5,
			'switch_pm_text' => $spt,
			'switch_pm_parameter' => "inlines"
		];
		$rr = sendRequest("https://api.telegram.org/$api/answerInlineQuery", $args, $config['devmode']);
		$ar = json_decode($rr, true);
		if ($ar['ok'] === false) call_error("answerInlineQuery\nUser: $userID\nQuery: $msg\n" . bold("Error: ") . json_encode($ar));
		die;
	}
	
	$iquery = $update['inline_query'];
	$anteprimanull[0] = "https://telegra.ph/file/624f1efe9c4ce4e682f48.png";
	$anteprimanull[1] = "https://telegra.ph/file/baad176b89723a359c6c3.png";
	$anteprima = [
		"" => $anteprima['null'],
		"vote" => [
			null => "https://telegra.ph/file/ed125dd969f566e5864eb.png",
			0 => "https://telegra.ph/file/ed125dd969f566e5864eb.png",
			1 => "https://telegra.ph/file/7c4be8cc4a54dd0e65904.png"
		],
		"doodle" => [
			null => "https://telegra.ph/file/32eb013b2575291f62bd6.png",
			0 => "https://telegra.ph/file/32eb013b2575291f62bd6.png",
			1 => "https://telegra.ph/file/42fa736cc1a7755badb55.png"
		],
		"limited doodle" => [
			null => "https://telegra.ph/file/d08d7b4a174efe0c023ad.png",
			0 => "https://telegra.ph/file/d08d7b4a174efe0c023ad.png",
			1 => "https://telegra.ph/file/c3b5880426908724aa25d.png"
		],
		"board" => [
			null => "https://telegra.ph/file/6b68666ea8efb9e1e748d.png",
			0 => "https://telegra.ph/file/6b68666ea8efb9e1e748d.png",
			1 => "https://telegra.ph/file/2e348b666d6ef90ecb283.png"
		],
		"participation" => [
			null => "https://telegra.ph/file/7c4be8cc4a54dd0e65904.png",
			0 => "https://telegra.ph/file/7c4be8cc4a54dd0e65904.png",
			1 => $anteprimanull[1]
		],
		"rating" => [
			null => "https://telegra.ph/file/355dba88dd53b553708a6.png",
			0 => "https://telegra.ph/file/355dba88dd53b553708a6.png",
			1 => "https://telegra.ph/file/23b8f68899c2477118839.png"
		],
		"quiz" => [
			null => "https://telegra.ph/file/355dba88dd53b553708a6.png",
			0 => "https://telegra.ph/file/355dba88dd53b553708a6.png",
			1 => "https://telegra.ph/file/23b8f68899c2477118839.png"
		]
		// 0 = personal
		// 1 = anonymous
	];
	$queryID = $iquery['id'];
	$msg = $iquery['query'];
	require($f['class']);
	if (!$classwork) {
		$text = '🤖 Bot Error: bug auto-reported...';
		aiq($queryID, [], "", $text);
		call_error("<b>Fatal Error:</b> le funzioni dei sondaggi non funzionano!");
		die;
	}
	
	if ($queryID and strpos($msg, "share") === 0) {
		$b = bot_decode(str_replace('share', '', str_replace(' ', '', $msg)));
		$e = explode("-", $b);
		$poll_id = $e[0];
		$creator = $e[1];
		if (!is_numeric($creator) or !is_numeric($poll_id)) {
			aiq($queryID, $results, "", "Invalid share code...");
			die;
		} 
		if ($creator and $poll_id) {
			$q = sendPoll($poll_id, $creator);
			if ($q['ok']) {
				$q = $q['result'];
			} else {
				aiq($queryID);
				die;
			}
			if ($q['status'] == "closed" or $q['status'] == "deleted" or !$q['status']) {
				aiq($queryID, $results, "", "This share code not work...");
				die;
			}
			$poll = pollToMessage($q, false);
			if (!$q['settings']['sharable'] and getPollAdmin($q, $userID)['status'] !== "administrator") {
				aiq($queryID, $results);
				die;
			}
			$r = $poll['text'];
			$menu = $poll['menu'];
			unset($choices);
			if ($q['type'] !== "board") {
				foreach ($q['choice'] as $choice => $value) {
					if (isset($choices)) $choices .= " | ";
					if (strlen($choice) <= 12) $choices .= $choice;
					else $choices .= substr($choice, 0, 12) . "...";
				}
			}
			if ($q['anonymous']) {
				$anony = getTranslate('inlineDescriptionAnonymous' . maiuscolo($q['type']));
			} else {
				$anony = getTranslate('inlineDescriptionPersonal' . maiuscolo($q['type']));
			}
			if ($q['settings']['web_page'] === "0") {
				$q['settings']['web_page'] = false;
			} elseif ($q['settings']['web_page'] === "1") {
				$q['settings']['web_page'] = true;
			} else {
				$q['settings']['web_page'] = false;
			}
			$results[] = [
				"type" => "article", 
				"id" => "$creator-$poll_id-" . time(), 
				"title" => $q['title'], 
				"message_text" => $r, 
				"parse_mode" => "html",
				"disable_web_page_preview" => $q['settings']['web_page'],
				"description" => mb_convert_encoding(getTranslate('inlineDescriptionFirstLine', [$anony, $q['votes']]) . " \n" . $choices, "UTF-8"),
				"thumb_url" => $anteprima[$q['type']][$q['anonymous']],
				"thumb_width" => 512,
				"thumb_height" => 512,
				"reply_markup" => ["inline_keyboard" => $menu]
			];
		}
		aiq($queryID, $results);
	}
	
	if ($queryID and strpos($msg, "administrators") === 0) {
		$b = bot_decode(str_replace('administrators', '', str_replace(' ', '', $msg)));
		$e = explode("-", $b);
		$poll_id = $e[0];
		$creator = $e[1];
		if (!is_numeric($creator) or !is_numeric($poll_id)) {
			aiq($queryID, $results, "", "Invalid share code...");
			die;
		} 
		$p = sendPoll($poll_id, $creator);
		if ($p['ok']) {
			$p = $p['result'];
		} else {
			aiq($queryID);
			die;
		}
		$menu[] = [
			[
				"text" => getTranslate('administratorsAddMe'),
				"callback_data" => "addMeAdmin_$poll_id-$creator"
			]
		];
		$r = getTranslate('administratorInvite', [$p['title']]);
		unset($choices);
		if ($p['type'] !== "board") {
			foreach ($p['choice'] as $choice => $value) {
				if (isset($choices)) $choices .= " | ";
				if (strlen($choice) <= 12) $choices .= $choice;
				else $choices .= substr($choice, 0, 12) . "...";
			}
		}
		if ($p['anonymous']) {
			$anony = getTranslate('inlineDescriptionAnonymous' . maiuscolo($p['type']));
		} else {
			$anony = getTranslate('inlineDescriptionPersonal' . maiuscolo($p['type']));
		}
		if ($p['settings']['web_page'] === "0") {
			$p['settings']['web_page'] = false;
		} elseif ($p['settings']['web_page'] === "1") {
			$p['settings']['web_page'] = true;
		} else {
			$p['settings']['web_page'] = false;
		}
		$results[] = [
			"type" => "article", 
			"id" => "$creator-$poll_id-addadmin", 
			"title" => mb_convert_encoding($p['title'], "UTF-8"),
			"message_text" => mb_convert_encoding($r, "UTF-8"),
			"parse_mode" => "html",
			"disable_web_page_preview" => $p['settings']['web_page'],
			"description" => mb_convert_encoding(getTranslate('inlineDescriptionFirstLine', [$anony, $p['votes']]) . " \n" . $choices, "UTF-8"),
			"thumb_url" => $anteprima[$p['type']][$p['anonymous']],
			"thumb_width" => 512,
			"thumb_height" => 512,
			"reply_markup" => ["inline_keyboard" => $menu]
		];
		aiq($queryID, $results);
	}
	
	function getInlinePoll($userID, $msg = false, $request_type = false) {
		global $anteprima;
		$lang = getLanguage($userID);
		if ($msg) {
			$msg = strtolower($msg);
			$polls = db_query("SELECT * FROM polls WHERE user_id = ? and status = ? and strpos(LOWER(title), LOWER(?))>0 ORDER BY poll_id LIMIT ?", [$userID, 'open', $msg, 50], false);
		} else {
			$polls = db_query("SELECT * FROM polls WHERE user_id = ? and status = ? ORDER BY poll_id LIMIT ?", [$userID, 'open', 50], false);
		}
		if ($polls['ok']) {
			$polls = $polls['result'];
		} else {
			return [];
		}
		if ($request_type == "bsearch") {
			foreach ($polls as $pollz) {
				$poll_id = $pollz['poll_id'];
				$creator = $pollz['user_id'];
				if ($pollz['status'] == "open" and strpos(strtolower($pollz['title']), strtolower($msg)) !== false) {
					$p = doPoll($pollz);
					if ($p['ok']) {
						$p = $p['result'];
						$r = getTranslate('loading', false, getLanguage($creator));
						if ($p['type'] == "board") {
							$urlor = "cburl-" . bot_encode("board_" . $p['poll_id'] . "-" . $p['creator']);
						} else {
							$urlor = "cburl-" . bot_encode("mypoll_" . $p['poll_id'] . "-" . $p['creator']);
						}
						$menu[0] = [
							[
								"text" => getTranslate('buttonVote'),
								"callback_data" => $urlor
							]
						];
						unset($choices);
						if ($p['type'] !== "board") {
							foreach ($p['choice'] as $choice => $value) {
								if (isset($choices)) $choices .= " | ";
								if (strlen($choice) <= 12) $choices .= $choice;
								else $choices .= substr($choice, 0, 12) . "...";
							}
						}
						if ($p['anonymous']) {
							$anony = getTranslate('inlineDescriptionAnonymous' . maiuscolo($p['type']));
						} else {
							$anony = getTranslate('inlineDescriptionPersonal' . maiuscolo($p['type']));
						}
						$results[] = [
							"type" => "article", 
							"id" => "$creator-$poll_id-button", 
							"title" => mb_convert_encoding($p['title'], "UTF-8"),
							"message_text" => mb_convert_encoding($r, "UTF-8"),
							"parse_mode" => "html",
							"disable_web_page_preview" => $poll['disable_web_preview'],
							"description" => mb_convert_encoding(getTranslate('inlineDescriptionFirstLine', [$anony, $p['votes']]) . " \n" . $choices, "UTF-8"),
							"thumb_url" => $anteprima[$p['type']][$p['anonymous']],
							"thumb_width" => 512,
							"thumb_height" => 512,
							"reply_markup" => ["inline_keyboard" => $menu]
						];
					}
				}
			}
		} elseif ($request_type == "search") {
			foreach ($polls as $pollz) {
				$poll_id = $pollz['poll_id'];
				$creator = $pollz['user_id'];
				if ($pollz['status'] == "open" and strpos(strtolower($pollz['title']), strtolower($msg)) !== false) {
					$p = doPoll($pollz);
					if ($p['ok']) {
						$p = $p['result'];
						$r = getTranslate('loading', false, getLanguage($creator));
						unset($choices);
						if ($p['type'] !== "board") {
							foreach ($p['choice'] as $choice => $value) {
								if (isset($choices)) $choices .= " | ";
								if (strlen($choice) <= 12) $choices .= $choice;
								else $choices .= substr($choice, 0, 12) . "...";
							}
						}
						if ($p['type'] == "board") {
							$urlor = "cburl-" . bot_encode("board_" . $p['poll_id'] . "-" . $p['creator']);
						} else {
							$urlor = "cburl-" . bot_encode("mypoll_" . $p['poll_id'] . "-" . $p['creator']);
						}
						$menu[0] = [
							[
								"text" => getTranslate('buttonVote'),
								"callback_data" => $urlor
							]
						];
						if ($p['anonymous']) {
							$anony = getTranslate('inlineDescriptionAnonymous' . maiuscolo($p['type']));
						} else {
							$anony = getTranslate('inlineDescriptionPersonal' . maiuscolo($p['type']));
						}
						$results[] = [
							"type" => "article", 
							"id" => "$creator-$poll_id-" . microtime(true), 
							"title" => mb_convert_encoding($p['title'], "UTF-8"),
							"message_text" => mb_convert_encoding($r, "UTF-8"),
							"parse_mode" => "html",
							"disable_web_page_preview" => $poll['disable_web_preview'],
							"description" => mb_convert_encoding(getTranslate('inlineDescriptionFirstLine', [$anony, $p['votes']]) . " \n" . $choices, "UTF-8"),
							"thumb_url" => $anteprima[$p['type']][$p['anonymous']],
							"thumb_width" => 512,
							"thumb_height" => 512,
							"reply_markup" => ["inline_keyboard" => $menu]
						];
					}
				}
			}
		} else {
			foreach ($polls as $pollz) {
				$poll_id = $pollz['poll_id'];
				$creator = $pollz['user_id'];
				if ($pollz['status'] == "open") {
					$p = doPoll($pollz);
					if ($p['ok']) {
						$p = $p['result'];
						$r = getTranslate('loading', false, getLanguage($creator));
						unset($choices);
						if ($p['type'] !== "board") {
							foreach ($p['choice'] as $choice => $value) {
								if (isset($choices)) $choices .= " | ";
								if (strlen($choice) <= 12) $choices .= $choice;
								else $choices .= substr($choice, 0, 12) . "...";
							}
						}
						if ($p['type'] == "board") {
							$urlor = "cburl-" . bot_encode("board_" . $p['poll_id'] . "-" . $p['creator']);
						} else {
							$urlor = "cburl-" . bot_encode("mypoll_" . $p['poll_id'] . "-" . $p['creator']);
						}
						$menu[0] = [
							[
								"text" => getTranslate('buttonVote'),
								"callback_data" => $urlor
							]
						];
						if ($p['anonymous']) {
							$anony = getTranslate('inlineDescriptionAnonymous' . maiuscolo($p['type']));
						} else {
							$anony = getTranslate('inlineDescriptionPersonal' . maiuscolo($p['type']));
						}
						$results[] = [
							"type" => "article", 
							"id" => "$creator-$poll_id-" . microtime(true), 
							"title" => mb_convert_encoding($p['title'], "UTF-8"),
							"message_text" => mb_convert_encoding($r, "UTF-8"),
							"parse_mode" => "html",
							"disable_web_page_preview" => $poll['disable_web_preview'],
							"description" => mb_convert_encoding(getTranslate('inlineDescriptionFirstLine', [$anony, $p['votes']]) . " \n" . $choices, "UTF-8"),
							"thumb_url" => $anteprima[$p['type']][$p['anonymous']],
							"thumb_width" => 512,
							"thumb_height" => 512,
							"reply_markup" => ["inline_keyboard" => $menu]
						];
					}
				}
			}
		}
		return $results;
	}
	
	if ($queryID and strpos($msg, '$c:') === 0) {
		$msg = str_replace('$c:', '', $msg);
		if ($msg) {
			$result = getInlinePoll($userID, $msg, "bsearch");
			aiq($queryID, $result);
		}
	}
	
	if ($queryID and strlen("$msg") <= 5 and is_numeric($msg) and $msg > 0 and strpos($msg, ".") == false and strpos($msg, ".") !== 0) {
		$poll_id = round($msg);
		$p = sendPoll($poll_id, $userID);
		if ($p['ok']) {
			$p = $p['result'];
		} else {
			aiq($queryID);
			die;
		}
		if ($p['status'] == "open") {
			$poll = pollToMessage($p, false);
			$r = $poll['text'];
			$menu = $poll['menu'];
			unset($choices);
			if ($p['type'] !== "board") {
				foreach ($p['choice'] as $choice => $value) {
					if (isset($choices)) $choices .= " | ";
					if (strlen($choice) <= 12) $choices .= $choice;
					else $choices .= substr($choice, 0, 12) . "...";
				}
			}
			if ($p['anonymous']) {
				$anony = getTranslate('inlineDescriptionAnonymous' . maiuscolo($p['type']));
			} else {
				$anony = getTranslate('inlineDescriptionPersonal' . maiuscolo($p['type']));
			}
			$result[] = [
				"type" => "article",
				"id" => "$userID-$poll_id-" . microtime(true),
				"title" => mb_convert_encoding($p['title'], "UTF-8"),
				"message_text" => mb_convert_encoding($r, "UTF-8"),
				"parse_mode" => "html",
				"disable_web_page_preview" => true,
				"description" => mb_convert_encoding(getTranslate('inlineDescriptionFirstLine', [$anony, $p['votes']]) . " \n" . $choices, "UTF-8"),
				"thumb_url" => $anteprima[$p['type']][$p['anonymous']],
				"thumb_width" => 512,
				"thumb_height" => 512,
				"reply_markup" => ["inline_keyboard" => $menu]
			];
		}
		aiq($queryID, $result);
	}
	
	if ($queryID and $msg or $msg == "0") {
		$result = getInlinePoll($userID, $msg, "search");
		aiq($queryID, $result);
	}
	
	if ($queryID and !$msg) {
		$result = getInlinePoll($userID);
		aiq($queryID, $result);
	}
	
	aiq($queryID, [], 'Bot Error?');
}
