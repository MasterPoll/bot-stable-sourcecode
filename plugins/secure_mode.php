<?php

$sm_cpu = sys_getloadavg();
foreach($sm_cpu as $sm_loadm) {
	$sm_cputot += $sm_loadm;
}
$sm_tot = round($sm_cputot / count($sm_cpu));
if ($sm_tot >= 10) {
	$secure_mode = true;
}
if (isset($redis)) {
	$stime = $redis->get('secure_mode' . $botID);
	if (is_numeric($stime)) {
		if (time() <= $stime) {
			$secure_mode = true;
		} else {
			$redis->del('secure_mode' . $botID);
		}
	}
	if ($cmd == "go" and $isadmin) {
		$redis->set('secure_mode' . $botID, time() + 10);
		sm($chatID, "✅ Secure mode for 10 seconds!");
		die;
	}
}

if ($secure_mode) {
	$t = "💤 Please wait a minute and try again...";
	$config['json_payload'] = true;
	$config['response'] = false;
	if ($cbid) {
		cb_reply($cbid, $t, true);
	} elseif ($typechat == "private") {
		sm($userID, $t);
	}
	die;
}

?>
