<?php 

if ($cbdata == "withdrawsmoney") {
	if ($redis) {
		if ($redis->get("withdrawsmoney") >= time()) {
			cb_reply($cbid, '⚠️ Please wait a second and try again...', true);
			die;
		} else {
			$redis->set("withdrawsmoney", time() + 5);
		}
	}
	$affiliates = json_decode(file_get_contents($f['affiliate']), true);
	if ($affiliates[$userID]['money'] >= 500) {
		$m = $affiliates[$userID]['money'];
		$affiliates[$userID]['money'] = 0;
		file_put_contents($f['affiliate'], json_encode($affiliates));
		$u['settings']['wallet'] = $m;
		db_query("UPDATE utenti SET settings = ? WHERE user_id = ?", [json_encode($u['settings']), $userID], "no");
		$redis->del($userID . "dbjson");
		cb_reply($cbid, "€" . number_format($m / 100, 2));
		$cbdata = "shop";
	} else {
		$cbdata = "affiliates";
	}
}

if (strpos($cbdata, "affiliate") === 0) {
	$to = [
		true => false,
		false => true
	];
	$emojis = [
		true => "⏸",
		false => "▶️"
	];
	$file = $f['affiliate'];
	$json = json_decode(file_get_contents($file), true);
	if (strpos($cbdata, "affiliate_") === 0) {
		if ($json[$userID]) {
			$json[$userID]['status'] = $to[$json[$userID]['status']];
			$u['settings']['affiliate'] = $json[$userID]['status'];
		} else {
			$json[$userID] = [
				"status" => true,
				"money" => 0,
				"users" => []
			];
			$u['settings']['affiliate'] = true;
		}
		db_query("UPDATE utenti SET settings = ? WHERE user_id = ?", [json_encode($u['settings']), $userID], "no");
		file_put_contents($file, json_encode($json, JSON_PRETTY_PRINT));
		$redis->del($userID . "dbjson");
	}
	if ($json[$userID]['status']) $desc = "\n" . getTranslate('affiliateDescription', [number_format($config['affiliate']['user_start'] / 100, 2), number_format($config['affiliate']['user_advertising'] / 100, 2)]);
	$menu[] = [
		[
			"text" => $emojis[$json[$userID]['status']] . " " . getTranslate('affiliateButton'),
			"callback_data" => "affiliate_"
		]
	];
	if ($u['settings']['affiliate']) {
		$menu[] = [
			[
				"text" => "🔄 " . getTranslate('commPageRefresh'),
				"callback_data" => "affiliate"
			]
		];
	}
	if ($json[$userID]) {
		if ($u['settings']['affiliate']) {
			$t = getTranslate('affiliateActivated');
		} else {
			$t = getTranslate('affiliateDeactivated');
		}
	} else {
		$t = getTranslate('affiliateDeactivated');
	}
	if (!isset($json[$userID]['money'])) {
		$json[$userID]['money'] = 0;
	} elseif ($json[$userID]['money'] >= 100) {
		$menu[] = [
			[
				"text" => "💱 " . getTranslate('withdrawsMoneyButton'),
				"callback_data" => "withdrawsmoney"
			]
		];
	}
	$menu[] = [
		[
			"text" => "🔙 " . getTranslate('backButton'),
			"callback_data" => "shop"
		]
	];
	if ($json[$userID]['users']) {
		foreach($json[$userID]['users'] as $botID => $users) {
			foreach ($users as $user_id => $adv) {
				$totalusers[$user_id] = true;
			}
		}
		$usercount = count(array_keys($totalusers));
	} else {
		$usercount = 0;
	}
	cb_reply($cbid, '', false, $cbmid, bold($t) . "$desc\n" . getTranslate('usersCount', [$usercount]) . "\n" . getTranslate('myWallet', [number_format($json[$userID]['money'] / 100, 2)]), $menu);
	die;
}

if ($cbdata == "shop" or $cmd == "start shop") {
	$r = bold("🛍 " . getTranslate('shop'));
	$shop = [];
	$sconto = $config['shop_discount'];
	if ($config['shop_discount'] !== 100) {
		$r .= " [" . getTranslate('shopDiscount', [round(100 - $config['shop_discount'])]) . "]";
	}
	foreach ($config['shop'] as $item => $infos) {
		if ($infos['available']) {
			unset($scontoapplicato);
			if ($infos['discount'] === 0) {
				$pi = number_format($infos['price'], 2);
				$pit = "";
				$len = strlen($pi);
				$range = range(0, $len - 1);
				foreach ($range as $num) {
					if ($ce) {
						$pit .= "̶";
						$pit .= $pi[$num];
					} else {
						$ce = 1;
						$pit[0] = $pi[0];
					}
				}
				unset($exnum);
				$scontoapplicato = italic("  (€$pit)");
				$infos['price'] = 0;
			} elseif ($infos['discount'] !== 100) {
				$pi = number_format($infos['price'], 2);
				$pit = "";
				$len = strlen($pi);
				$range = range(0, $len - 1);
				foreach ($range as $num) {
					if ($ce) {
						$pit .= "̶";
						$pit .= $pi[$num];
					} else {
						$ce = 1;
						$pit[0] = $pi[0];
					}
				}
				unset($exnum);
				$scontoapplicato = italic("  (€$pit)");
				$infos['price'] = $infos['price'] / 100 * $infos['discount'];
			} elseif ($sconto !== 100) {
				$pi = number_format($infos['price'], 2);
				$pit = "";
				$len = strlen($pi);
				$range = range(0, $len - 1);
				foreach ($range as $num) {
					if ($ce) {
						$pit .= "̶";
						$pit .= $pi[$num];
					} else {
						$ce = 1;
						$pit[0] = $pi[0];
					}
				}
				unset($exnum);
				$scontoapplicato = italic("  (€$pit)");
				$infos['price'] = $infos['price'] / 100 * $sconto;
			}
			if ($infos['price'] === 0) {
				$infos['price'] = "🆓 " . getTranslate('freePrice') . $scontoapplicato;
			} else {
				$infos['price'] = "€" . number_format($infos['price'], 2) . $scontoapplicato;
			}
		} else {
			$infos['price'] = getTranslate('shopNotAvailable');
		}
		if (strpos($item, "bot_") === 0) {
			$viptype = str_replace("bot_", '', $item);
			$shop['bot'][$viptype] = $infos;
		}
		if (strpos($item, "vip_") === 0) {
			$viptype = str_replace("vip_", '', $item);
			$shop['vip'][$viptype] = $infos;
		}
		if (strpos($item, "advertising_") === 0) {
			$viptype = str_replace("advertising_", '', $item);
			$shop['advertising'][$viptype] = $infos;
		}
	}
	if (isset($shop['bot'])) {
		foreach($shop['bot'] as $type => $infos) {
			$bott .= "\n • " . getTranslate('shopBot' . $type) . ": " . $infos['price'];
		}
		$r .= "\n\n🤖 " . bold(getTranslate('shopTitleBot')) . "\n" . italic(getTranslate('shopDescriptionBot')) . $bott;
	}
	if (isset($shop['vip'])) {
		foreach($shop['vip'] as $type => $infos) {
			$vip .= "\n • " . getTranslate('shopVIP' . $type) . ": " . $infos['price'];
		}
		$r .= "\n\n⭐️ " . bold(getTranslate('shopTitleVIP')) . "\n" . italic(getTranslate('shopDescriptionVIP')) . $vip;
	}
	if (isset($shop['advertising'])) {
		foreach($shop['advertising'] as $type => $infos) {
			$advertising .= "\n • " . getTranslate('shopAdvertisingViews', [$type]) . ": " . $infos['price'];
		}
		$r .= "\n\n💰 " . bold(getTranslate('shopTitleAdvertising')) . "\n" . italic(getTranslate('shopDescriptionAdvertising')) . $advertising;
	}
	if (file_exists($f['affiliate'])) {
		$r .= "\n\n" . bold("💰 " . getTranslate('affiliateButton')) . "\n" . getTranslate('affiliateDescription', [number_format($config['affiliate']['user_start'] / 100, 2), number_format($config['affiliate']['user_advertising'] / 100, 2)]);
	}
	if ($u['settings']['wallet']) {
		$r .= "\n\n🏦 " . getTranslate('myWallet', [number_format($u['settings']['wallet'] / 100, 2)]);
		$menup1[] = [
			"text" => "🛒 " . getTranslate('buy'),
			"callback_data" => "shop-buy"
		];
	} else {
		$menup1[] = [
			"text" => "🛒 " . getTranslate('buy'),
			"callback_data" => "shop-withdraws"
		];
	}
	$menup1[] = [
		"text" => "💰 " . getTranslate('affiliateButton'),
		"callback_data" => "affiliate"
	];
	if (isset($menup1)) $menu[] = $menup1;
	$menu[] = [
		[
			"text" => "🔙 " . getTranslate('mainMenuButton'),
			"callback_data" => "startMessage"
		]
	];
	if ($cmd) {
		sm($chatID, $r, $menu);
	} else {
		editMsg($chatID, $r, $cbmid, $menu);
		if (!$novip) cb_reply($cbid, '', false);
	}
	die;
}

if (strpos($cbdata, "buy-") === 0) {
	$e = explode("-", $cbdata);
	$art = $e[1];
	if ($config['shop'][$art]['available']) {
		if ($config['shop'][$art]['discount'] === 0) {
			$prez = 0;
		} elseif ($config['shop'][$art]['discount'] !== 100) {
			$prez = ($config['shop'][$art]['price'] / 100 * $config['shop'][$art]['discount']) * 100;
		} elseif ($config['shop_discount'] !== 100) {
			$prez = ($config['shop'][$art]['price'] / 100 * $config['shop_discount']) * 100;
		} else {
			$prez = $config['shop'][$art]['price'] * 100;
		}
		if ($u['settings']['wallet'] >= $prez) {
			if (strpos($art, "bot_") === 0) {
				$artic = str_replace('bot_', '', $art);
				$article = getTranslate('shopBot' . $artic);
				if ($e[2]) {
					$u['settings']['wallet'] = $u['settings']['wallet'] - $prez;
					$u['settings']['premium_clones'] += $artic;
					db_query("UPDATE utenti SET settings = ? WHERE user_id = ?", [json_encode($u['settings']), $userID], "no");
					cb_reply($cbid, '', false, $cbmid, getTranslate('botPaymentDone', [$config['clonerbot_username']]));
					$api = $config['shop_logger'];
					sm($config['shop_logger_chat'], "#Bot #id$userID \n<b>User:</b> " . tag() . " \n<b>Bots:</b> " . $artic . " \n<b>Spesa:</b> €" . number_format($prez / 100, 2) . " \n@" . $config['username_bot']);
				} else {
					$menu[] = [
						[
							"text" => getTranslate('yes'),
							"callback_data" => "buy-$art-yes"
						],
						[
							"text" => getTranslate('no'),
							"callback_data" => "shop-buy"
						]
					];
					cb_reply($cbid, '', false, $cbmid, getTranslate('shopBuyArticle', [$article]), $menu);
					die;
				}
			} elseif (strpos($art, "vip_") === 0) {
				$artic = str_replace('vip_', '', $art);
				$article = getTranslate('shopVIP' . $artic);
				if ($e[2]) {
					$u['settings']['wallet'] = $u['settings']['wallet'] - $prez;
					$times = [
						"Monthly" => 60 * 60 * 24 * 30,
						"SixMonth" => 60 * 60 * 24 * 30 * 6,
						"Annual" => 60 * 60 * 24 * 30 * 12,
						"LifeTime" => "lifetime"
					];
					$new = $times[$artic];
					if ($new == "lifetime") {
						$u['settings']['premium'] = $new;
						$expires = "LifeTime";
					} else {
						if (isset($u['settings']['premium'])) {
							if ($u['settings']['premium'] == "lifetime") {
								cb_reply($cbid, '');
								die;
							} else {
								$u['settings']['premium'] = $u['settings']['premium'] + $new;
							}
						} else {
							$u['settings']['premium'] = time() + $new;
						}
						$expires = date("D, j M Y H:i", $u['settings']['premium']);
					}
					db_query("UPDATE utenti SET settings = ? WHERE user_id = ?", [json_encode($u['settings']), $userID], "no");
					cb_reply($cbid, '', false, $cbmid, getTranslate('youArePremium'));
					$api = $config['shop_logger'];
					sm($config['shop_logger_chat'], "#Premium #id$userID \n<b>User:</b> " . tag() . " \n<b>Premium Expires:</b> " . $expires . " \n<b>Premium by:</b> " . tag($botID, "Bot Shop") . " \n<b>Spesa:</b> €" . number_format($prez / 100, 2) . " \n@" . $config['username_bot']);
					die;
				} else {
					$menu[] = [
						[
							"text" => getTranslate('yes'),
							"callback_data" => "buy-$art-yes"
						],
						[
							"text" => getTranslate('no'),
							"callback_data" => "shop-buy"
						]
					];
					cb_reply($cbid, '', false, $cbmid, getTranslate('shopBuyArticle', [$article]), $menu);
					die;
				}
			} elseif (strpos($art, "advertising_") === 0) {
				$views = str_replace('advertising_', '', $art);
				$article = getTranslate('shopAdvertisingViews', [$views]);
				if ($e[2]) {
					$u['settings']['wallet'] = $u['settings']['wallet'] - $prez;
					db_query("UPDATE utenti SET settings = ? WHERE user_id = ?", [json_encode($u['settings']), $userID], "no");
					cb_reply($cbid, '', false, $cbmid, getTranslate('sponsorPaymentDone'));
					$api = $config['shop_logger'];
					sm($config['shop_logger_chat'], "#Advertising #id$userID \n<b>User:</b> " . tag() . " \n<b>Spesa:</b> €" . number_format($prez / 100, 2) . " \n<b>Views:</b> " . $views . " \n@" . $config['username_bot']);
					die;
				} else {
					$menu[] = [
						[
							"text" => getTranslate('yes'),
							"callback_data" => "buy-$art-yes"
						],
						[
							"text" => getTranslate('no'),
							"callback_data" => "shop-buy"
						]
					];
					cb_reply($cbid, '', false, $cbmid, getTranslate('shopBuyArticle', [$article]), $menu);
					die;
				}
			} else {
				cb_reply($cbid, getTranslate('shopNotAvailable'), false);
				$cbdata = "shop-buy";
			}
		} else {
			cb_reply($cbid, "💸 " . getTranslate('shopNoEnoughMoney'), true);
			die;
		}
	} else {
		cb_reply($cbid, getTranslate('shopNotAvailable'), false);
		$cbdata = "shop-buy";
	}
}

if (strpos($cbdata, "shop-") === 0) {
	if ($cbdata == "shop-buy") {
		$t = bold("🛒 " . getTranslate('shop'));
		$shop = [];
		$sconto = $config['shop_discount'];
		foreach ($config['shop'] as $item => $infos) {
			if ($infos['available']) {
				$infos['id'] = $item;
				if ($infos['discount'] !== 100) {
					$infos['price'] = $infos['price'] / 100 * $infos['discount'];
				} elseif ($sconto !== 100) {
					$infos['price'] = $infos['price'] / 100 * $sconto;
				}
				$infos['price'] = "€" . number_format($infos['price'], 2);
				if (strpos($item, "vip_") === 0) {
					$viptype = str_replace("vip_", '', $item);
					$shop['vip'][$viptype] = $infos;
				}
				if (strpos($item, "advertising_") === 0) {
					$viptype = str_replace("advertising_", '', $item);
					$shop['advertising'][$viptype] = $infos;
				}
				if (strpos($item, "bot_") === 0) {
					$viptype = str_replace("bot_", '', $item);
					$shop['bot'][$viptype] = $infos;
				}
			}
		}
		if (isset($shop['bot'])) {
			$menu[] = [
				[
					"text" => "🤖 " . getTranslate('shopTitleBot'),
					"callback_data" => "shopart-bot"
				]
			];
			foreach($shop['bot'] as $type => $infos) {
				$menu[] = [
					[
						"text" => getTranslate('shopBot' . $type) . " - " . $infos['price'],
						"callback_data" => "buy-" . $infos['id']
					]
				];
			}
		}
		if (isset($shop['vip'])) {
			$menu[] = [
				[
					"text" => "⭐️ " . getTranslate('shopTitleVIP'),
					"callback_data" => "shopart-vip"
				]
			];
			foreach($shop['vip'] as $type => $infos) {
				$menu[] = [
					[
						"text" => getTranslate('shopVIP' . $type) . " - " . $infos['price'],
						"callback_data" => "buy-" . $infos['id']
					]
				];
			}
		}
		if (isset($shop['advertising'])) {
			$menu[] = [
				[
					"text" => "💰 " . getTranslate('shopTitleAdvertising'),
					"callback_data" => "shopart-advertising"
				]
			];
			foreach($shop['advertising'] as $type => $infos) {
				$menu[] = [
					[
						"text" => getTranslate('shopAdvertisingViews', [$type]) . " - " . $infos['price'],
						"callback_data" => "buy-" . $infos['id']
					]
				];
			}
		}
		$t .= "\n" . getTranslate('myWallet', [number_format($u['settings']['wallet'] / 100, 2)]);
		$menu[] = [
			[
				"text" => "🔙 " . getTranslate('backButton'),
				"callback_data" => "shop"
			]
		];
		cb_reply($cbid, '', false, $cbmid, $t, $menu);
	} elseif ($cbdata == "shop-withdraws") {
		$menu[] = [
			[
				"text" => "💶 PayPal",
				"url" => $config['links']['paypal']
			]
		];
		$menu[] = [
			[
				"text" => "🔙 " . getTranslate('backButton'),
				"callback_data" => "shop"
			]
		];
		cb_reply($cbid, '', false, $cbmid, getTranslate('shopBuyHelp', [$userID]), $menu);
	}
	die;
}

if (strpos($cbdata, "shopart-") === 0) {
	if ($cbdata == "shopart-vip") {
		cb_reply($cbid, getTranslate('shopDescriptionVIP'), true);
	} elseif ($cbdata == "shopart-bot") {
		cb_reply($cbid, getTranslate('shopDescriptionBot'), true);
	} else {
		cb_reply($cbid, getTranslate('shopDescriptionAdvertising'), true);
	}
	die;
}
